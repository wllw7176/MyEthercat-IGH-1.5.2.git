### 觉得资源对你有用，记得点击右上角的小星星哟 :kissing: 
### 本人已经不再从事ethercat相关工作，如果有问题也可以邮件沟通717628525@qq.com
### 代码用SourceInsight 3.5打开，其它软件打开可能会有乱码
### 文档目录结构及简要说明
### 创建了一个Ethercat-Igh交流群，要加群的话可以添加微信:wl717628525 备注信息请填：Ethercat-Igh 交流

# 0.Ethercat调试记录.txt
	调试Ethercat过程中碰到的问题及解决办法
```
ethercat 命令行工具有个隐藏选项没有提示
--master master_index 选择特定的主机索引

-------------------------------------------------------------------------------------------------------------------------
山羊电机与驱动器调试
1.
Ethercat 别名信息
ethercat slaves 
1  5555:0  PREOP  +  EL3162 2C. Ana. Input 0-10V
|  |    |  |      |  |
|  |    |  |      |  \- Name from the SII if avaliable,
|  |    |  |      |     otherwise vendor ID and product
|  |    |  |      |     code (both hexadecimal).
|  |    |  |      \- Error flag. '+' means no error,
|  |    |  |         'E' means that scan or
|  |    |  |         configuration failed.
|  |    |  \- Current application-layer state.
|  |    \- Decimal relative position to the last
|  |       slave with an alias address set.
|  \- Decimal alias address of this slave (if set),
|     otherwise of the last slave with an alias set,
|     or zero, if no alias was encountered up to this
|     position.
\- Absolute ring position in the bus.

If the --verbose option is given, a detailed (multi-line)
description is output for each slave.

Slave selection:
  Slaves for this and other commands can be selected with
  the --alias and --position parameters as follows:

  1) If neither the --alias nor the --position option
     is given, all slaves are selected.
  2) If only the --position option is given, it is
     interpreted as an absolute ring position and
     a slave with this position is matched.
  3) If only the --alias option is given, all slaves
     with the given alias address and subsequent
     slaves before a slave with a different alias
     address match (use -p0 if only the slaves
     with the given alias are desired, see 4)).
  4) If both the --alias and the --position option are
     given, the latter is interpreted as relative
     position behind any slave with the given alias.

Command-specific options:
  --alias    -a <alias>  Slave alias (see above).
  --position -p <pos>    Slave position (see above).
  --verbose  -v          Show detailed slave information.
  
驱动器信息:
0  0:0  PREOP  +  R ADVANCED MODEL with EtherCAT(P0002813G01)
1.1 获取pdo数(手册P212查看索引及子索引)
ethercat upload -a 1 -p 0 0x1018 0x00 
0x04 4
1.2 获取VENDOR_ID(手册P212查看索引及子索引)
ethercat upload -a 1 -p 0 0x1018 0x01
0x000001b9 441
1.2 获取PRODUCT_ID(手册P212查看索引及子索引)
ethercat upload -a 1 -p 0 0x1018 0x02
0x00000002 2


2. 通过ethercat应用程序操作电机
	首先要通过etercat slaves 获取从机信息，然后对从机进行相应操作
2.1 设置别名
	etercat alias 0x0001 -a 0 -p 0
2.2 读写寄存器
	注意，为了谨慎起见我们只对0x0FC0:0x0FFF 64KB 用户RAM空间进行操作，进行其它空间操作时要明确参数意义及作用。
	写寄存器
		ethercat reg_write -a 0 -p 0 0x0FC0 --type uint32 0x3344
	读寄存器
		ethercat reg_read -a 0 -p 0  -t uint32 0x0FC0 
		返回结果0x44 0x33

		
3. pdo 信息
ethercat pdos
SM0: PhysAddr 0x1800, DefaultSize  512, ControlRegister 0x26, Enable 1
SM1: PhysAddr 0x1c00, DefaultSize  512, ControlRegister 0x22, Enable 1
SM2: PhysAddr 0x1100, DefaultSize    0, ControlRegister 0x24, Enable 1
SM3: PhysAddr 0x1400, DefaultSize    0, ControlRegister 0x20, Enable 1

4. sdo 信息
ethercat  sdos
SDO 0x1000, "Device type"
  0x1000:00, r-r-r-, uint32, 32 bit, "Device type"
SDO 0x1001, "Error register"
  0x1001:00, r-r-r-, uint8, 8 bit, "Error register"
SDO 0x1008, "Device name"
  0x1008:00, r-r-r-, string, 88 bit, "Device name"
SDO 0x1009, "Hardware version"
  0x1009:00, r-r-r-, string, 32 bit, "Hardware version"
SDO 0x100a, "Software version"
  0x100a:00, r-r-r-, string, 88 bit, "Software version"
SDO 0x1010, "Store parameters"
  0x1010:00, r-r-r-, uint8, 8 bit, "Software version"
  0x1010:01, rwrwrw, uint32, 32 bit, "Save all parameters"
SDO 0x1018, "Identity"
  0x1018:00, r-r-r-, uint8, 8 bit, "Identity"
  0x1018:01, r-r-r-, uint32, 32 bit, "Vendor ID"
  0x1018:02, r-r-r-, uint32, 32 bit, "Product code"
  0x1018:03, r-r-r-, uint32, 32 bit, "Revision"
  0x1018:04, r-r-r-, uint32, 32 bit, "Serial number"

5. 查看主机信息
ethercat master
Master0
  Phase: Idle
  Active: no
  Slaves: 1
  Ethernet devices:
    Main: e4:f3:f5:c6:4e:b5 (attached)
      Link: UP
      Tx frames:   44056
      Tx bytes:    3942496
      Rx frames:   44055
      Rx bytes:    3942436
      Tx errors:   0
      Tx frame rate [1/s]:    100    100    100
      Tx rate [KByte/s]:      5.9    5.8    6.0
      Rx frame rate [1/s]:    100    100    100
      Rx rate [KByte/s]:      5.9    5.8    6.0
    Common:
      Tx frames:   44056
      Tx bytes:    3942496
      Rx frames:   44055
      Rx bytes:    3942436
      Lost frames: 0
      Tx frame rate [1/s]:    100    100    100
      Tx rate [KByte/s]:      5.9    5.8    6.0
      Rx frame rate [1/s]:    100    100   4340
      Rx rate [KByte/s]:      5.9    5.9    6.0
      Loss rate [1/s]:          0     -0      0
      Frame loss [%]:         0.0   -0.0    0.0
  Distributed clocks:
    Reference clock: Slave 0
    Application time: 0
                      2000-01-01 00:00:00.000000000

6.更改别名
ethercat alias 0x0001
ethercat slaves
0  1:0  PREOP  +  R ADVANCED MODEL with EtherCAT(P0002813G01)
别名一旦更改可以保存在驱动器E2PROM中,配置好后可以下次直接使用，多轴控制的时候可以配置不同的别名信息区分不同的轴

7. 操作模式设置 (CoE INDEX:SUBINDEX 0x0600:0x00 )
设置
ethercat download -a 0 -p 0 0x6060 0x00 0x01
读取
ethercat upload -a 0 -p 0 0x6061 0x00

8. 从机状态设置
ethercat state -a 0 -p 0 OP

9. 自由运行模式(手册P221)
设置自由运行模式(FreeRun mode)
root@linaro-ubuntu-desktop:/home/linaro/ftp/wuliang# ethercat download -a 0 -p 0 -t uint8 0x1c32 0x01 0x00

10. DC模式不能进入OP模式的可能原因？
DC模式一般用于实时性较高的场合,主站实时性要求非常高，一般高实时性场合Slaves要求时间抖动不超过100us

11. 通过ethercat让电机力矩

11.1 设置自由模式
ethercat download -a 0 -p 0 -t uint8 0x1c32 0x01 0x00
设置力矩模式:
ethercat download -a 0 -p 0 -t uint8   0x6060 0x00 0x04

11.2 设置驱动器成'OP'模式
ethercat states -a 0 -p 0 OP

11.3 伺服ON 
root@linaro-ubuntu-desktop:/home/output/bin# ethercat download -a 0 -p 0 -t uint8   0x6040 0x00 0x06
root@linaro-ubuntu-desktop:/home/output/bin# ethercat download -a 0 -p 0 -t uint8   0x6040 0x00 0x07
root@linaro-ubuntu-desktop:/home/output/bin# ethercat download -a 0 -p 0 -t uint8   0x6040 0x00 0x0f

11.4 设置目标力矩电机开始转动（山羊电机手册P270）
以正力矩转动:
ethercat download -a 0 -p 0 -t uint16  0x6071 0x00 0x001c
以负力矩转动:
ethercat download -a 0 -p 0 -t uint16  0x6071 0x00 0xffe4

11.5 停止电机转动
ethercat download -a 0 -p 0 -t uint8  0x6040 0x00 0x0f
ethercat download -a 0 -p 0 -t uint8  0x6040 0x00 0x06

12.设置例句模式问题
OP模式下进行一些对象字典设置会有如下报错:
root@linaro-ubuntu-desktop:/home/linaro/ftp/ethercat-1.5.2/test# ethercat download -a 0 -p 0 -t uint8   0x6060 0x00 0x04
EtherCAT ERROR 0-0: Reception of CoE download response failed: No response.
EtherCAT ERROR 0-0: Failed to process SDO request.
Failed to download SDO: Input/output error
原因是OP模式不能更改一些寄存器器的值所以会包上述错误

13.非'OP'模式力矩测试问题
经测试发现非'OP'模式下也可以设置电机力矩控制

14. 数字输出OUT1,OUT2控制

Ethercat控制OUT1输出
ethercat download -a 0 -p 0   -t uint32  0x60fe 0x01  0x00010000
Ethercat控制OUT2输出
ethercat download -a 0 -p 0   -t uint32  0x60fe 0x01  0x00020000
Ethercat同时控制OUT1,OUT2输出
ethercat download -a 0 -p 0   -t uint32  0x60fe 0x01  0x00030000

OUT1输出ON(刹车松开)
ethercat download -a 0 -p 0   -t uint8  0x20f9 0x02   0x42
OUT1输出OFF((刹车抱死))
ethercat download -a 0 -p 0   -t uint8  0x20f9 0x02   0x43
OUT2输出ON
ethercat download -a 0 -p 0   -t uint8  0x20f9 0x02   0x44
OUT2输出OFF
ethercat download -a 0 -p 0   -t uint8  0x20f9 0x02   0x45


14.
OUTPUT-----RxPdo 映射对象字典原始数据读取
ethercat upload -a 0 -p 0 -t uint16 0x6040 0x00
ethercat upload -a 0 -p 0 -t uint32 0x607a 0x00
ethercat upload -a 0 -p 0 -t uint32 0x6081 0x00
ethercat upload -a 0 -p 0 -t uint32 0x6083 0x00
ethercat upload -a 0 -p 0 -t uint32 0x6084 0x00
ethercat upload -a 0 -p 0 -t uint32 0x60ff 0x00
ethercat upload -a 0 -p 0 -t uint16 0x6071 0x00
ethercat upload -a 0 -p 0 -t uint16 0x60b8 0x00
ethercat upload -a 0 -p 0 -t uint32 0x60fe 0x01

 
INPUT-----TxPdo 映射对象字典原始数据读取
ethercat upload -a 0 -p 0 -t uint16 0x6041 0x00 
ethercat upload -a 0 -p 0 -t uint16 0x2100 0x00 
ethercat upload -a 0 -p 0 -t uint32 0x6064 0x00
ethercat upload -a 0 -p 0 -t uint32 0x606c 0x00
ethercat upload -a 0 -p 0 -t uint16 0x6077 0x00
ethercat upload -a 0 -p 0 -t uint32 0x60f4 0x00
ethercat upload -a 0 -p 0 -t uint16 0x60b9 0x00
ethercat upload -a 0 -p 0 -t uint32 0x60ba 0x00
ethercat upload -a 0 -p 0 -t uint32 0x60bb 0x00
ethercat upload -a 0 -p 0 -t uint32 0x60fd 0x00        
ethercat upload -a 0 -p 0 -t uint8  0x1001 0x00
ethercat upload -a 0 -p 0 -t uint8  0x6061 0x00

15.发生报警时根据手册P420页内容逐步排查原因 
15.1 任务处理异常

报警代码        描述
F1              任务处理异常
0x603f:0x8700
0x2101/0x2102 
针对问题15.1的解决办法:
(1)要先配0x6040:0x00=0x06,让伺服驱动处于ShutDown状态 山羊电机手册 P229
(2)没有进行pdo映射的对象字典要通过sdo方式先进行配置
(3)sdo配置对象字典要在主站激活前进行
(4)所有配置完成，主机激活完成通过PDO方式配置0x6040:0x00=0x07,使能主机 



************************************************************************************************************
泰科电机调试
(1)获取VENDOR_ID(手册P212查看索引及子索引)
ethercat upload -a 1 -p 0 0x1018 0x01
0x000001b9 441
(2)获取PRODUCT_ID(手册P212查看索引及子索引)
ethercat upload -a 1 -p 0 0x1018 0x02
0x00000002 2

------------------------------------------------------------------------------------------------------------
TwinCat 配置泰科伺服
1， 要在配置模式进行对象字典的配置

伺服使能状态字对比
root@linaro-ubuntu-desktop:/home/linaro/ftp/ethercat-1.5.2/test# ethercat download -a 255 -p 0 -t uint8   0x6060 0x00 0x04
root@linaro-ubuntu-desktop:/home/linaro/ftp/ethercat-1.5.2/test# ethercat download -a 255 -p 0 -t uint16   0x6040 0x00 0x06
root@linaro-ubuntu-desktop:/home/linaro/ftp/ethercat-1.5.2/test# ethercat upload -a 255 -p 0 -t uint16   0x6041 0x00
0x0631 1585
root@linaro-ubuntu-desktop:/home/linaro/ftp/ethercat-1.5.2/test# ethercat download -a 255 -p 0 -t uint16   0x6040 0x00 0x07
root@linaro-ubuntu-desktop:/home/linaro/ftp/ethercat-1.5.2/test# ethercat upload -a 255 -p 0 -t uint16   0x6041 0x00
0x0633 1587
root@linaro-ubuntu-desktop:/home/linaro/ftp/ethercat-1.5.2/test# ethercat download -a 255 -p 0 -t uint16   0x6040 0x00 0x0f
root@linaro-ubuntu-desktop:/home/linaro/ftp/ethercat-1.5.2/test# ethercat upload -a 255 -p 0 -t uint16   0x6041 0x00
0x0637 1591

------------------------------------------------------------------------------------------------------------
重启后:
root@linaro-ubuntu-desktop:~# ethercat upload -a 255 -p 0 -t uint16   0x6041 0x00
0x0670 1648
通过调试软件使能JOG后:
root@linaro-ubuntu-desktop:~# ethercat upload -a 255 -p 0 -t uint16   0x6041 0x00
0x0537 1335
命令行使能后:
ethercat upload -a 255 -p 0 -t uint16   0x6041 0x00
0x0737 1847


0x6076 额定输出力矩值
最大力矩 = 额定输出力矩值 * 3
泰科模组Ethercat调试

——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
可以正常以力矩模式控制泰科电机的命令序列
0.设置自由模式
ethercat download -a 255 -p 0 -t uint16 0x1c32 0x01 0x00
ethercat states -a 255 -p 0 OP

1.设置力矩模式
ethercat download -a 255 -p 0 -t uint8   0x6060 0x00 0x04

2.设置力矩上升斜率
ethercat download -a 255 -p 0 -t int32   0x6087 0x00 1000

3.伺服ON
ethercat download -a 255 -p 0 -t uint16   0x6040 0x00 0x06
ethercat download -a 255 -p 0 -t uint16   0x6040 0x00 0x07
ethercat download -a 255 -p 0 -t uint16   0x6040 0x00 0x0f

4. 设置输出目标力矩,电机旋转
ethercat download -a 255 -p 0 -t int16  0x6071 0x00 100

5.输出力矩0电机
ethercat download -a 255 -p 0 -t int16  0x6071 0x00 0
——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

关于OP模式的几点要点
1.进行PDO映射的对象字典要在OP模式下更改才会生效，否则可能即使更改成功了也会不起作用
2.没有进行PDO映射的对象字典在OP模式下读写可能会发生错误

————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
对象字典使用PDO还是SDO方式进行操作?
	SDO一般用于配置从站信息，PDO方式一般用于同从站进行实时数据交互，SDO方式主从站需要交互确认机制因而费时。
	

————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

EthercatIO板调试

root@linaro-ubuntu-desktop:~# ethercat pdos -a 0 -p 0
SM0: PhysAddr 0x1000, DefaultSize  128, ControlRegister 0x26, Enable 1
SM1: PhysAddr 0x1080, DefaultSize  128, ControlRegister 0x22, Enable 1
SM2: PhysAddr 0x1100, DefaultSize    2, ControlRegister 0x64, Enable 1
  RxPDO 0x1601 "DO RxPDO-Map"
    PDO entry 0x7010:01,  1 bit, "LED 1"
    PDO entry 0x7010:02,  1 bit, "LED 2"
    PDO entry 0x7010:03,  1 bit, "LED 3"
    PDO entry 0x7010:04,  1 bit, "LED 4"
    PDO entry 0x7010:05,  1 bit, "LED 5"
    PDO entry 0x7010:06,  1 bit, "LED 6"
    PDO entry 0x7010:07,  1 bit, "LED 7"
    PDO entry 0x7010:08,  1 bit, "LED 8"
    PDO entry 0x0000:00,  8 bit, "Gap"
SM3: PhysAddr 0x1400, DefaultSize    6, ControlRegister 0x20, Enable 1
  TxPDO 0x1a00 "DI TxPDO-Map"
    PDO entry 0x6000:01,  1 bit, "Switch 1"
    PDO entry 0x6000:02,  1 bit, "Switch 2"
    PDO entry 0x6000:03,  1 bit, "Switch 3"
    PDO entry 0x6000:04,  1 bit, "Switch 4"
    PDO entry 0x6000:05,  1 bit, "Switch 5"
    PDO entry 0x6000:06,  1 bit, "Switch 6"
    PDO entry 0x6000:07,  1 bit, "Switch 7"
    PDO entry 0x6000:08,  1 bit, "Switch 8"
    PDO entry 0x0000:00,  8 bit, "Gap"
  TxPDO 0x1a02 "AI TxPDO-Map"
    PDO entry 0x6020:01,  1 bit, "Underrange"
    PDO entry 0x6020:02,  1 bit, "Overrange"
    PDO entry 0x6020:03,  2 bit, "Limit 1"
    PDO entry 0x6020:05,  2 bit, "Limit 2"
    PDO entry 0x0000:00,  8 bit, "Gap"
    PDO entry 0x1802:07,  1 bit, "TxPDOState"
    PDO entry 0x1802:09,  1 bit, "TxPDO Toggle"
    PDO entry 0x6020:11, 16 bit, "Analog input"
    PDO entry 0x6020:12, 16 bit, "Temp"
    PDO entry 0x6020:13, 16 bit, "Hum"
	
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PDO方式控制问题的解决:
对象字典0x0000:0x00 为填充对象， 不能在ecrt_domain_reg_pdo_entry_list注册，也就是不能出现在ec_pdo_entry_reg_t board0_domain_regs[]数组定义里，
但是为了匹配队形E2PROM中PDO映射关系要在ec_pdo_entry_info_t slave_0_pdo_entries[]定义

PDO数据命令行显示：
ethercat data -a 0 -p 0 | hd
ethercat domains -a 0 -p 0 -v //更详细会显示哪个PDO管理器

const static ec_pdo_entry_reg_t board0_domain_regs[] = {
	/*Leds*/
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x01, //子索引 
		&off_bytes_board0_led1_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led1_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x02, //子索引 
		&off_bytes_board0_led2_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led2_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x03, //子索引 
		&off_bytes_board0_led3_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led3_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x04, //子索引 
		&off_bytes_board0_led4_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led4_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x05, //子索引 
		&off_bytes_board0_led5_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led5_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x06, //子索引 
		&off_bytes_board0_led6_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led6_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x07, //子索引 
		&off_bytes_board0_led7_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led7_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x08, //子索引 
		&off_bytes_board0_led8_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led8_val
	},
	
	//一定要注意，对象字典0x0000:0x00 为填充对象，不能注册
	/*
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x0000,                          //索引
		0x00, //子索引 
		&off_bytes_board0_ledgap,       //PDO入口在process_data字节偏移量
		&off_bits_board0_ledgap
	},
	*/

		
	/*switchs*/
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x01, //子索引 
		&off_bytes_board0_switch1_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch1_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x02, //子索引 
		&off_bytes_board0_switch2_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch2_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x03, //子索引 
		&off_bytes_board0_switch3_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch3_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x04, //子索引 
		&off_bytes_board0_switch4_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch4_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x05, //子索引 
		&off_bytes_board0_switch5_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch5_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x06, //子索引 
		&off_bytes_board0_switch6_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch6_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x07, //子索引 
		&off_bytes_board0_switch7_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch7_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x08, //子索引 
		&off_bytes_board0_switch8_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch8_status
	},
	
	//一定要注意，对象字典0x0000:0x00 为填充对象，不能注册
	/*
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x0000,                          //索引
		0x00, //子索引 
		&off_bytes_board0_switchgap,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switchgap
	},
	*/
	
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x01, //子索引 
		&off_bytes_board0_underrange_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_underrange_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x02, //子索引 
		&off_bytes_board0_overrange_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_overrange_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x03, //子索引 
		&off_bytes_board0_limit1_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_limit1_status
	},

	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x05, //子索引 
		&off_bytes_board0_limit2_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_limit2_status
	},
	
	//一定要注意，对象字典0x0000:0x00 为填充对象，不能注册
	/*
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x0000,                          //索引
		0x00, //子索引 
		&off_bytes_board0_limitgap,//PDO入口在process_data字节偏移量
		&off_bits_board0_limitgap
	},
	*/

	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x1802,                          //索引
		0x07, //子索引 
		&off_bytes_board0_TxPDOState_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_TxPDOState_status
	},
	
	
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x1802,                          //索引
		0x09, //子索引 
		&off_bytes_board0_TxPDO_Toggle_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_TxPDO_Toggle_status
	},
	
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x11, //子索引 
		&off_bytes_board0_Analog_input_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_Analog_input_status
	},
	
	
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x12, //子索引 
		&off_bytes_board0_Temp_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_Temp_val
	},
	
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x13, //子索引 
		&off_bytes_board0_Hum_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_Hum_val
	},
	{
	}
}; 

ec_pdo_entry_info_t slave_0_pdo_entries[] = {
    {0x7010, 0x01, 1}, /* LED 1 */
    {0x7010, 0x02, 1}, /* LED 2 */
    {0x7010, 0x03, 1}, /* LED 3 */
    {0x7010, 0x04, 1}, /* LED 4 */
    {0x7010, 0x05, 1}, /* LED 5 */
    {0x7010, 0x06, 1}, /* LED 6 */
    {0x7010, 0x07, 1}, /* LED 7 */
    {0x7010, 0x08, 1}, /* LED 8 */
    {0x0000, 0x00, 8}, /* Gap */ //一定要注意，对象字典0x0000:0x00 为填充对象，不能在ecrt_domain_reg_pdo_entry_list注册， 但是要在此处定义
    {0x6000, 0x01, 1}, /* Switch 1 */
    {0x6000, 0x02, 1}, /* Switch 2 */
    {0x6000, 0x03, 1}, /* Switch 3 */
    {0x6000, 0x04, 1}, /* Switch 4 */
    {0x6000, 0x05, 1}, /* Switch 5 */
    {0x6000, 0x06, 1}, /* Switch 6 */
    {0x6000, 0x07, 1}, /* Switch 7 */
    {0x6000, 0x08, 1}, /* Switch 8 */
    {0x0000, 0x00, 8}, /* Gap */ //一定要注意，对象字典0x0000:0x00 为填充对象，不能在ecrt_domain_reg_pdo_entry_list注册， 但是要在此处定义
    {0x6020, 0x01, 1}, /* Underrange */
    {0x6020, 0x02, 1}, /* Overrange */
    {0x6020, 0x03, 2}, /* Limit 1 */
    {0x6020, 0x05, 2}, /* Limit 2 */
    {0x0000, 0x00, 8}, /* Gap */ //一定要注意，对象字典0x0000:0x00 为填充对象，不能在ecrt_domain_reg_pdo_entry_list注册， 但是要在此处定义
    {0x1802, 0x07, 1}, /* TxPDOState */
    {0x1802, 0x09, 1}, /* TxPDO Toggle */
    {0x6020, 0x11, 16}, /* Analog input */
    {0x6020, 0x12, 16}, /* Temp */
    {0x6020, 0x13, 16}, /* Hum */
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
root@linaro-ubuntu-desktop:~# ethercat sdos
SDO 0x1000, "Device type"
  0x1000:00, r-r-r-, uint32, 32 bit, "Device type"
SDO 0x1001, "Error register"
  0x1001:00, r-r-r-, uint8, 8 bit, "Error register"
SDO 0x1008, "Device name"
  0x1008:00, r-r-r-, string, 80 bit, "Device name"
SDO 0x1009, "Hardware version"
  0x1009:00, r-r-r-, string, 32 bit, "Hardware version"
SDO 0x100a, "Software version"
  0x100a:00, r-r-r-, string, 32 bit, "Software version"
SDO 0x1018, "Identity"
  0x1018:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1018:01, r-r-r-, uint32, 32 bit, "Vendor ID"
  0x1018:02, r-r-r-, uint32, 32 bit, "Product code"
  0x1018:03, r-r-r-, uint32, 32 bit, "Revision"
  0x1018:04, r-r-r-, uint32, 32 bit, "Serial number"
SDO 0x10f1, "Error Settings"
  0x10f1:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x10f1:01, rwrwrw, uint32, 32 bit, "Local Error Reaction"
  0x10f1:02, rwrwrw, uint16, 16 bit, "Sync Error Counter Limit"
SDO 0x1601, "DO RxPDO-Map"
  0x1601:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1601:01, r-r-r-, uint32, 32 bit, "SubIndex 001"
  0x1601:02, r-r-r-, uint32, 32 bit, "SubIndex 002"
  0x1601:03, r-r-r-, uint32, 32 bit, "SubIndex 003"
  0x1601:04, r-r-r-, uint32, 32 bit, "SubIndex 004"
  0x1601:05, r-r-r-, uint32, 32 bit, "SubIndex 005"
  0x1601:06, r-r-r-, uint32, 32 bit, "SubIndex 006"
  0x1601:07, r-r-r-, uint32, 32 bit, "SubIndex 007"
  0x1601:08, r-r-r-, uint32, 32 bit, "SubIndex 008"
  0x1601:09, r-r-r-, uint32, 32 bit, "SubIndex 009"
SDO 0x1802, "TxPDO Parameter"
  0x1802:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1802:01, ------, type 0000, 0 bit, "SubIndex 001"
  0x1802:02, ------, type 0000, 0 bit, "SubIndex 002"
  0x1802:03, ------, type 0000, 0 bit, "SubIndex 003"
  0x1802:04, ------, type 0000, 0 bit, "SubIndex 004"
  0x1802:05, ------, type 0000, 0 bit, "SubIndex 005"
  0x1802:06, r-r-r-, octet_string, 0 bit, "Exclude TxPDOs"
  0x1802:07, r-r-r-, bool, 1 bit, "TxPDOState"
  0x1802:08, ------, type 0000, 0 bit, "SubIndex 008"
  0x1802:09, r-r-r-, bool, 1 bit, "TxPDO Toggle"
SDO 0x1a00, "DI TxPDO-Map"
  0x1a00:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1a00:01, r-r-r-, uint32, 32 bit, "SubIndex 001"
  0x1a00:02, r-r-r-, uint32, 32 bit, "SubIndex 002"
  0x1a00:03, r-r-r-, uint32, 32 bit, "SubIndex 003"
  0x1a00:04, r-r-r-, uint32, 32 bit, "SubIndex 004"
  0x1a00:05, r-r-r-, uint32, 32 bit, "SubIndex 005"
  0x1a00:06, r-r-r-, uint32, 32 bit, "SubIndex 006"
  0x1a00:07, r-r-r-, uint32, 32 bit, "SubIndex 007"
  0x1a00:08, r-r-r-, uint32, 32 bit, "SubIndex 008"
  0x1a00:09, r-r-r-, uint32, 32 bit, "SubIndex 009"
SDO 0x1a02, "AI TxPDO-Map"
  0x1a02:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1a02:01, r-r-r-, uint32, 32 bit, "SubIndex 001"
  0x1a02:02, r-r-r-, uint32, 32 bit, "SubIndex 002"
  0x1a02:03, r-r-r-, uint32, 32 bit, "SubIndex 003"
  0x1a02:04, r-r-r-, uint32, 32 bit, "SubIndex 004"
  0x1a02:05, r-r-r-, uint32, 32 bit, "SubIndex 005"
  0x1a02:06, r-r-r-, uint32, 32 bit, "SubIndex 006"
  0x1a02:07, r-r-r-, uint32, 32 bit, "SubIndex 007"
  0x1a02:08, r-r-r-, uint32, 32 bit, "SubIndex 008"
  0x1a02:09, r-r-r-, uint32, 32 bit, "SubIndex 009"
  0x1a02:0a, r-r-r-, uint32, 32 bit, "SubIndex 010"
SDO 0x1c00, "Sync manager type"
  0x1c00:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1c00:01, r-r-r-, uint8, 8 bit, "SubIndex 001"
  0x1c00:02, r-r-r-, uint8, 8 bit, "SubIndex 002"
  0x1c00:03, r-r-r-, uint8, 8 bit, "SubIndex 003"
  0x1c00:04, r-r-r-, uint8, 8 bit, "SubIndex 004"
SDO 0x1c12, "RxPDO assign"
  0x1c12:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1c12:01, r-r-r-, uint16, 16 bit, "SubIndex 001"
SDO 0x1c13, "TxPDO assign"
  0x1c13:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1c13:01, r-r-r-, uint16, 16 bit, "SubIndex 001"
  0x1c13:02, r-r-r-, uint16, 16 bit, "SubIndex 002"
SDO 0x1c32, "SM output parameter"
  0x1c32:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1c32:01, rwr-r-, uint16, 16 bit, "Synchronization Type"
  0x1c32:02, r-r-r-, uint32, 32 bit, "Cycle Time"
  0x1c32:03, ------, type 0000, 32 bit, "SubIndex 003"
  0x1c32:04, r-r-r-, uint16, 16 bit, "Synchronization Types supported"
  0x1c32:05, r-r-r-, uint32, 32 bit, "Minimum Cycle Time"
  0x1c32:06, r-r-r-, uint32, 32 bit, "Calc and Copy Time"
  0x1c32:07, ------, type 0000, 32 bit, "SubIndex 007"
  0x1c32:08, rwrwrw, uint16, 16 bit, "Get Cycle Time"
  0x1c32:09, r-r-r-, uint32, 32 bit, "Delay Time"
  0x1c32:0a, rwrwrw, uint32, 32 bit, "Sync0 Cycle Time"
  0x1c32:0b, r-r-r-, uint16, 16 bit, "SM-Event Missed"
  0x1c32:0c, r-r-r-, uint16, 16 bit, "Cycle Time Too Small"
  0x1c32:0d, ------, type 0000, 16 bit, "Shift Time Too Short"
  0x1c32:0e, ------, type 0000, 16 bit, "SubIndex 014"
  0x1c32:0f, ------, type 0000, 32 bit, "SubIndex 015"
  0x1c32:10, ------, type 0000, 32 bit, "SubIndex 016"
  0x1c32:11, ------, type 0000, 32 bit, "SubIndex 017"
  0x1c32:12, ------, type 0000, 32 bit, "SubIndex 018"
  0x1c32:13, ------, type 0000, 0 bit, "SubIndex 019"
  0x1c32:14, ------, type 0000, 0 bit, "SubIndex 020"
  0x1c32:15, ------, type 0000, 0 bit, "SubIndex 021"
  0x1c32:16, ------, type 0000, 0 bit, "SubIndex 022"
  0x1c32:17, ------, type 0000, 0 bit, "SubIndex 023"
  0x1c32:18, ------, type 0000, 0 bit, "SubIndex 024"
  0x1c32:19, ------, type 0000, 0 bit, "SubIndex 025"
  0x1c32:1a, ------, type 0000, 0 bit, "SubIndex 026"
  0x1c32:1b, ------, type 0000, 0 bit, "SubIndex 027"
  0x1c32:1c, ------, type 0000, 0 bit, "SubIndex 028"
  0x1c32:1d, ------, type 0000, 0 bit, "SubIndex 029"
  0x1c32:1e, ------, type 0000, 0 bit, "SubIndex 030"
  0x1c32:1f, ------, type 0000, 0 bit, "SubIndex 031"
  0x1c32:20, r-r-r-, bool, 1 bit, "Sync Error"
SDO 0x1c33, "SM input parameter"
  0x1c33:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x1c33:01, rwr-r-, uint16, 16 bit, "Synchronization Type"
  0x1c33:02, r-r-r-, uint32, 32 bit, "Cycle Time"
  0x1c33:03, ------, type 0000, 32 bit, "SubIndex 003"
  0x1c33:04, r-r-r-, uint16, 16 bit, "Synchronization Types supported"
  0x1c33:05, r-r-r-, uint32, 32 bit, "Minimum Cycle Time"
  0x1c33:06, r-r-r-, uint32, 32 bit, "Calc and Copy Time"
  0x1c33:07, ------, type 0000, 32 bit, "SubIndex 007"
  0x1c33:08, rwrwrw, uint16, 16 bit, "Get Cycle Time"
  0x1c33:09, r-r-r-, uint32, 32 bit, "Delay Time"
  0x1c33:0a, rwrwrw, uint32, 32 bit, "Sync0 Cycle Time"
  0x1c33:0b, r-r-r-, uint16, 16 bit, "SM-Event Missed"
  0x1c33:0c, r-r-r-, uint16, 16 bit, "Cycle Time Too Small"
  0x1c33:0d, ------, type 0000, 16 bit, "Shift Time Too Short"
  0x1c33:0e, ------, type 0000, 16 bit, "SubIndex 014"
  0x1c33:0f, ------, type 0000, 32 bit, "SubIndex 015"
  0x1c33:10, ------, type 0000, 32 bit, "SubIndex 016"
  0x1c33:11, ------, type 0000, 32 bit, "SubIndex 017"
  0x1c33:12, ------, type 0000, 32 bit, "SubIndex 018"
  0x1c33:13, ------, type 0000, 0 bit, "SubIndex 019"
  0x1c33:14, ------, type 0000, 0 bit, "SubIndex 020"
  0x1c33:15, ------, type 0000, 0 bit, "SubIndex 021"
  0x1c33:16, ------, type 0000, 0 bit, "SubIndex 022"
  0x1c33:17, ------, type 0000, 0 bit, "SubIndex 023"
  0x1c33:18, ------, type 0000, 0 bit, "SubIndex 024"
  0x1c33:19, ------, type 0000, 0 bit, "SubIndex 025"
  0x1c33:1a, ------, type 0000, 0 bit, "SubIndex 026"
  0x1c33:1b, ------, type 0000, 0 bit, "SubIndex 027"
  0x1c33:1c, ------, type 0000, 0 bit, "SubIndex 028"
  0x1c33:1d, ------, type 0000, 0 bit, "SubIndex 029"
  0x1c33:1e, ------, type 0000, 0 bit, "SubIndex 030"
  0x1c33:1f, ------, type 0000, 0 bit, "SubIndex 031"
  0x1c33:20, r-r-r-, bool, 1 bit, "Sync Error"
SDO 0x6000, "DI Inputs"
  0x6000:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x6000:01, r-r-r-, bool, 1 bit, "Switch 1"
  0x6000:02, r-r-r-, bool, 1 bit, "Switch 2"
  0x6000:03, r-r-r-, bool, 1 bit, "Switch 3"
  0x6000:04, r-r-r-, bool, 1 bit, "Switch 4"
  0x6000:05, r-r-r-, bool, 1 bit, "Switch 5"
  0x6000:06, r-r-r-, bool, 1 bit, "Switch 6"
  0x6000:07, r-r-r-, bool, 1 bit, "Switch 7"
  0x6000:08, r-r-r-, bool, 1 bit, "Switch 8"
SDO 0x6020, "AI Inputs"
  0x6020:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x6020:01, r-r-r-, bool, 1 bit, "Underrange"
  0x6020:02, r-r-r-, bool, 1 bit, "Overrange"
  0x6020:03, r-r-r-, type 0031, 2 bit, "Limit 1"
  0x6020:04, ------, type 0000, 0 bit, "SubIndex 004"
  0x6020:05, r-r-r-, type 0031, 2 bit, "Limit 2"
  0x6020:06, ------, type 0000, 2 bit, "SubIndex 006"
  0x6020:07, ------, type 0000, 6 bit, "SubIndex 007"
  0x6020:08, ------, type 0000, 0 bit, "SubIndex 008"
  0x6020:09, ------, type 0000, 0 bit, "SubIndex 009"
  0x6020:0a, ------, type 0000, 0 bit, "SubIndex 010"
  0x6020:0b, ------, type 0000, 0 bit, "SubIndex 011"
  0x6020:0c, ------, type 0000, 0 bit, "SubIndex 012"
  0x6020:0d, ------, type 0000, 0 bit, "SubIndex 013"
  0x6020:0e, ------, type 0000, 0 bit, "SubIndex 014"
  0x6020:0f, r-r-r-, bool, 1 bit, "TxPDO State"
  0x6020:10, r-r-r-, bool, 1 bit, "TxPDO Toggle"
  0x6020:11, r-r-r-, int16, 16 bit, "Analog input"
  0x6020:12, r-r-r-, int16, 16 bit, "Temp"
  0x6020:13, r-r-r-, int16, 16 bit, "Hum"
SDO 0x7010, "DO Outputs"
  0x7010:00, rwrwrw, uint8, 8 bit, "SubIndex 000"
  0x7010:01, rwrwrw, bool, 1 bit, "LED 1"
  0x7010:02, rwrwrw, bool, 1 bit, "LED 2"
  0x7010:03, rwrwrw, bool, 1 bit, "LED 3"
  0x7010:04, rwrwrw, bool, 1 bit, "LED 4"
  0x7010:05, rwrwrw, bool, 1 bit, "LED 5"
  0x7010:06, rwrwrw, bool, 1 bit, "LED 6"
  0x7010:07, rwrwrw, bool, 1 bit, "LED 7"
  0x7010:08, rwrwrw, bool, 1 bit, "LED 8"
SDO 0x8020, "AI Settings"
  0x8020:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0x8020:01, rwrwrw, bool, 1 bit, "Enable user scale"
  0x8020:02, rwrwrw, type 0800, 3 bit, "Presentation"
  0x8020:03, ------, type 0000, 0 bit, "SubIndex 003"
  0x8020:04, ------, type 0000, 0 bit, "SubIndex 004"
  0x8020:05, ------, type 0000, 2 bit, "SubIndex 005"
  0x8020:06, ------, type 0000, 0 bit, "SubIndex 006"
  0x8020:07, rwrwrw, bool, 1 bit, "Enable limit 1"
  0x8020:08, rwrwrw, bool, 1 bit, "Enable limit 2"
  0x8020:09, ------, type 0000, 8 bit, "SubIndex 009"
  0x8020:0a, ------, type 0000, 0 bit, "SubIndex 010"
  0x8020:0b, ------, type 0000, 0 bit, "SubIndex 011"
  0x8020:0c, ------, type 0000, 0 bit, "SubIndex 012"
  0x8020:0d, ------, type 0000, 0 bit, "SubIndex 013"
  0x8020:0e, ------, type 0000, 0 bit, "SubIndex 014"
  0x8020:0f, ------, type 0000, 0 bit, "SubIndex 015"
  0x8020:10, ------, type 0000, 0 bit, "SubIndex 016"
  0x8020:11, rwrwrw, int16, 16 bit, "Offset"
  0x8020:12, rwrwrw, int32, 32 bit, "Gain"
  0x8020:13, rwrwrw, int16, 16 bit, "Limit 1"
  0x8020:14, rwrwrw, int16, 16 bit, "Limit 2"
SDO 0xf000, "Modular device profile"
  0xf000:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0xf000:01, r-r-r-, uint16, 16 bit, "Module index distance"
  0xf000:02, r-r-r-, uint16, 16 bit, "Maximum number of modules"
SDO 0xf010, "Module profile list"
  0xf010:00, r-r-r-, uint8, 8 bit, "SubIndex 000"
  0xf010:01, r-r-r-, uint32, 32 bit, "SubIndex 001"
  0xf010:02, r-r-r-, uint32, 32 bit, "SubIndex 002"
  0xf010:03, r-r-r-, uint32, 32 bit, "SubIndex 003"

  
root@linaro-ubuntu-desktop:~# ethercat cstruct -a 0 -p 0
/* Master 0, Slave 0, "TempHumi"
 * Vendor ID:       0x00000017
 * Product code:    0x26483056
 * Revision number: 0x00020111
 */

ec_pdo_entry_info_t slave_0_pdo_entries[] = {
    {0x7010, 0x01, 1}, /* LED 1 */
    {0x7010, 0x02, 1}, /* LED 2 */
    {0x7010, 0x03, 1}, /* LED 3 */
    {0x7010, 0x04, 1}, /* LED 4 */
    {0x7010, 0x05, 1}, /* LED 5 */
    {0x7010, 0x06, 1}, /* LED 6 */
    {0x7010, 0x07, 1}, /* LED 7 */
    {0x7010, 0x08, 1}, /* LED 8 */
    {0x0000, 0x00, 8}, /* Gap */
    {0x6000, 0x01, 1}, /* Switch 1 */
    {0x6000, 0x02, 1}, /* Switch 2 */
    {0x6000, 0x03, 1}, /* Switch 3 */
    {0x6000, 0x04, 1}, /* Switch 4 */
    {0x6000, 0x05, 1}, /* Switch 5 */
    {0x6000, 0x06, 1}, /* Switch 6 */
    {0x6000, 0x07, 1}, /* Switch 7 */
    {0x6000, 0x08, 1}, /* Switch 8 */
    {0x0000, 0x00, 8}, /* Gap */
    {0x6020, 0x01, 1}, /* Underrange */
    {0x6020, 0x02, 1}, /* Overrange */
    {0x6020, 0x03, 2}, /* Limit 1 */
    {0x6020, 0x05, 2}, /* Limit 2 */
    {0x0000, 0x00, 8}, /* Gap */
    {0x1802, 0x07, 1}, /* TxPDOState */
    {0x1802, 0x09, 1}, /* TxPDO Toggle */
    {0x6020, 0x11, 16}, /* Analog input */
    {0x6020, 0x12, 16}, /* Temp */
    {0x6020, 0x13, 16}, /* Hum */
};

ec_pdo_info_t slave_0_pdos[] = {
    {0x1601, 9, slave_0_pdo_entries + 0}, /* DO RxPDO-Map */
    {0x1a00, 9, slave_0_pdo_entries + 9}, /* DI TxPDO-Map */
    {0x1a02, 10, slave_0_pdo_entries + 18}, /* AI TxPDO-Map */
};

ec_sync_info_t slave_0_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, slave_0_pdos + 0, EC_WD_ENABLE},
    {3, EC_DIR_INPUT, 2, slave_0_pdos + 1, EC_WD_DISABLE},
    {0xff}
};

****************************************************************************************************************************
wireshark  抓包分析
过滤器
ecat 
	ecat协议
	
ecat and ecat.cmd==0x0b
ecat and ecat.cmd==0x0c
	ecat协议 且cmd=0x0b(LWR)
	
ecat_mailbox.coe.sdoreq
	过滤SDO请求
```
# 1. libethercat\std 目录
	编译Igh生成的ethecat库

# 2. libethercat\include
	 Igh Master Ethercat库包含的头文件
	 
 # 3. scripts/
	自动生成从站信息的相关脚本文件，要生成从站信息，将该文件夹复制到开发板运行GenerateSlavesConfig.sh。
	默认生成ec_common_configs_define.h和ec_common_configs_define.c文件，这两个文件会在libethercat\ec_common\ecat_common.c,libethercat\ec_common\ecat_common.h相关接口使用。
		 
# 4. libethercat\ec_common
## 4.0 
```
	ec_common_configs_define.h
	ec_common_configs_define.c
	由脚本scripts/GenerateSlavesConfig.sh自动生成的从站信息，包括从站PDO,SDO设置
等，更具体的可以参照scripts/README.txt
```
## 4.1
```
   ecat_common.h
   ecat_common.c
   基于ibethercat\std 中的库的二次封装库,简化了PDO,SDO等操作
```

## 4.2 
```
   ecat_common_intermediate_interface.h
   ecat_common_intermediate_interface.c
   与具体厂商相关的接口库，基于ecat_common.c中接口的实现，不同设备修改这两个文件中的接口进行适配。
   目前该文件实现的山羊电机接口。主要实现的PDO接口的电机状态查询，上电开机，关机，操作模式设置，力矩设置等接口，不同电机的类似接口可以参照该文件实现,接口作用见名知意。
   封装接口的关键是对象字典的操作，根据手册设置对象字典即可，标准的Ethercat接口伺服电机一般来说对象字典定义基本是一样的，可能稍微有差别
```
   
# 5. 标准Igh Master接口的使用例子 
## mytest目录
```
   mytest/test_torque_sanyo_ioctl
        基于ioctl接口的sanyo电机测试例子
   mytest/test_torque_tec_ioctl
        基于ioctl接口的泰科电机电机测试例子
   mytest/test_torque_tec_lib
        基于libethercat\std库接口的电机测试例子
   mytest/test_torque2
        基于libethercat\std库接口的电机测试例子2
   mytest/test_io_board
		ET1100,IO板的测试例子
   基于标准的Igh 库及iotcl接口可以参照该文件夹例子
```
 
# 6.二次封装接口库ecat_common_intermediate_interface.c的使用说明

## 6.1使用步骤
```
(1)将编译生成的Igh库文件替换libethercat\std文件， 文件名可能要改成，或者不该也行，自己写Makefile时匹配库名称就行
(2)将 scripts/ 目录复制到开发运行GenerateSlavesConfig.sh脚本，将脚本生成的ec_common_configs_define.h ec_common_configs_define.h 复制到libethercat\ec_common 文件夹
(3)基于二次封装接口的电机一般操作步骤
  参照demo.c中的ethercat_init() 进行从站初始化
  初始化后就可以调用ecat_common_intermediate_interface.c中接口对从站进行操作
  比如
	interpolation_2_ecat_set_slave_pwr_on()接口使能电机，
	interpolation_2_ecat_set_slave_pwr_off() 关闭电机
	interpolation_2_ecat_set_slave_target_pos() 设置目标位置，对应位置模式操作
	其它接口作用见参照具体实现
```	
# 7.关于轴操作的几点说明
```
（1）每个轴对应一个从站，由alias，position确定，一般来说从站不多时alias=0固定不变，对不同轴根据positon确定。
	例如使能和关闭不同的轴
	int interpolation_2_ecat_set_slave_pwr_on(MasterSpecifiedInfo_T *master_specified_info, int slave_pos);
	int interpolation_2_ecat_set_slave_pwr_off(MasterSpecifiedInfo_T *master_specified_info, int slave_pos);
	中slave_pos参数就对应不同的轴，slave_pos=0,axis1 slave_pos=1,axis2...
（2）设置不同轴的操作模式,位置，力矩，速度模式
	int interpolation_2_ecat_set_slave_operation_model(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, unsigned char operation_model)
```
# Igh Master 1.5.2 源码
ethercat-1.5.2.zip

# Igh Master 1.5.2 ethercat-1.5.2/源码编译安装说明文件.txt 
```
EtherCAT 1.5.2 编译及使用说明.txt
1.交叉编译源码
	源码中有内核模块编译，指令定模块目录
	根据实际情况修改build_ethercat-1.5.2.sh中以下几个变量:
	output_dir='output'                               #编译输出目录
	module_install_dir='module_install'               #内核模块安装目录
	kernel_source_dir='/mnt/fs_ext/imx6/linux-3.0.35' #内核源码目录
	lib_modules_kernel_promt='3.0.35-2666-gbdde708'   #内核版本号
	host=arm-fsl-linux-gnueabi                        #交叉编译链前缀
	注意要先编译内核，然后编译Ethrecat因为Ethercat依赖于内核通用网卡模块
	以root用户运行./build_ethercat-1.5.2.sh           #编译具体根据提示选择y/n就可以了
----------------------------------------------------------------------------------------------------------------------------------------------
手动安装Ethercat到ARM开发板:
2.内核模块安装
2.1
	将output文件下通lib_modules_kernel_promt='3.0.35-2666-gbdde708'(build_ethercat-1.5.2.sh 中定义)放入开发板/lib/modules下
2.2
	开发板中执行"depmod"命令
3. output里面的其它文件放入开发板对应位置
4. 设置参数
	修改/etc/sysconfig/ethercat
	MASTER0_DEVICE="e4:f3:f5:c6:41:b6" #与ethercat绑定的Mac地址
	DEVICE_MODULES="generic"           #通用网卡就填generic，其余支持网卡换成模块名字就行
5.添加udev规则
	echo KERNEL==\"EtherCAT[0-9]*\", MODE=\"0664\" > /etc/udev/rules.d/99-EtherCAT.rules 
6. 启动服务
	/etc/init.d/ethercat restart
	出现以下信息表明移植成功
		Shutting down EtherCAT master 1.5.2  done
		Starting EtherCAT master 1.5.2 ec_generic: Binding socket to interface 3 (eth0).
		done
7. 应用层测试
	root@linaro-ubuntu-desktop:~# ethercat
	Please specify a command!

	Usage: ethercat <COMMAND> [OPTIONS] [ARGUMENTS]

	Commands (can be abbreviated):
	  alias      Write alias addresses.
	  config     Show slave configurations.
	  cstruct    Generate slave PDO information in C language.
	  data       Output binary domain process data.
	  debug      Set the master's debug level.
	  domains    Show configured domains.
	  download   Write an SDO entry to a slave.
	  eoe        Display Ethernet over EtherCAT statictics.
	  foe_read   Read a file from a slave via FoE.
	  foe_write  Store a file on a slave via FoE.
	  graph      Output the bus topology as a graph.
	  master     Show master and Ethernet device information.
	  pdos       List Sync managers, PDO assignment and mapping.
	  reg_read   Output a slave's register contents.
	  reg_write  Write data to a slave's registers.
	  rescan     Rescan the bus.
	  sdos       List SDO dictionaries.
	  sii_read   Output a slave's SII contents.
	  sii_write  Write SII contents to a slave.
	  slaves     Display slaves on the bus.
	  soe_read   Read an SoE IDN from a slave.
	  soe_write  Write an SoE IDN to a slave.
	  states     Request application-layer states.
	  upload     Read an SDO entry from a slave.
	  version    Show version information.
	  xml        Generate slave information XML.

	Global options:
	  --master  -m <master>  Comma separated list of masters
							 to select, ranges are allowed.
							 Examples: '1,3', '5-7,9', '-3'.
							 Default: '-' (all).
	  --force   -f           Force a command.
	  --quiet   -q           Output less information.
	  --verbose -v           Output more information.
	  --help    -h           Show this help.

	Numerical values can be specified either with decimal (no
	prefix), octal (prefix '0') or hexadecimal (prefix '0x') base.

	Call 'ethercat <COMMAND> --help' for command-specific help.
以上内容为基本的使用，进一步使用要结合ethercat说明文档和电机说明。
----------------------------------------------------------------------------------------------------------------------------------------------
自动安装Ethercat到ARM开发板:
    将output目录复制到开发板然后运行output目录下install_to_arm.sh脚本
	执行6，7步骤测试自动安装是否成功
----------------------------------------------------------------------------------------------------------------------------------------------
问题汇总:
(1)Starting EtherCAT master 1.5.2 EtherCAT ERROR: MAC address may not be empty.
FATAL: Error inserting ec_master (/lib/modules/3.0.35-2666-gbdde708-gbdbf2583/mnt/hgfs/win_linux_share/linux/download/ethercat-1.5.2/module_install/master/ec_master.ko): Invalid argument
failed
Mac地址为空，将对应的mac地址MASTER0_DEVICE="e4:f3:f5:c6:41:b6"
(2)编译Ethercat 一定要保证所用编译器与内核与硬件平台一致，否则会出现内核模块不能使用或者其它未知问题
(3)内核源码要先编译通过，否则Ethercat模块将不能编译通过，因为Ethercat模块依赖于内核本身驱动模块。
```