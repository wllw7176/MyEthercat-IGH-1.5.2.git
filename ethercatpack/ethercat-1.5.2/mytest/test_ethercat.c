#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "ecrt.h"
#include "../lib/ioctl.h"



/*以下几个宏定义参照山羊电机手册P212*/
//从设备别名,位置信息，根据手册定义
#define SANMOTION_RS2_ALIAS 0
#define SANMOTION_RS2_POSITION 0
#define SANMOTION_RS2_VENDORID  0x000001B9
#define SANMOTION_RS2_PRODUCTID 0x00000002
//待访问和设置PDO对象宏定义
#define SANMOTION_RS2_VENDORID_PDO_INDEX 0x1018
#define SANMOTION_RS2_VENDORID_PDO_SUBINDEX 0x01
#define SANMOTION_RS2_PRODUCTID_PDO_INDEX 0x1018
#define SANMOTION_RS2_PRODUCTID_PDO_SUBINDEX 0x02
//AL 模式定义
#define EC_AL_STATE_BOOT 0x03


//状态字符串，调试用
#define AL_STATE_STR(x) (x == EC_AL_STATE_INIT ? "AL_STATE_INIT" :\
					  x == EC_AL_STATE_PREOP ? "AL_STATE_PREOP" :\
					  x == EC_AL_STATE_SAFEOP ? "AL_STATE_SAFEOP" :\
					  x == EC_AL_STATE_OP ? "AL_STATE_OP":\
					  x == EC_AL_STATE_BOOT ? "AL_STATE_BOOT":\
					  "AL_STATE_UNKNOWN")
					  
/*****************************************************
typedef enum {
    EC_WC_ZERO = 0,   //**< No registered process data were exchanged. 
    EC_WC_INCOMPLETE, //**< Some of the registered process data were
                      //  exchanged. 
    EC_WC_COMPLETE    //**< All registered process data were exchanged. 
} ec_wc_state_t;
*****************************************************/
#define DOMAIN_STATE_STR(x) (x == EC_WC_ZERO ? "DOMAIN_STATE_WC_ZERO" : \
	                         x == EC_WC_INCOMPLETE ? "DOMAIN_STATE_WC_INCOMPLETE" : \
	                         x == EC_WC_COMPLETE ? "DOMAIN_STATE_WC_COMPLETE" : \
	                         "DOMAIN_STATE_UNKNOWN")



//电机运行模式
#define SANMOTION_R_AD_RUN_MODEL_FREE 0x00
#define SANMOTION_R_AD_RUN_MODEL_DC 0x01
//EtherCAT
static ec_master_t *master = NULL;
static ec_master_state_t master_state = {};
static ec_domain_t *domain1 = NULL;
static ec_domain_state_t domain1_state = {};
static ec_slave_config_t *sc_in = NULL;
static ec_slave_config_state_t sc_in_state = {};
//Timer
static unsigned int sig_alarms = 0;
static unsigned int user_alarms = 0;
//Application parameters
#define FREQUENCY 100
// process data
static uint8_t *domain1_pd = NULL;
ec_ioctl_slave_sdo_download_t sdo_download_dta;
ec_ioctl_slave_sdo_download_t sdo_upload_dta;

unsigned int off_vendorid;
unsigned int off_productid;

int master_fd = -1;

const static ec_pdo_entry_reg_t domain1_regs[] = {
    {
		SANMOTION_RS2_ALIAS,               //别名
		SANMOTION_RS2_POSITION,            //位置
		SANMOTION_RS2_VENDORID,            //厂商id
		SANMOTION_RS2_PRODUCTID,           //产品id
		SANMOTION_RS2_VENDORID_PDO_INDEX,  //索引
		SANMOTION_RS2_VENDORID_PDO_SUBINDEX,//子索引 
		&off_vendorid                                  //PDO入口在process_data字节偏移量
		//&off_vendorid_bit_position
	},
    {
    	SANMOTION_RS2_ALIAS, 
		SANMOTION_RS2_POSITION, 
		SANMOTION_RS2_VENDORID, 
		SANMOTION_RS2_PRODUCTID, 
		SANMOTION_RS2_PRODUCTID_PDO_INDEX, 
		SANMOTION_RS2_PRODUCTID_PDO_SUBINDEX, 
		&off_productid
		//&off_vendorid_bit_position
	},
    {
    }
};

//pdo 索引数据大小定义，数据大小见手册P212
static ec_pdo_entry_info_t sanmotion_r_ad_m0011098g_pdo_entries[] = {
    {SANMOTION_RS2_VENDORID_PDO_INDEX, SANMOTION_RS2_VENDORID_PDO_SUBINDEX,     32},  // vendorid value 
    {SANMOTION_RS2_PRODUCTID_PDO_INDEX, SANMOTION_RS2_PRODUCTID_PDO_SUBINDEX,   32}   // productid value
};

//PDO 信息,参照ethercat1.5.2 example/user/main.c 
//PDO 映射信息，还没弄懂具体映射规则，先使用如下定义测试
//PDO 映射分为RxPDO,TxPDO 具体见手册 P145
//在本次测试中,我们是要从驱动器获取数据,因而使用驱动器TxPDO的索引
static ec_pdo_info_t sanmotion_r_ad_m0011098g_pdos[] = {
    {0x1A00, 1, &sanmotion_r_ad_m0011098g_pdo_entries[0]},
    {0x1A01, 1, &sanmotion_r_ad_m0011098g_pdo_entries[1]},
};

//同步信息管理
static ec_sync_info_t sanmotion_r_ad_m0011098g_syncs[] = {
    {0, EC_DIR_OUTPUT, 2, sanmotion_r_ad_m0011098g_pdos},
    {1, EC_DIR_INPUT},
    {0xff}
};


/*******************************************************
*@return 0:download success -1:dowolad failed
********************************************************/
int sdoDownload(int fd, ec_ioctl_slave_sdo_download_t *data)
{
	int i;
	if (ioctl(fd, EC_IOCTL_SLAVE_SDO_DOWNLOAD, data) < 0) 
	{
        printf("SdoDownload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			printf("%02x", data->data [data->data_size-i-1]);
		}
		printf(" failed,ErrMsg:%s\n", strerror(errno));
		return -1;
    }
	else
	{
		printf("SdoDownload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			printf("%02x", data->data[data->data_size-i-1]);
		}
		printf(" success\n");
		return 0;
	}
}

/*******************************************************
*@return 0:download success -1:dowolad failed
********************************************************/
int sdoUpload(int fd, ec_ioctl_slave_sdo_download_t *data)
{
	int i;
	if (ioctl(fd, EC_IOCTL_SLAVE_SDO_UPLOAD, data) < 0) 
	{
        printf("SdoUpload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			printf("%02x", data->data[data->data_size-i-1]);
		}
		printf(" failed,ErrMsg:%s\n", strerror(errno));
		return -1;
    }
	else
	{
		printf("SdoUpload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			printf("%02x", data->data[data->data_size-i-1]);
		}
		printf(" success\n");
		return 0;
	}
}

void check_domain1_state(void)
{
    static ec_domain_state_t pre_ds;
	ec_domain_state_t ds;
    ecrt_domain_state(domain1, &ds);

	if (pre_ds.wc_state != ds.wc_state || pre_ds.working_counter != ds.working_counter || pre_ds.redundancy_active != ds.redundancy_active)
	{
		pre_ds = ds;
		printf("---------Domain1 WC:%d State:%s ReundancyActive:%d---------\n", ds.working_counter, DOMAIN_STATE_STR(ds.wc_state), ds.redundancy_active);
	}
   
}


void check_master_state(void)
{
	static ec_al_state_t pre_al_st;
	static unsigned int pre_link_st;
	ec_master_state_t ms;
	
    ecrt_master_state(master, &ms);

	if (ms.al_states != pre_al_st)
	{
		pre_al_st = ms.al_states;
		printf("MasterAlState:%s\n", AL_STATE_STR(ms.al_states));
	}
	if (ms.link_up != pre_link_st)
	{
		pre_link_st = ms.link_up;
		printf("LinkStatus:%s\n", ms.link_up ? "UP" : "DOWN");
	}

    
}

void check_slave_config_states(void)
{
	static ec_al_state_t pre_al_st;
	ec_slave_config_state_t s;
	ecrt_slave_config_state(sc_in, &s);
	if (s.al_state != pre_al_st)
	{
		printf("SlaveAlState:%s\n", AL_STATE_STR(s.al_state));
		pre_al_st = s.al_state;
	}
   
  	
}


void cyclic_task()
{
	static long int count = 0;
	while (1)
	{
		// receive process data
	    ecrt_master_receive(master);
	    ecrt_domain_process(domain1);
	    // send process data
	    ecrt_domain_queue(domain1);
	    ecrt_master_send(master);
	}
	
}


/****************************************************************************/

void signal_handler(int signum) 
{
    switch (signum) 
	{
        case SIGALRM:
            sig_alarms++;
			if (sig_alarms % 5 == 0)
			{
				check_slave_config_states();
				check_master_state();
				//check_domain1_state();
			}
            break;
    }
}


int main(int argc, char **argv)
{
    ec_slave_config_t *sc;
    struct sigaction sa;
    struct itimerval tv;
	unsigned char run_mode;
	unsigned char recv_buff[4];
	int err;
	/********************************************************************/
	if (argc !=2)
	{
		printf("Usage %s devname\n", argv[0]);
		return -1;
	}
	
	master_fd = open(argv[1], O_RDWR);
	if (master_fd < 0)
	{
		printf("Open file %s error \n", argv[1]);
		return -1;
	}
	/********************************************************************/
	//配置电机运行自由运行模式
	run_mode = SANMOTION_R_AD_RUN_MODEL_FREE;
	sdo_download_dta.slave_position = SANMOTION_RS2_POSITION;
	sdo_download_dta.sdo_index = 0x1c32;
	sdo_download_dta.sdo_entry_subindex = 0x01;
	sdo_download_dta.data = &run_mode;
	sdo_download_dta.data_size = 1;
	sdoDownload(master_fd, &sdo_download_dta);
	run_mode = SANMOTION_R_AD_RUN_MODEL_FREE;
	sdo_download_dta.slave_position = SANMOTION_RS2_POSITION;
	sdo_download_dta.sdo_index = 0x1c33;
	sdo_download_dta.sdo_entry_subindex = 0x01;
	sdo_download_dta.data = &run_mode;
	sdo_download_dta.data_size = 1;
	sdoDownload(master_fd, &sdo_download_dta);
	//获取productid及vendorid
	sdo_upload_dta.slave_position = SANMOTION_RS2_POSITION;
	sdo_upload_dta.sdo_index = SANMOTION_RS2_PRODUCTID_PDO_INDEX;
	sdo_upload_dta.sdo_entry_subindex = SANMOTION_RS2_PRODUCTID_PDO_SUBINDEX;
	sdo_upload_dta.data = recv_buff;
	sdo_upload_dta.data_size = 4;
	sdoUpload(master_fd, &sdo_upload_dta);
	sdo_upload_dta.slave_position = SANMOTION_RS2_POSITION;
	sdo_upload_dta.sdo_index = SANMOTION_RS2_VENDORID_PDO_INDEX;
	sdo_upload_dta.sdo_entry_subindex = SANMOTION_RS2_VENDORID_PDO_SUBINDEX;
	sdo_upload_dta.data = recv_buff;
	sdo_upload_dta.data_size = 4;
	sdoUpload(master_fd, &sdo_upload_dta);
	close(master_fd);
	/********************************************************************/
	//请求主机
    master = ecrt_request_master(0);
    if (NULL != master)
    {
		printf("Request master(0) success!\n");
	}
    else
   	{
		printf("Request master(0) failed!\n");
		return -1;
	}
    //请求域, 解释见IgH ethercatmaster手册
    domain1 = ecrt_master_create_domain(master);
    if (NULL != domain1)
    {
		printf("Master create domain success!\n");
	}
	else
	{
		printf("Master create domain failed!\n");
		return -1;
	}
    //获取从机配置
    if (!(sc_in = ecrt_master_slave_config(
                    master, SANMOTION_RS2_ALIAS, SANMOTION_RS2_POSITION, SANMOTION_RS2_VENDORID, SANMOTION_RS2_PRODUCTID))) 
    {
        fprintf(stderr, "Get slave configuration failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Get slave configuration success!\n");
	}
	//从机PDO配置
	//ecrt_slave_config_pdos与ecrt_master_slave_config成对出现校验从机是否配置成功
    printf("Configuring PDOs...\n");
    if (ecrt_slave_config_pdos(sc_in, EC_END, sanmotion_r_ad_m0011098g_syncs)) 
	{
        fprintf(stderr, "Configure PDOs failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Configure PDOs success!\n");
	}
	
    if (!(sc = ecrt_master_slave_config(master, SANMOTION_RS2_ALIAS, SANMOTION_RS2_POSITION, SANMOTION_RS2_VENDORID, SANMOTION_RS2_PRODUCTID))) 
    {
        fprintf(stderr, "Get slave configuration failed after configure pdos !\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Get slave configuration success after configure pdos !\n");
	}
	//从机pdos注册
    if (ecrt_slave_config_pdos(sc, EC_END, sanmotion_r_ad_m0011098g_syncs)) 
	{
        fprintf(stderr, "Slave configure PDOs failed!ErrMsg:%s\n", strerror(errno));
        return -1;
    }
	else
	{
		fprintf(stderr, "Slave configure PDOs configure success!\n");
	}
	//域注册
	if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs))
	{
        fprintf(stderr, "Reg PDO entry registration failed!ErrMsg:%s\n", strerror(errno));
        return -1;
    }
	else
	{
		fprintf(stderr, "Reg PDO entry registration success!\n");
	}
	//激活主机
    printf("Activating master...\n");
    if (ecrt_master_activate(master))
    {
		fprintf(stderr, "Activate master failed!\n");
		return -1;
	}
	else
	{
		fprintf(stderr, "Activate master success!\n");
	}
	//process data in domain1
    if (!(domain1_pd = ecrt_domain_data(domain1))) 
	{
		fprintf(stderr, "ecrt_domain_data(domain1) failed!\n");
		return -1;
    }
	else
	{
		fprintf(stderr, "ecrt_domain_data(domain1) success!\n");
	}
	/********************************************************************/
    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
    printf("Starting timer...\n");
	printf("**************Start test************\n");
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 100; //100ms
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 1000;
    if (setitimer(ITIMER_REAL, &tv, NULL)) {
        fprintf(stderr, "Failed to start timer: %s\n", strerror(errno));
        return 1;
    }
	/********************************************************************/
	//开始主循环
    while (1) {
         cyclic_task();
    }
    /********************************************************************/
    return 0;
}

