#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include "ecrt.h"
#include "../lib/ioctl.h"


//EtherCAT
static ec_master_t *master = NULL;
static ec_master_state_t master_state = {};

static ec_domain_t *domain1 = NULL;
static ec_domain_state_t domain1_state = {};

static ec_slave_config_t *sc_in = NULL;
static ec_slave_config_state_t sc_in_state = {};

//Timer
static unsigned int sig_alarms = 0;
static unsigned int user_alarms = 0;


//Application parameters
#define FREQUENCY 100


// process data
static uint8_t *domain1_pd = NULL;


#define EC_AL_STATE_BOOT 0x03
#define STATE_STR(x) (x == EC_AL_STATE_INIT ? "STATE_INIT" :\
					  x == EC_AL_STATE_PREOP ? "STATE_PREOP" :\
					  x == EC_AL_STATE_SAFEOP ? "STATE_SAFEOP" :\
					  x == EC_AL_STATE_OP ? "STATE_OP":\
					  x == EC_AL_STATE_BOOT ? "STATE_BOOT":\
					  "UNKNOWN_STATE")
					  

/*以下几个宏定义参照山羊电机手册P212*/
//从设备别名,位置信息，根据手册定义
#define SANMOTION_R_AD_M0011098G_ALIAS 0
#define SANMOTION_R_AD_M0011098G_POSITION 0
#define SANMOTION_R_AD_M0011098G_VENDORID  0x000001B9
#define SANMOTION_R_AD_M0011098G_PRODUCTID 0x00000002
//待访问和设置PDO对象宏定义
#define SANMOTION_R_AD_M0011098G_VENDORID_PDO_INDEX 0x1018
#define SANMOTION_R_AD_M0011098G_VENDORID_PDO_SUBINDEX 0x01
#define SANMOTION_R_AD_M0011098G_PRODUCTID_PDO_INDEX 0x1018
#define SANMOTION_R_AD_M0011098G_PRODUCTID_PDO_SUBINDEX 0x02

// offsets for PDO entries
static unsigned int off_vendorid;
static unsigned int off_vendorid_bit_position;
static unsigned int off_productid;
static unsigned int off_productid_bit_position;


//可以根据手册来定义,可能还可以用命令行工具生成 "ethercat cstruct"
//命令行工具生成待测试
const static ec_pdo_entry_reg_t domain1_regs[] = {
    {
		SANMOTION_R_AD_M0011098G_ALIAS,               //别名
		SANMOTION_R_AD_M0011098G_POSITION,            //位置
		SANMOTION_R_AD_M0011098G_VENDORID,            //厂商id
		SANMOTION_R_AD_M0011098G_PRODUCTID,           //产品id
		SANMOTION_R_AD_M0011098G_VENDORID_PDO_INDEX,  //索引
		SANMOTION_R_AD_M0011098G_VENDORID_PDO_SUBINDEX,//子索引 
		&off_vendorid                                  //PDO入口在process_data字节偏移量
		//&off_vendorid_bit_position
	},
    {
    	SANMOTION_R_AD_M0011098G_ALIAS, 
		SANMOTION_R_AD_M0011098G_POSITION, 
		SANMOTION_R_AD_M0011098G_VENDORID, 
		SANMOTION_R_AD_M0011098G_PRODUCTID, 
		SANMOTION_R_AD_M0011098G_PRODUCTID_PDO_INDEX, 
		SANMOTION_R_AD_M0011098G_PRODUCTID_PDO_SUBINDEX, 
		&off_productid
		//&off_vendorid_bit_position
	},
    {
    }
};

//pdo 索引数据大小定义，数据大小见手册P212
static ec_pdo_entry_info_t sanmotion_r_ad_m0011098g_pdo_entries[] = {
    {SANMOTION_R_AD_M0011098G_VENDORID_PDO_INDEX, SANMOTION_R_AD_M0011098G_VENDORID_PDO_SUBINDEX,     32},  // vendorid value 
    {SANMOTION_R_AD_M0011098G_PRODUCTID_PDO_INDEX, SANMOTION_R_AD_M0011098G_PRODUCTID_PDO_SUBINDEX,   32}   // productid value
};

//PDO 信息,参照ethercat1.5.2 example/user/main.c 
//PDO 映射信息，还没弄懂具体映射规则，先使用如下定义测试
static ec_pdo_info_t sanmotion_r_ad_m0011098g_pdos[] = {
    {SANMOTION_R_AD_M0011098G_VENDORID_PDO_INDEX, 1,  &sanmotion_r_ad_m0011098g_pdo_entries[0]},
    {SANMOTION_R_AD_M0011098G_PRODUCTID_PDO_INDEX, 1, &sanmotion_r_ad_m0011098g_pdo_entries[1]},
};

//同步信息管理
static ec_sync_info_t sanmotion_r_ad_m0011098g_syncs[] = {
    {0, EC_DIR_OUTPUT},
    {1, EC_DIR_INPUT, 2, sanmotion_r_ad_m0011098g_pdos},
    {0xff}
};


static unsigned int counter = 0;
static unsigned int blink = 0;

void check_domain1_state(void)
{
    ec_domain_state_t ds;

    ecrt_domain_state(domain1, &ds);

    if (ds.working_counter != domain1_state.working_counter)
        printf("Domain1: WC %u.\n", ds.working_counter);
    if (ds.wc_state != domain1_state.wc_state)
        printf("Domain1: State %u.\n", ds.wc_state);

    domain1_state = ds;
}


void check_master_state(void)
{
	static ec_al_state_t pre_al_st;
	static unsigned int pre_link_st;
	ec_master_state_t ms;
	
    ecrt_master_state(master, &ms);

	if (ms.al_states != pre_al_st)
	{
		pre_al_st = ms.al_states;
		printf("MasterAlState:%s\n", STATE_STR(ms.al_states));
	}
	if (ms.link_up != pre_link_st)
	{
		pre_link_st = ms.link_up;
		printf("LinkStatus:%s\n", ms.link_up ? "UP" : "DOWN");
	}

    
}

void check_slave_config_states(void)
{
	static ec_al_state_t pre_al_st;
	ec_slave_config_state_t s;
	ecrt_slave_config_state(sc_in, &s);
	if (s.al_state != pre_al_st)
	{
		printf("SlaveAlState:%s\n", STATE_STR(s.al_state));
		pre_al_st = s.al_state;
	}
   
  	
}


void cyclic_task()
{
	static long int count = 0;
	while (1)
	{
		// receive process data
	    ecrt_master_receive(master);
	    ecrt_domain_process(domain1);
	    // send process data
	    ecrt_domain_queue(domain1);
	    ecrt_master_send(master);
	}
	
}


/****************************************************************************/

void signal_handler(int signum) 
{
    switch (signum) 
	{
        case SIGALRM:
            sig_alarms++;
			if (sig_alarms % 5 == 0)
			{
				check_slave_config_states();
				check_master_state();
			}
            break;
    }
}


int main(int argc, char **argv)
{
    ec_slave_config_t *sc;
    struct sigaction sa;
    struct itimerval tv;

	//1.请求主机
    master = ecrt_request_master(0);
    if (NULL != master)
    {
		printf("Request master(0) success!");
	}
    else
   	{
		printf("Request master(0) failed!");
		return -1;
	}
    //2.请求域, 解释见IgH ethercatmaster手册
    domain1 = ecrt_master_create_domain(master);
    if (NULL != domain1)
    {
		printf("Maset create domain success!\n");
	}
	else
	{
		printf("Maset create domain failed!\n");
		return -1;
	}
    //3.获取从机配置
    if (!(sc_in = ecrt_master_slave_config(
                    master, SANMOTION_R_AD_M0011098G_ALIAS, SANMOTION_R_AD_M0011098G_POSITION, SANMOTION_R_AD_M0011098G_VENDORID, SANMOTION_R_AD_M0011098G_PRODUCTID))) 
    {
        fprintf(stderr, "Get slave configuration failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Get slave configuration success!\n");
	}
	//4.丛机PDO配置
	//ecrt_slave_config_pdos与ecrt_master_slave_config成对出现校验从机是否配置成功
    printf("Configuring PDOs...\n");
    if (ecrt_slave_config_pdos(sc_in, EC_END, sanmotion_r_ad_m0011098g_syncs)) 
	{
        fprintf(stderr, "Configure PDOs failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Configure PDOs success!\n");
	}

    if (!(sc = ecrt_master_slave_config(
                    master, SANMOTION_R_AD_M0011098G_ALIAS, SANMOTION_R_AD_M0011098G_POSITION, SANMOTION_R_AD_M0011098G_VENDORID, SANMOTION_R_AD_M0011098G_PRODUCTID))) 
    {
        fprintf(stderr, "Get slave configuration failed after configure pdos !\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Get slave configuration success after configure pdos !\n");
	}

	//5. 域注册
	/*
    if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs)) 
	{
        fprintf(stderr, "PDO entry registration failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "PDO entry registration success!\n");
	}
	*/
	//6. 激活主机
    printf("Activating master...\n");
    if (ecrt_master_activate(master))
    {
		fprintf(stderr, "Activate master failed!\n");
		return -1;
	}
	else
	{
		fprintf(stderr, "Activate master success!\n");
	}
	//process data in domain1
	/*
    if (!(domain1_pd = ecrt_domain_data(domain1))) 
	{
		fprintf(stderr, "ecrt_domain_data(domain1) failed!\n");
		return -1;
    }
	else
	{
		fprintf(stderr, "ecrt_domain_data(domain1) success!\n");
	}
	*/

    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
    printf("Starting timer...\n");
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 10; //100ms
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 1000;
    if (setitimer(ITIMER_REAL, &tv, NULL)) {
        fprintf(stderr, "Failed to start timer: %s\n", strerror(errno));
        return 1;
    }

	//开始主循环
    printf("Started test.\n");
    while (1) {
         cyclic_task();
    }

    return 0;
}






