var NAVTREE =
[
  [ "IgH EtherCAT Master", "index.html", [
    [ "General information", "index.html#sec_general", null ],
    [ "Contact", "index.html#sec_contact", null ],
    [ "License", "index.html#sec_license", null ],
    [ "Todo List", "todo.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"foe_8h.html#a1a3752c2c35892afeaf65cac5615c659a5bceb1cbe5096a97510b5f4f66121908",
"fsm__pdo__entry_8c.html#a133b853be4523479faa849c220581400",
"functions_vars_o.html",
"group__ApplicationInterface.html#ga7bc2683699a5c0c551f0cfdc90c1a559",
"group__TTYInterface.html#ga03d764370f5d76bec71412ea1124a156",
"module_8c.html#a91a626e2e5f82abd07434ca87dd28ee5",
"slave_8c.html#a3cc63d4432c2e733cce13c64308ec83d",
"structec__eoe.html#afd6bcfa6f1ad2bfb371439776b2ca8a9",
"structec__master__state__t.html#a3f7cdc7119df341803926360e65e3405",
"structec__sync__t.html#a581dfbd7e289ab43dc3f5b7f844c4776"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';