#!/bin/bash

source ./echo_color.sh
module_install_dir='module_install'
output_dir='output'
kernel_source_dir='/mnt/fs_ext/imx6/linux-3.0.35'
lib_modules_kernel_promt='3.0.35-2666-gbdde708'
host=arm-fsl-linux-gnueabi

#根据实时内核选项设置对应目录
echo_blue "Your LinuxKernel patch PREEMPT RT(Y/N)?[N]"
read rt_linux_flag
if [ ${rt_linux_flag} = 'y' -o ${rt_linux_flag} = 'Y' ];  then
	#echo_blue "Your LinuxKernel patch PREEMPT RT!!!"
	#kernel_source_dir='/mnt/fs_ext/linux-xlnx-xilinx-v2016.3_rt_patch' #实时内核目录
	#lib_modules_kernel_promt='4.6.0-rt14-xilinx'
	#echo_blue "kernel_source_dir='/mnt/fs_ext/linux-xlnx-xilinx-v2016.3_rt_patch'"
	#echo_blue "lib_modules_kernel_promt=4.6.0-rt14-xilinx"
	echo ""
else
	echo_blue "Linux didn't patch PREEMPT RT!!!"
	kernel_source_dir='/mnt/fs_ext/imx6/linux-3.0.35' #普通内核目录
	lib_modules_kernel_promt='3.0.35-2666-gbdde708'
	echo_blue "kernel_source_dir=$kernel_source_dir"
	echo_blue "lib_modules_kernel_promt=$lib_modules_kernel_promt"
fi

#用超级用户权限执行
if [ "`id -u`" -ne 0 ]; then
        echo_red "You should be root to run this script"
        exit 1
fi

if ! [ -d ${output_dir} ] ; then
	mkdir ${output_dir}
	echo_blue "Output idr not exitst, create it"
fi
if ! [ -d ${module_install_dir} ] ; then
	mkdir ${module_install_dir}
	echo_blue "Module_install idr not exitst, create it"
fi

if [  -e 'Makefile' ]; then
	echo_blue "Did you want to make clean?(Y/N)[N]"
	read ans
	case ${ans} in
		y);&
		Y)
		make clean
		if [ $? -eq 0 ] ; then
			echo_blue "Make clean finishied!!!"
		fi
		;;
	esac
fi



echo_blue "Confiugre start....."
echo_blue "Did you want to use debug configure options?(Y/N)[N]"
debug='N'
read debug
if [ ${debug} = 'y' -o ${debug} = 'Y' ] ; then
	echo_blue "Exec full configure..."
	./configure \
	--prefix=${PWD}/${output_dir} \
	--host=${host} \
	--with-linux-dir=${kernel_source_dir} \
	--enable-8139too=no \
	--enable-generic=yes \
	--enable-debug-if=yes \
	--enable-debug-ring=yes \ 
	--enable-wildcards=yes \ 
	--with-devices=5 
else
	#use simple config
	echo_blue "Exec simple configure..."
	./configure \
	--prefix=${PWD}/output \
	--with-linux-dir=${kernel_source_dir} \
	--enable-8139too=no \
	--enable-generic=yes \
	--host=${host} \
	--with-module-dir=${PWD}/${module_install_dir}
fi

echo_blue "Export ARCH=arm CROSS_COMPILE=arm-fsl-linux-gnueabi-"
export ARCH=arm
export CROSS_COMPILE=arm-fsl-linux-gnueabi-

if [ $? -eq 0 ] ; then
	echo_blue "Confiugre finish, make modules"
	make modules
else
	echo_red "Confiuge failed,exit!!! "
	exit 1
fi

echo_blue "Make modules_install..."
make  modules_install
if [ $? -eq 0 ] ; then
	echo_blue "Make modules_install success!!!"
	if [ $? -eq 0 ] ; then	
		cp -r /lib/modules/${lib_modules_kernel_promt}${PWD}/${module_install_dir} ${output_dir}
	fi	
else
	echo_red "Make modules_install failed, exit!!!"
	exit 1
fi

echo_blue "Make ..."
make 

echo_blue "Make install..."
make install 
if [ $? -eq 0 ] ; then
	echo_blue "cp -r /lib/modules/${lib_modules_kernel_promt}${PWD}/${module_install_dir} ${output_dir}"
	echo_blue "Make install success,build files in ${output_dir} and *.ko in ${module_install_dir}"
else
	echo_red "Make install failed"
	exit 1
fi

echo_blue "cp -r /lib/modules/${lib_modules_kernel_promt} ${output_dir}"
cp -r /lib/modules/${lib_modules_kernel_promt} ${output_dir}

if [ -e install_to_arm.sh ]; then
	cp install_to_arm.sh ${output_dir}
fi

echo_light_green "Finished all operations!!!"



