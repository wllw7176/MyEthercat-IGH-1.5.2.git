﻿文档目录结构及简要说明

0.Ethercat调试记录.txt
	调试Ethercat过程中碰到的问题及解决办法
	
1. libethercat\std 目录
	编译Igh生成的ethecat库

2. libethercat\include
	 Igh Master Ethercat库包含的头文件
	 
3. scripts/
	自动生成从站信息的相关脚本文件，要生成从站信息，将该文件夹复制到开发板运行GenerateSlavesConfig.sh。
	默认生成ec_common_configs_define.h和ec_common_configs_define.c文件，这两个文件会在libethercat\ec_common\ecat_common.c,libethercat\ec_common\ecat_common.h相关接口使用。
		 
4. libethercat\ec_common
4.0 
	ec_common_configs_define.h
	ec_common_configs_define.c
	由脚本scripts/GenerateSlavesConfig.sh自动生成的从站信息，包括从站PDO,SDO设置等，更具体的可以参照scripts/README.txt
4.1
   ecat_common.h
   ecat_common.c
   基于ibethercat\std 中的库的二次封装库,简化了PDO,SDO等操作
4.2 
   ecat_common_intermediate_interface.h
   ecat_common_intermediate_interface.c
   与具体厂商相关的接口库，基于ecat_common.c中接口的实现，不同设备修改这两个文件中的接口进行适配。
   目前该文件实现的山羊电机接口。主要实现的PDO接口的电机状态查询，上电开机，关机，操作模式设置，力矩设置等接口，不同电机的类似接口可以参照该文件实现,接口作用见名知意。
   封装接口的关键是对象字典的操作，根据手册设置对象字典即可，标准的Ethercat接口伺服电机一般来说对象字典定义基本是一样的，可能稍微有差别
   
5. 标准Igh Master接口的使用例子 
mytest目录
   mytest/test_torque_sanyo_ioctl
        基于ioctl接口的sanyo电机测试例子
   mytest/test_torque_tec_ioctl
        基于ioctl接口的泰科电机电机测试例子
   mytest/test_torque_tec_lib
        基于libethercat\std库接口的电机测试例子
   mytest/test_torque2
        基于libethercat\std库接口的电机测试例子2
   mytest/test_io_board
		ET1100,IO板的测试例子
   基于标准的Igh 库及iotcl接口可以参照该文件夹例子
   
6.二次封装接口库ecat_common_intermediate_interface.c的使用说明

使用步骤
(1)将编译生成的Igh库文件替换libethercat\std文件， 文件名可能要改成，或者不该也行，自己写Makefile时匹配库名称就行
(2)将 scripts/ 目录复制到开发运行GenerateSlavesConfig.sh脚本，将脚本生成的ec_common_configs_define.h ec_common_configs_define.h 复制到libethercat\ec_common 文件夹
(3)基于二次封装接口的电机一般操作步骤
  参照demo.c中的ethercat_init() 进行从站初始化
  初始化后就可以调用ecat_common_intermediate_interface.c中接口对从站进行操作
  比如
	interpolation_2_ecat_set_slave_pwr_on()接口使能电机，
	interpolation_2_ecat_set_slave_pwr_off() 关闭电机
	interpolation_2_ecat_set_slave_target_pos() 设置目标位置，对应位置模式操作
	其它接口作用见参照具体实现
	
7.关于轴操作的几点说明
（1）每个轴对应一个从站，由alias，position确定，一般来说从站不多时alias=0固定不变，对不同轴根据positon确定。
	例如使能和关闭不同的轴
	int interpolation_2_ecat_set_slave_pwr_on(MasterSpecifiedInfo_T *master_specified_info, int slave_pos);
	int interpolation_2_ecat_set_slave_pwr_off(MasterSpecifiedInfo_T *master_specified_info, int slave_pos);
	中slave_pos参数就对应不同的轴，slave_pos=0,axis1 slave_pos=1,axis2...
（2）设置不同轴的操作模式,位置，力矩，速度模式
	int interpolation_2_ecat_set_slave_operation_model(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, unsigned char operation_model)
  

	
   

