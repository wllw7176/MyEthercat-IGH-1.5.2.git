此sh脚本自动生成ethercat配置信息configs_define.h和configs_define.c文件
=========================================================================
configs_define.h
	1.自定义从站信息结构体SlaveSpecifiedInfo_T
	2.自定义主站信息结构体MasterSpecifiedInfo_T
	SlaveSpecifiedInfo_T和MasterSpecifiedInfo_T供ec_common_类函数调用
	
	3.从站pdo_entries,pdos,syns数组extern声明
	//slave 0 array definition
	extern ec_pdo_entry_info_t slave_0_pdo_entries[28];
	extern ec_pdo_info_t slave_0_pdos[3];
	extern ec_sync_info_t slave_0_syncs[5];
	...
	
	4.从站ID信息数组extern声明
	//Last element in slaves_id_info is '{-1,-1,-1, -1}'to indicate end of array
	//slaves_id_info[i] slave_i Id info
	//Size SLAVE_COUNT+1
	extern SlaveIdInfo_T slaves_id_info[3];
	
	5.从站识别信息extern声明
	extern SlaveSpecifiedInfo_T slaves_specified_info[3];
	
configs_define.c
	从站中所有数组的实际定义
=========================================================================
extern_define_file.txt 需要存入.h的内容 可以是typedef数组定义，注释或者其他东西
extern_define_file.txt 要与生成脚本在同一级目录 