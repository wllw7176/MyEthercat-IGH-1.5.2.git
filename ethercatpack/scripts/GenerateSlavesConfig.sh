#!/bin/bash

#Usaually,you don't need modify this file.

#prefix.c and prefix.h are used by our application written by c or c++
prefix='ec_common_configs_define'
include_file_name=$prefix.h
src_file_name=$prefix.c

#Used dof define macros in prefix.h
VENDOR_ID=`ethercat cstruct  | grep "Vendor ID"  | tail -1 | awk '{print $4}'`
PRODUCT_CODE=`ethercat cstruct  | grep "Product code"  | tail -1 | awk '{print $4}'`
SLAVE_COUNT=`ethercat slaves | wc -l`

#All length of slave_array_size[]
#Each 3 element in slave_array_size[] is one uint for one slave.
#eg.
#    slave_array_size[0] = length of array 'slave_0_pdo_entries[]'
#    slave_array_size[1] = length of array 'slave_0_pdos[]'
#    slave_array_size[2] = length of array 'slave_0_syncs[]'
#    slave_array_size[3] = length of array 'slave_1_pdo_entries[]'
#    slave_array_size[4] = length of array 'slave_2_pdos[]'
#    slave_array_size[5] = length of array 'slave_3_syncs[]'
#    ...
slave_array_size=''

slave_alias_arr=''
slave_position_arr=''
slave_vendorid_arr=''
slave_productcode_arr=''

function get_slave_id_array()
{
	 alias_str=`ethercat slaves | awk '{print $2}' | awk  -F ":"  '{print $1}'`
	 position_str=`ethercat slaves | awk '{print $2}' | awk  -F ":"  '{print $2}'`
	 vendor_id_str=`ethercat cstruct  | grep "Vendor ID"  | awk '{print $4}'`
	 productcode_str=`ethercat cstruct  | grep "Product code" | awk '{print $4}'`
	 
	 i=0
	 for x in ${alias_str[@]}
	 do
		slave_alias_arr[i]=$x
		let i++
	 done
	 
	 i=0
	 for x in ${position_str[@]}
	 do
		slave_position_arr[i]=$x
		let i++
	 done
	 
	 i=0
	 for x in ${vendor_id_str[@]}
	 do
		slave_vendorid_arr[i]=$x
		let i++
	 done
	 
	 i=0
	 for x in ${productcode_str[@]}
	 do
		slave_productcode_arr[i]=$x
		let i++
	 done
}

function get_slave_array_size()
{
	#Get length of array such as:'slave_1_pdo_entries[]  slave_1_pdos[] slave_1_syncs[]'
	
	#Get number of slaves
	slave_count=`ethercat slaves | wc -l`
	
	#Get line index of all end of array like 'slave_1_pdo_entries[]  slave_1_pdos[] slave_1_syncs[]'
	str=`ethercat cstruct  | grep -n "};" | awk -F [:] '{print $1}'`
	match_end_struct_index=`ethercat cstruct  | grep -n  "};" | awk -F [:] '{print $1}'`
	
	#Get values of slave_array_size[]
	i=0
	while [ $i -le $[ $slave_count - 1 ] ];
	do
		struct_start_line=`ethercat cstruct  | grep -n "slave_${i}_pdo_entries\[\]" | awk -F [:] '{print $1}'`
		j=0
		x=0
		for x in ${match_end_struct_index[@]}
		do
			if [ $j -ne $[$i*3+0] ]; then
				let j++
			else
				slave_array_size[$[$i*3+0]]=$[ $x - $struct_start_line - 1 ]
				break
			fi
		done
		
		struct_start_line=`ethercat cstruct  | grep -n "slave_${i}_pdos\[\]" | awk -F [:] '{print $1}'`
		j=0
		x=0
		for x in ${match_end_struct_index[@]}
		do
			if [ $j -ne $[$i*3+1] ]; then
				let j++
			else
				slave_array_size[$[$i*3+1]]=$[ $x - $struct_start_line - 1 ]
				break
			fi
		done
		
		struct_start_line=`ethercat cstruct  | grep -n "slave_${i}_syncs\[\]" | awk -F [:] '{print $1}'`
		j=0
		x=0
		for x in ${match_end_struct_index[@]}
		do
			if [ $j -ne $[$i*3+2] ]; then
				let j++
			else
				slave_array_size[$[$i*3+2]]=$[ $x - $struct_start_line - 1 ]
				break
			fi
		done
		
		let i++
	done
}

function generate_include_c_struct()
{
	slave_count=`ethercat slaves | wc -l`
	
	get_slave_array_size
	
	i=0
	
	while [ $i -le $[ $slave_count - 1 ] ];
	do
		echo "//slave ${i} array definition"                                             >>  $1
		j=0
		x=0
		for x in ${match_end_struct_index[@]}
		do
			if [ $j -ne $[$i*3+0] ]; then
				let j++
			else
				echo "extern ec_pdo_entry_info_t slave_${i}_pdo_entries[${slave_array_size[$[$i*3+0]]}];" >> $1
				break
			fi
		done
		j=0
		x=0
		for x in ${match_end_struct_index[@]}
		do
			if [ $j -ne $[$i*3+1] ]; then
				let j++
			else
				echo "extern ec_pdo_info_t slave_${i}_pdos[${slave_array_size[$[$i*3+1]]}];"  >> $1
				break
			fi
		done
		j=0
		x=0
		for x in ${match_end_struct_index[@]}
		do
			if [ $j -ne $[$i*3+2] ]; then
				let j++
			else
				echo "extern ec_sync_info_t slave_${i}_syncs[${slave_array_size[$[$i*3+2]]}];"  >> $1
				break
			fi
		done
		
		echo                                                                               >> $1
		let i++
	done
	
	echo ""    >> $1
	
	echo "//Last element in slaves_id_info is '{-1,-1,-1, -1}'to indicate end of array" >> $1
    echo "//slaves_id_info[i] slave_i Id info"                                          >> $1
    echo "//Size SLAVE_COUNT+1"                                                         >> $1
	echo "extern SlaveIdInfo_T slaves_id_info[$[$SLAVE_COUNT+1]];"                      >> $1
	
	echo ""   >> $1
	
	echo "//Last element in slaves_specified_info is '{-1,-1,-1, -1}'to indicate end of array" >> $1
	echo "//slaves_specified_info[i] slave_i info"                                             >> $1
	echo "//Size SLAVE_COUNT+1"                                                                >> $1
    echo "extern SlaveSpecifiedInfo_T slaves_specified_info[$[$SLAVE_COUNT+1]];"               >> $1
	
	
}

function generate_h_file()
{
	echo "#ifdef __cplusplus"                               >> $1       
	echo "extern \"C\" {"                                   >> $1 
	echo "#endif"                                           >> $1
	echo ""                                                 >> $1
	
	uppercase_prefix=$(echo $prefix | tr [a-z] [A-Z])
	echo "#ifndef $uppercase_prefix"                        >> $1
	echo "#define $uppercase_prefix"             		    >> $1
	echo "" 												>> $1
	
	echo "/*******************************************"     >> $1
	echo " *    Authour:WuLiang" 							>> $1
	echo " *    Email:jxustwl@163.com" 	                    >> $1
	echo " *********************************************/" 	>> $1
	echo "" 												>> $1
	
	echo "#include  \"ecrt.h\""                             >> $1
	
	echo "" 												>> $1
	echo "#define    VENDOR_ID    $VENDOR_ID"               >> $1
	echo "#define    PRODUCT_CODE $PRODUCT_CODE"            >> $1
	echo "#define    SLAVE_COUNT  $SLAVE_COUNT"             >> $1
	echo -e "#define    MASTER_COUNT 1\n"\                  >> $1
	
	cat  extern_define_file.txt                             >> $1
	
	echo ""                                                 >> $1
	
	get_slave_array_size
	generate_include_c_struct $1
	echo ""                                                 >> $1
	echo "#endif //$uppercase_prefix"                       >> $1
	echo ""                                                 >> $1
	
	echo "#ifdef __cplusplus"                               >> $1
	echo "}"                                                >> $1
	echo "#endif"                                           >> $1
	echo ""                                                 >> $1
	
}

function generate_c_file() 
{
	echo "#ifdef __cplusplus"                               >> $1       
	echo "extern \"C\" {"                                   >> $1 
	echo "#endif"                                           >> $1
	echo ""                                                 >> $1
	
	#generate c source file
	echo "/*" 												>> $1
	echo " *    Authour:WuLiang" 							>> $1
	echo " *    Email:jxustwl@163.com " 	                >> $1
	#echo " *    CreateTime:`date`" 							>> $1
	echo " */" 												>> $1
	echo "" 												>> $1
	echo "#include \"$include_file_name\"" 				    >> $1
	echo ""                                                 >> $1
                                                                
	#src file content                                           
	#generate by tools
	ethercat cstruct										>> $1
	
	#extra self definied info
	
	#Generate slave id info for all slaves
	get_slave_id_array
	echo "//Last element in slaves_id_info is '{-1,-1,-1, -1}'to indicate end of array" >> $1
    echo "//slaves_id_info[i] slave_i Id info"                                          >> $1
    echo "//Size SLAVE_COUNT+1"                                                         >> $1
	echo "SlaveIdInfo_T slaves_id_info[$[$SLAVE_COUNT+1]] = {"                          >> $1
	i=0
	while [ $i -le $[ $SLAVE_COUNT - 1 ] ]
	do
		echo -e "\t{${slave_alias_arr[$i]}, ${slave_position_arr[$i]}, ${slave_vendorid_arr[$i]}, ${slave_productcode_arr[$i]}}," >> $1    
		let i++
	done
	echo -e "\t{-1, -1, -1, -1}"                                                        >> $1
	echo "};"                                                                           >> $1
	echo                                                                                >> $1
	
	echo "//Last element in slaves_specified_info is slave_id_info is '{-1,-1,-1, -1}'" >> $1
    echo "//slaves_specified_info[i] slave_i info"                                      >> $1
	echo "//Size SLAVE_COUNT+1"                                                         >> $1
    echo "SlaveSpecifiedInfo_T slaves_specified_info[$[$SLAVE_COUNT+1]] = {"            >> $1
	
	get_slave_array_size
	i=0
	while [ $i -le $[ $SLAVE_COUNT - 1 ] ]
	do
		echo -e "\t{{${slave_alias_arr[$i]}, ${slave_position_arr[$i]}, ${slave_vendorid_arr[$i]}, ${slave_productcode_arr[$i]}}, slave_$[$i]_pdo_entries, ${slave_array_size[$[$i*3+0]]}, slave_$[$i]_pdos, ${slave_array_size[$[$i*3+1]]}, slave_$[$i]_syncs, ${slave_array_size[$[$i*3+2]]}}," >> $1		
		let i++
	done
	echo -e "\t{{-1, -1, -1, -1}}"                                                        >> $1
	echo "};"                                                                             >> $1
	echo                                                                                  >> $1
	
	echo "#ifdef __cplusplus"                               >> $1
	echo "}"                                                >> $1
	echo "#endif"                                           >> $1
	echo ""                                                 >> $1
};

#src cnd include file if not exist create them
#else backup them
if ! [ -e $include_file_name  ];then
	touch $include_file_name
else
	mv $include_file_name ${include_file_name}_bak
fi
if ! [ -e $src_file_name  ];then
	touch $src_file_name
else
	mv $src_file_name ${src_file_name}_bak
fi

generate_c_file $src_file_name
generate_h_file $include_file_name

