#ifdef __cplusplus
extern "C" {
#endif

/*
 *    Authour:WuLiang
 *    Email:jxustwl@163.com 
 */

#include "ec_common_configs_define.h"

/* Master 0, Slave 0, "SanyoDenki RS2 EtherCAT"
 * Vendor ID:       0x000001b9
 * Product code:    0x00000002
 * Revision number: 0x00000000
 */

ec_pdo_entry_info_t slave_0_pdo_entries[] = {
    {0x6040, 0x00, 16}, /* Control word */
    {0x607a, 0x00, 32}, /* Target position */
    {0x6081, 0x00, 32}, /* Profile velocity */
    {0x6083, 0x00, 32}, /* Profile acceleration */
    {0x6084, 0x00, 32}, /* Profile declaration */
    {0x60ff, 0x00, 32}, /* Target velocity */
    {0x6071, 0x00, 16}, /* Target torque */
    {0x60b8, 0x00, 16}, /* Touch probe function */
    {0x60fe, 0x01, 32}, /* Physical outputs */
    {0x6041, 0x00, 16}, /* Status word */
    {0x2100, 0x00, 16}, /* Status Word 1 */
    {0x6064, 0x00, 32}, /* Position actual value */
    {0x606c, 0x00, 32}, /* Velocity actual value */
    {0x6077, 0x00, 16}, /* Torque actual value */
    {0x60f4, 0x00, 32}, /* Following error actual value */
    {0x60b9, 0x00, 16}, /* Touch probe status */
    {0x60ba, 0x00, 32}, /* Touch probe position 1 positive value */
    {0x60bb, 0x00, 32}, /* Touch probe position 1 negative value */
    {0x60fd, 0x00, 32}, /* Digital inputs */
    {0x1001, 0x00, 8}, /* Error register */
    {0x6061, 0x00, 8}, /* Modes of operation display */
};

ec_pdo_info_t slave_0_pdos[] = {
    {0x1700, 9, slave_0_pdo_entries + 0}, /* RxPDO mapping */
    {0x1b00, 12, slave_0_pdo_entries + 9}, /* TxPDO mapping */
};

ec_sync_info_t slave_0_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, slave_0_pdos + 0, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 1, slave_0_pdos + 1, EC_WD_DISABLE},
    {0xff}
};

//Last element in slaves_id_info is '{-1,-1,-1, -1}'to indicate end of array
//slaves_id_info[i] slave_i Id info
//Size SLAVE_COUNT+1
SlaveIdInfo_T slaves_id_info[2] = {
	{0, 0, 0x000001b9, 0x00000002},
	{-1, -1, -1, -1}
};

//Last element in slaves_specified_info is slave_id_info is '{-1,-1,-1, -1}'
//slaves_specified_info[i] slave_i info
//Size SLAVE_COUNT+1
SlaveSpecifiedInfo_T slaves_specified_info[2] = {
	{{0, 0, 0x000001b9, 0x00000002}, slave_0_pdo_entries, 21, slave_0_pdos, 2, slave_0_syncs, 5},
	{{-1, -1, -1, -1}}
};

#ifdef __cplusplus
}
#endif

