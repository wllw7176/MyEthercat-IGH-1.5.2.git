#ifdef __cplusplus
extern "C" {
#endif


#include "ecat_common.h"

/******************************For Statistic Results***************************************/
void ec_common_get_statistic_results_hist(long arr [],long size, HistNode_T *h, int hist_size)
{
	int i, j;
	for (i = 0; i<size; i++) 
	{
		for (j = 0; j<hist_size-1; j++)
		{
			if (arr[i] / 1000 >= h[j].secntion.start && arr[i] / 1000 <= h[j].secntion.end)
			{
				h[j].count++;
				break;
			}
		}
		if (j == hist_size -1)
		{
			h[j].count++;
		}
	}
	for (j = 0; j<hist_size; j++)
	{
		h[j].percent = (h[j].count + 0.0) / size;
	}
	
}

void ec_common_reset_hist(HistNode_T *h, int hist_size)
{
	int i;
	for (i = 0; i < hist_size; i++)
	{
		h[i].count = 0;
		h[i].percent = 0.0;
	}
}

void ec_common_disp_hist(char *hist_name, HistNode_T *h, int hist_size)
{
	long total_count = 0;
	int i;
	
	for ( i = 0; i<hist_size; i++)
	{
		total_count += h[i].count;
	}
	if (NULL == hist_name)
	{
		LOG("StatisticHist(total_count:%ld):\n", total_count);
	}
	else
	{
		LOG("%s(total_count:%ld):\n", hist_name, total_count);
	}
	for (i = 0; i<hist_size-1; i++)
	{
		LOG("Seciton(%ld~%ld)us count:%d percent:%.5f\n", h[i].secntion.start, h[i].secntion.end, h[i].count, h[i].percent);
	}
	LOG("Other secitons count:%d percent:%.5f\n", h[i].count, h[i].percent);
}

void ec_common_get_statistic_results(long arr[], long size, long *max_val, long *min_val, long *ave_val)
{
	long tmp_max_val = arr[0], tmp_min_val = arr[0], total = 0;
	int i;
	for ( i = 0; i<size; i++)
	{
		if (arr[i] > tmp_max_val)
			tmp_max_val = arr[i];
		if (arr[i] < tmp_min_val)
			tmp_min_val = arr[i];
		total += arr[i];
	}
	if (max_val != NULL)
		*max_val = tmp_max_val;
	if (min_val != NULL)
		*min_val = tmp_min_val;
	if (ave_val != NULL)
		*ave_val = total / size;
}

void ec_common_disp_ec_pdo_entry_reg_array(ec_pdo_entry_reg_t *regs)
{
	int i=0;
	//LOG("List pdo_entry_reg_array:\n");
	while (regs[i].index != 0)
	{
		LOG("[%d] index:0x%04x subindex:%03d off_bytes:%d off_bits:%d\n", i, regs[i].index, regs[i].subindex, *regs[i].offset, *regs[i].bit_position);
		i++;
	}
}



/******************************For Generate reg data***************************************/
ec_pdo_entry_reg_t *ec_common_generate_pdo_entry_reg_arr(int alias, int position, int vendorid, 
																int productid, ec_pdo_entry_info_t pdo_entries[], int pdo_entries_size)
{
	int i = 0, j = 0;
	int size_resgs = 0;
	ec_pdo_entry_reg_t *domain_regs = NULL;
	
	for ( i = 0; i<pdo_entries_size; i++)
	{
		//无效对象字典不分配空间
		if (pdo_entries[i].index != 0) 
		{
			size_resgs++;
		}
	}
	if (0 == size_resgs)
	{
		return NULL;
	}
	//最后多出一个空间用于标示数组结束
	size_resgs++;

	//空间分配成功才进行设置
	domain_regs = (ec_pdo_entry_reg_t *) malloc(size_resgs*sizeof(ec_pdo_entry_reg_t));
	if (NULL != domain_regs)
	{
		for (i = 0; i<size_resgs-1; i++)
		{
			//过滤无效对象字典 通生成函数使用使用相同过滤条件
			while (pdo_entries[j].index == 0 && j < pdo_entries_size) 
			{
				j++;
			}
			domain_regs[i].alias = alias;
			domain_regs[i].position = position;
			domain_regs[i].vendor_id = vendorid;
			domain_regs[i].product_code = productid;
			domain_regs[i].index = pdo_entries[j].index;
			domain_regs[i].subindex = pdo_entries[j].subindex;
			domain_regs[i].offset = (unsigned int *) malloc(sizeof(unsigned int));
			j++;

			//分配内存失败释放已经分配的内存,返回NULL
			if (NULL == domain_regs[i].offset)
			{
				//释放之前分配的offset,bit_position内存
				i--; 
				while (i >= 0)
				{
					free(domain_regs[i].offset);
					free(domain_regs[i].bit_position);
				}
				free(domain_regs);
				return NULL;
			}
			domain_regs[i].bit_position= (unsigned int *) malloc(sizeof(unsigned int));
			if (NULL == domain_regs[i].bit_position)
			{
				//已经分配了offset先释放掉
				free(domain_regs[i].offset); 
				//释放之前分配的offset,bit_position内存
				i--; 
				while (i >= 0)
				{
					free(domain_regs[i].offset);
					free(domain_regs[i].bit_position);
				}
				free(domain_regs);
				return NULL;
			}
		}
		 //数组最后一个元素为标记数组结束
		memset((void *)&domain_regs[size_resgs-1], 0, sizeof(ec_pdo_entry_reg_t)); 
		//domain_regs[size_resgs-1].index = 0;
	}
	return domain_regs;
}

void ec_common_free_pdo_entry_reg_arr(ec_pdo_entry_reg_t *domain_regs)
{
	int size_regs;

	if (NULL == domain_regs)
	{
		return;
	}

	size_regs = 0;

	//domain_regs 数组最后一个元素domain_regs[i].index=0
	while (!LAST_DATA_IN_DOMAIN_REGS(domain_regs[size_regs])) 
	{
		size_regs++;
	}
	
	//释放除最后一个之外所有分配内存
	size_regs--;
	for (; size_regs>=0; size_regs--)
	{
		free(domain_regs[size_regs].offset);
		free(domain_regs[size_regs].bit_position);
	}
	free(domain_regs);
	
}

/******************************For init or configure slave specified info***************************************/

void ec_common_reset_slave_specified_info(SlaveSpecifiedInfo_T *slave_specified_info)
{
	memset(slave_specified_info, 0, sizeof(SlaveSpecifiedInfo_T));
}


void  ec_common_config_slave_specified_info_id(SlaveSpecifiedInfo_T *dst, SlaveIdInfo_T*src)
{
	memcpy(&dst->slave_id_info, src, sizeof(SlaveIdInfo_T));
}


void ec_common_config_slave_specified_info_pdo_entries(SlaveSpecifiedInfo_T *slave_specified_info, ec_pdo_entry_info_t *pdo_entries, unsigned int pdo_entries_size)
{
	slave_specified_info->pdo_entries = pdo_entries;
	slave_specified_info->pdo_entries_size = pdo_entries_size;
}


void ec_common_config_slave_specified_info_pdos(SlaveSpecifiedInfo_T *slave_specified_info, ec_pdo_info_t *pdos, unsigned int pdos_size)
{
	slave_specified_info->pdos = pdos;
	slave_specified_info->pdos_size = pdos_size;
}


void ec_common_config_slave_specified_info_syncs(SlaveSpecifiedInfo_T *slave_specified_info, ec_sync_info_t *syncs, unsigned int syns_size)
{
	slave_specified_info->syncs = syncs;
	slave_specified_info->syns_size = syns_size;
}


int  ec_common_config_config_slave_specified_info_domain_regs(SlaveSpecifiedInfo_T *slave_specified_info)
{
	slave_specified_info->domain_regs = ec_common_generate_pdo_entry_reg_arr(slave_specified_info->slave_id_info.alias, slave_specified_info->slave_id_info.position,
																			 slave_specified_info->slave_id_info.vendorid, slave_specified_info->slave_id_info.productcode,
																			 slave_specified_info->pdo_entries, slave_specified_info->pdo_entries_size);
	if (slave_specified_info->domain_regs)
		return 1;
	else
		return 0;
}


/******************************For init or configure master specified info***************************************/

void ec_common_reset_master_specified_info(MasterSpecifiedInfo_T *master_specified_info)
{
	 //alread init,did'n be reset
	if (0 == master_specified_info->init_flag)
		memset((void *)master_specified_info, 0, sizeof(MasterSpecifiedInfo_T));
}

void ec_common_config_master_specified_info(MasterSpecifiedInfo_T *master_specified_info, int master_index, int slaves_count, SlaveSpecifiedInfo_T *slaves_specified_infos)
{
	master_specified_info->index = master_index;
	master_specified_info->slaves_count = slaves_count;
	//master_specified_info->slaves_configs_count = slaves_count;
	//master_specified_info->slaves_specified_info_count = slaves_count+1; //remains one element to end the array
	master_specified_info->slaves_specified_infos = slaves_specified_infos;
}

// Each element in master_specified_info->slaves_specified_infos[]
// master_specified_info->slaves_specified_infos[i].domain_regs didn't be set, other filed have been setuped up.
EcCommonErr_T ec_common_init_master_specified_info(MasterSpecifiedInfo_T *master_specified_info)
{
	int i = 0;
	ec_slave_config_t *tmp_ec_slave_config = NULL;
	EcCommonErr_T err_code = EC_COMMON_OK;
	
	//step 1:config slaves domain_regs
	/*
	i = 0;
	while (!LAST_DATA_IN_SLAVES_SPECIFIED_INFOS(master_specified_info->slaves_specified_infos[i])) //not end of slaves_specified_infos[]
	{
		master_specified_info->slaves_specified_infos[i].domain_regs =  ec_common_generate_pdo_entry_reg_arr(master_specified_info->slaves_specified_infos[i].slave_id_info.alias, master_specified_info->slaves_specified_infos[i].slave_id_info.position, 
																		master_specified_info->slaves_specified_infos[i].slave_id_info.vendorid, master_specified_info->slaves_specified_infos[i].slave_id_info.productcode,
																		master_specified_info->slaves_specified_infos[i].pdo_entries, master_specified_info->slaves_specified_infos[i].pdo_entries_size);
		if (NULL == master_specified_info->slaves_specified_infos[i].domain_regs)
		{
			//Free  malloced space
			i--;
			while (i>=0)
			{
				ec_common_free_pdo_entry_reg_arr(master_specified_info->slaves_specified_infos[i].domain_regs);
			}
			return EC_COMMON_ERR_CFG_SLAVES;
		}
		i++;	
	}
	*/
	EC_COMMON_GENERATE_MASTER_SLAVES_PDO_ENTRY_REG_ARR(master_specified_info);
	
	//setp 2:malloc space for slaves_config
	master_specified_info->slaves_config = (ec_slave_config_t *) malloc(sizeof(ec_slave_config_t)*master_specified_info->slaves_count);
	if (NULL == master_specified_info->slaves_config)
	{
		err_code = EC_COMMON_ERR_REUEST_ALLOC_SLAVE_CONFIGS;
		goto out2;
	}
		

	//setp 3:request Master
	//ecrt_request_master
	master_specified_info->master = ecrt_request_master(master_specified_info->index);
	if (NULL == master_specified_info->master)
	{
		err_code = EC_COMMON_ERR_REUEST_MASTER;
		goto out1;
	}
		
	//step 4: create domain
	//ecrt_master_create_domain
	master_specified_info->domain = ecrt_master_create_domain(master_specified_info->master);
	if (NULL == master_specified_info->domain)
	{
		err_code = EC_COMMON_ERR_REUEST_DOMAIN;
		goto out1;
	}
		
	//step 5:Configure all slaves.
	//Ecah slave attached to master,do following operateion:
	//5.1 ecrt_master_slave_config
	//5.2 ecrt_slave_config_pdos
	//5.3 ecrt_domain_reg_pdo_entry_list
	/*
	i = 0; 
	for (i=0; i<master_specified_info->slaves_count; i++)
	{
		tmp_ec_slave_config = ecrt_master_slave_config(master_specified_info->master, master_specified_info->slaves_specified_infos[i].slave_id_info.alias, master_specified_info->slaves_specified_infos[i].slave_id_info.position,
														master_specified_info->slaves_specified_infos[i].slave_id_info.vendorid, master_specified_info->slaves_specified_infos[i].slave_id_info.productcode);
		if (NULL == tmp_ec_slave_config)
		{
			err_code = EC_COMMON_ERR_CFG_SLAVES;
			goto out1;
		}
		else
		{
			master_specified_info->slaves_config[i] = *tmp_ec_slave_config;
			err_code = ecrt_slave_config_pdos(&master_specified_info->slaves_config[i], master_specified_info->slaves_specified_infos[i].syns_size, master_specified_info->slaves_specified_infos[i].syncs);
			if (err_code)
			{
				err_code = EC_COMMON_ERR_CFG_SLAVES;
				goto out1;
			}
			err_code = ecrt_domain_reg_pdo_entry_list(master_specified_info->domain, master_specified_info->slaves_specified_infos[i].domain_regs);
			if (err_code)
			{
				err_code = EC_COMMON_ERR_CFG_SLAVES;
				goto out1;
			}
		}
	}
	*/

	EC_COMMON_CFG_MASTER_SLAVES(master_specified_info);

	//step 6,7 will in ec_common_activate_master
	
	return  EC_COMMON_OK;

out1:
	
	free(master_specified_info->slaves_config);

out2:
	i = 0;
	while (!LAST_DATA_IN_SLAVES_SPECIFIED_INFOS(master_specified_info->slaves_specified_infos[i]))	//not end of slaves_specified_infos[]
	{
		ec_common_free_pdo_entry_reg_arr(master_specified_info->slaves_specified_infos[i].domain_regs);
		i++;
	}

	return err_code;
	
}

EcCommonErr_T ec_common_activate_master(MasterSpecifiedInfo_T *master_specified_info)
{
	int i = 0;
	EcCommonErr_T err_code = EC_COMMON_OK;

	//step 6: Activate master
	//ecrt_master_activate
	err_code = ecrt_master_activate(master_specified_info->master);
	if (err_code)
	{
		err_code = EC_COMMON_ERR_ACTIVATE_MASTER;
		goto out1;
	}
	
	//step 7: Get domain memory address
	//ecrt_domain_data
	master_specified_info->domain_pd = ecrt_domain_data(master_specified_info->domain);
	if (NULL == master_specified_info->domain_pd)
	{
		err_code = EC_COMMON_ERR_REUEST_DOMAIN_PD;
		goto out1;
	}

	//step 8: Get domain size(Also:maped pdo data size)
	//ecrt_domain_size
    master_specified_info->domain_pd_bytes_count = ecrt_domain_size(master_specified_info->domain);
	master_specified_info->init_flag = 1;
	
	return EC_COMMON_OK;

out1:
	
	free(master_specified_info->slaves_config);

out2:
	i = 0;
	while (!LAST_DATA_IN_SLAVES_SPECIFIED_INFOS(master_specified_info->slaves_specified_infos[i]))	//not end of slaves_specified_infos[]
	{
		ec_common_free_pdo_entry_reg_arr(master_specified_info->slaves_specified_infos[i].domain_regs);
		i++;
	}

	return err_code;

}


void ec_common_master_free(MasterSpecifiedInfo_T *master_specified_info)
{
	int i = 0;
	while (!LAST_DATA_IN_SLAVES_SPECIFIED_INFOS(master_specified_info->slaves_specified_infos[i]))  //not end of slaves_specified_infos[]
	{
		ec_common_free_pdo_entry_reg_arr(master_specified_info->slaves_specified_infos[i].domain_regs);
		i++;
	}
	for (i=0; i<master_specified_info->slaves_count; i++)
		free(&master_specified_info->slaves_config[i]);
	//master_specified_info->init_flag = 0;
	memset(master_specified_info, 0, sizeof(MasterSpecifiedInfo_T));
}

void ec_common_traverse_master_slave_info(MasterSpecifiedInfo_T *master_specified_info)
{
	int i = 0;

	while (!LAST_DATA_IN_SLAVES_SPECIFIED_INFOS(master_specified_info->slaves_specified_infos[i]))  //not end of slaves_specified_infos[]
	{
		LOG("-------------------------------------------------------------------------------------------------------\n");
		LOG("Slave %d info:\n", i);
		LOG("Alias:0x%08x Position:%04d VendorId:0x%08x ProductCode:0x%08x\n", master_specified_info->slaves_specified_infos[i].slave_id_info.alias, master_specified_info->slaves_specified_infos[i].slave_id_info.position, 
			master_specified_info->slaves_specified_infos[i].slave_id_info.vendorid, master_specified_info->slaves_specified_infos[i].slave_id_info.productcode);
		ec_common_disp_ec_pdo_entry_reg_array(master_specified_info->slaves_specified_infos[i].domain_regs);
		i++;
	}
}

/******************************For process pdo data***************************************/
int ec_common_compare_slave_id_info(SlaveIdInfo_T *slave_info_dst, SlaveIdInfo_T *slave_info_src)
{
	return !memcmp(slave_info_dst, slave_info_src, sizeof(SlaveIdInfo_T));
}

int ec_common_locate_slave_pos(MasterSpecifiedInfo_T *master_specified_info, SlaveIdInfo_T *slave_id_info)
{
	int i;
	if (0 == master_specified_info->init_flag)
		return -1;
	for (i = 0 ; i<master_specified_info->slaves_count; i++)
	{
		if (ec_common_compare_slave_id_info(&master_specified_info->slaves_specified_infos[i].slave_id_info , slave_id_info))
		{
			return i;
		}
	}
	return -1;
}

int ec_common_locate_index_pos(ec_pdo_entry_reg_t *domain_regs, unsigned int index, unsigned int subindex)
{
	int i;

	while (!LAST_DATA_IN_DOMAIN_REGS(domain_regs[i]))
	{
		if (index == domain_regs[i].index && subindex == domain_regs[i].subindex)
		{
			return i;
		}
		i++;
	}
	return -1;
}

EcCommonErr_T ec_common_get_pdo_data(MasterSpecifiedInfo_T *master_specified_info, SlaveIdInfo_T *slave_id_info, unsigned int index, unsigned int sub_index, EcCommonDataType_T type, void *value)
{
	int i = 0, j = 0;
	int offset = 0;
	int bit_position = 0;
	EcCommonErr_T ret_code = EC_COMMON_OK;
	
	//finish init master continue doing other things
	if (1 == master_specified_info->init_flag)
	{
		i = ec_common_locate_slave_pos(master_specified_info, slave_id_info);
		if (i >= 0)
		{
			j = ec_common_locate_index_pos(master_specified_info->slaves_specified_infos[i].domain_regs, index,  sub_index);
			if (j >= 0)
			{
				offset = *(uint32_t *)master_specified_info->slaves_specified_infos[i].domain_regs[j].offset;
				bit_position = *(uint32_t *)master_specified_info->slaves_specified_infos[i].domain_regs[j].bit_position;
				switch (type) 
				{
					case EC_COMMON_DATA_TYPE_BIT:
						 *(uint8_t *)value = EC_READ_BIT(master_specified_info->domain_pd+offset, bit_position);
						break;
					case EC_COMMON_DATA_TYPE_U8:
						*(uint8_t *)value = EC_READ_U8(master_specified_info->domain_pd+offset);
						break;
					case EC_COMMON_DATA_TYPE_S8:
						 *(int8_t *)value = EC_READ_S8(master_specified_info->domain_pd+offset);
						break;
					case EC_COMMON_DATA_TYPE_U16:
						*(uint16_t *)value= EC_READ_U16(master_specified_info->domain_pd+offset);
						break;
					case EC_COMMON_DATA_TYPE_S16:
						*(int16_t *)value = EC_READ_S16(master_specified_info->domain_pd+offset);
						break;
					case EC_COMMON_DATA_TYPE_U32:
						*(uint32_t *)value = EC_READ_U32(master_specified_info->domain_pd+offset);
						break;
					case EC_COMMON_DATA_TYPE_S32:
						*(int32_t *)value = EC_READ_S32(master_specified_info->domain_pd+offset);
						break;
					case EC_COMMON_DATA_TYPE_U64:
						*(uint64_t *)value = EC_READ_U64(master_specified_info->domain_pd+offset);
						break;
					case EC_COMMON_DATA_TYPE_S64:
						*(int64_t *)value = EC_READ_S64(master_specified_info->domain_pd+offset);
						break;
					default:
						ret_code = EC_COMMON_ERR_INVALID_DATATYPE;
				};
			}
			else
			{
				ret_code = EC_COMMON_ERR_NO_SLAVE_INDEX;
			}
		}
		else
		{
			ret_code = EC_COMMON_ERR_NO_SLAVE;
		}
	}
	else
	{
		ret_code = EC_COMMON_ERR_NOT_INIT_MASTER;
	}
	return ret_code;
	
	
}

EcCommonErr_T ec_common_set_pdo_data(MasterSpecifiedInfo_T *master_specified_info, SlaveIdInfo_T *slave_id_info, unsigned int index, unsigned int sub_index, EcCommonDataType_T type, void *value)
{
	int i = 0, j = 0;
	int offset = 0;
	int bit_position = 0;
	EcCommonErr_T ret_code = EC_COMMON_OK;
	
	//finish init master continue doing other things
	if (1 == master_specified_info->init_flag)
	{
		i = ec_common_locate_slave_pos(master_specified_info, slave_id_info);
		if (i >= 0)
		{
			j = ec_common_locate_index_pos(master_specified_info->slaves_specified_infos[i].domain_regs, index,  sub_index);
			if (j >= 0)
			{
				offset = *(uint32_t *)master_specified_info->slaves_specified_infos[i].domain_regs[j].offset;
				bit_position = *(uint32_t *)master_specified_info->slaves_specified_infos[i].domain_regs[j].bit_position;
				switch (type) 
				{
					case EC_COMMON_DATA_TYPE_BIT:
						if ((*(uint8_t *) value) != 0 && (*(uint8_t *) value) != 1)
						{
							ret_code = EC_COMMON_ERR_INVALID_VALUE;
							break;
						}
						EC_WRITE_BIT(master_specified_info->domain_pd+offset, bit_position, *(uint8_t *)value);
						break;
					case EC_COMMON_DATA_TYPE_U8:
						EC_WRITE_U8(master_specified_info->domain_pd+offset, *(uint8_t *)value);
						break;
					case EC_COMMON_DATA_TYPE_S8:
						EC_WRITE_S8(master_specified_info->domain_pd+offset, *(int8_t *)value);
						break;
					case EC_COMMON_DATA_TYPE_U16:
						EC_WRITE_U16(master_specified_info->domain_pd+offset, *(uint16_t *)value);
						break;
					case EC_COMMON_DATA_TYPE_S16:
						EC_WRITE_S16(master_specified_info->domain_pd+offset, *(int16_t *)value);
						break;
					case EC_COMMON_DATA_TYPE_U32:
						EC_WRITE_U32(master_specified_info->domain_pd+offset, *(uint32_t *)value);
						break;
					case EC_COMMON_DATA_TYPE_S32:
						EC_WRITE_S32(master_specified_info->domain_pd+offset, *(int32_t *)value);
						break;
					case EC_COMMON_DATA_TYPE_U64:
						EC_WRITE_U64(master_specified_info->domain_pd+offset, *(uint64_t *)value);
						break;
					case EC_COMMON_DATA_TYPE_S64:
						EC_WRITE_S64(master_specified_info->domain_pd+offset, *(int64_t *)value);
						break;
					default:
						ret_code = EC_COMMON_ERR_INVALID_DATATYPE;
				};
			}
			else
			{
				ret_code = EC_COMMON_ERR_NO_SLAVE_INDEX;
			}
		}
		else
		{
			ret_code = EC_COMMON_ERR_NO_SLAVE;
		}
	}
	else
	{
		ret_code = EC_COMMON_ERR_NOT_INIT_MASTER;
	}
	return ret_code;

}


/******************************For sequence excute sdo commands***************************************/
int ec_common_sdo_download(MasterSpecifiedInfo_T *master_specified_info, SlaveIdInfo_T *slave_id_info, uint16_t index, uint8_t subindex, uint8_t *data, size_t data_size, uint32_t *abort_code)
{	
	return ecrt_master_sdo_download(master_specified_info->master, (uint16_t)slave_id_info->position, index, subindex,  data,  data_size, abort_code);
}

int ec_common_sdo_upload(MasterSpecifiedInfo_T *master_specified_info, SlaveIdInfo_T *slave_id_info, uint16_t index, uint8_t subindex, uint8_t *data, size_t data_size, size_t *result_size, uint32_t *abort_code)
{
	
	return ecrt_master_sdo_upload(master_specified_info->master, (uint16_t)slave_id_info->position, index, subindex,  data,  data_size, result_size, abort_code);
}

#ifdef __cplusplus
}
#endif





