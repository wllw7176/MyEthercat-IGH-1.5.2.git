#include "test.h"

extern int test_cycle_time;

int main(int argc, char *argv[])
{
	if (argc < 2 || argc > 4)
	{
		LOG("Usage %s devname [test_cycle_time_flag(1/0)] [dc_model_flag(1_0)].\n", argv[0]);
		return -1;
	}
	if (argc == 2)
	{
		test_cycle_time = 0;
	}
	if (argc == 3)
	{
		sscanf(argv[2], "%d", &test_cycle_time);
	}
	//test_io_board(argv[1]);
	//test_io_board2(argv[1]);
	test_io_board3(argv[1]);
	return 0;

}



