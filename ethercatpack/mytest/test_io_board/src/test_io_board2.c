#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include "manufacturer_specified_define.h"
#include "ecat_common.h"
#include "io_board.h"
#include "ec_common_configs_define.h"

ec_pdo_entry_reg_t *slave_domain_regs = NULL;

//使用ethercat库函数时用到的变量定义 非直接ioctl模式
static ec_master_t *main_master = NULL;
static ec_master_state_t main_master_state = {};
static ec_domain_t *main_domain = NULL;
static unsigned char *main_domain_pd = NULL;
static ec_domain_state_t main_domain_state = {};
static ssize_t main_domain_pd_bytes_count;
static ec_slave_config_t *slave_config = NULL;

MasterSpecifiedInfo_T master_specified_info;


/*PDO 实时性测试 */
extern int test_cycle_time;


static void cycle_task(void)
{
	unsigned long long count = 0;
	int i = 0;
	while (1)
	{
		usleep(1);

		RECEIVE_PDO_DATA(main_master, main_domain);

		// process pdo data and other things
		count++;
		if (count % 100 == 0)
		{
			for ( i = 0; i<master_specified_info.slaves_count; i++)
			{
				EC_WRITE_U8(main_domain_pd + *master_specified_info.slaves_specified_infos[i].domain_regs[0].offset,  0);
			}
			
		}
			
		if (count % 200 == 0)
		{
			for ( i = 0; i<master_specified_info.slaves_count; i++)
			{
				EC_WRITE_U8(main_domain_pd + *master_specified_info.slaves_specified_infos[i].domain_regs[0].offset,  1);
			}
		}
			
		
		SEND_PDO_DATA(main_master, main_domain);
	}
}

static void cycle_task2(void)
{
	unsigned long long count = 0;
	int ret_code = 0;
	
	SlaveIdInfo_T slave_0_id_info = {0, 0, VENDOR_ID, PRODUCT_CODE};
	SlaveIdInfo_T slave_1_id_info = {0, 1, VENDOR_ID, PRODUCT_CODE};
	unsigned char leds_val = 0;
	uint16_t slave_0_temp, slave_0_hum, slave_1_temp, slave_1_hum, invalid_data;
	while (1)
	{
		usleep(100);

		RECEIVE_PDO_DATA(master_specified_info.master,  master_specified_info.domain);

		
		// process pdo data and other things
		count++;
		if (count % 10000 == 0)
		{
			leds_val = 0;
			//ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, 0x7010, 0x01, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, 0x7010, 0x02, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, 0x7010, 0x03, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, 0x7010, 0x04, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_1_id_info, 0x7010, 0x01, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			ec_common_set_pdo_data(&master_specified_info, &slave_1_id_info, 0x7010, 0x02, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_1_id_info, 0x7010, 0x03, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_1_id_info, 0x7010, 0x04, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
		} 
			
		if (count % 20000 == 0)
		{
			leds_val = 1;
			//ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, 0x7010, 0x01, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, 0x7010, 0x02, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, 0x7010, 0x03, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, 0x7010, 0x04, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_1_id_info, 0x7010, 0x01, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			ec_common_set_pdo_data(&master_specified_info, &slave_1_id_info, 0x7010, 0x02, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_1_id_info, 0x7010, 0x03, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
			//ec_common_set_pdo_data(&master_specified_info, &slave_1_id_info, 0x7010, 0x04, EC_COMMON_DATA_TYPE_BIT, (void *)&leds_val);
		}

		if (count % 15000 == 0)
		{
			ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, 0x6020, 0x12, EC_COMMON_DATA_TYPE_U16, (void *)&slave_0_temp);
			ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, 0x6020, 0x13, EC_COMMON_DATA_TYPE_U16, (void *)&slave_0_hum);
			ec_common_get_pdo_data(&master_specified_info, &slave_1_id_info, 0x6020, 0x12, EC_COMMON_DATA_TYPE_U16, (void *)&slave_1_temp);
			ec_common_get_pdo_data(&master_specified_info, &slave_1_id_info, 0x6020, 0x13, EC_COMMON_DATA_TYPE_U16, (void *)&slave_1_hum);

			LOG("slave_0_temp:0x%04x slave_0_hum:0x%04x slave_1_temp:0x%04x slave_1_hum:0x%04x\n", slave_0_temp, slave_0_hum, slave_1_temp, slave_1_hum);

			ret_code = ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, 0x6020, 0x14, EC_COMMON_DATA_TYPE_U16, (void *)&invalid_data);
			if (!ret_code)
			{
				LOG("ec_common_get_pdo_data error, ret:%d", ret_code);
			}
		
		}
		
		
		SEND_PDO_DATA(master_specified_info.master,  master_specified_info.domain);
	}
}

//1~100us, 101~200us, 201~300us, 301~400us, 401~500us, 501~1000us, 1001~10000us, >10000us
static HistNode_T result_hist[] = { 
	{{1,   100}, 0, 0.0}, /*1~100us*/
	{{101, 200}, 0, 0.0},
	{{201, 300}, 0, 0.0},
	{{301, 400}, 0, 0.0},
	{{401, 500}, 0, 0.0},
	{{501, 1000}, 0, 0.0},
	{{1001, 10000}, 0, 0.0},
	{{10001, 100000}, 0, 0.0},
	{{100001, 1000000}, 0, 0.0},
	{{0, 0}, 0, 0.0},
};
	

static void cycle_task3(void)
{
	unsigned long long count = 0;
	int ret_code = 0;
	
	SlaveIdInfo_T slave_0_id_info = {0, 0, VENDOR_ID, PRODUCT_CODE};
	int val = 0x001c;
	int32_t  slave_0_torque, slave_0_vel;
	volatile int32_t slave_0_pos, pre_slave_0_pos = slave_0_pos;
	uint8_t err_register_val;
	uint16_t status_word, status_word1;
	char cmdStr[64];
	

	if (0 == test_cycle_time)	
	{
		while (1)
		{
			//usleep(1);
			//sleep(1);
			//经测试最多休眠10000us 否则pdo不起作用
			usleep(1000); 
			
			RECEIVE_PDO_DATA(master_specified_info.master,  master_specified_info.domain);
			
			// process pdo data and other things
			count++;
			
			ret_code = ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_POSITION_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&slave_0_pos);
			if (ret_code != EC_COMMON_OK)
			{
				LOG("ec_common_get_pdo_data error, %s:%d\n", __FILE__, __LINE__);
			}
			
			sprintf(cmdStr, "echo %d >> pdo_test.txt", slave_0_pos);
			system(cmdStr);
			
			
			/*
			ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_TORQUE_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&slave_0_torque);
			if (ret_code != EC_COMMON_OK)
			{
				LOG("ec_common_get_pdo_data error, %s:%d\n", __FILE__, __LINE__);
			}
			ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_VELOCITY_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&slave_0_vel);
			if (ret_code != EC_COMMON_OK)
			{
				LOG("ec_common_get_pdo_data error, %s:%d\n", __FILE__, __LINE__);
			}
			ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_ERROR_REGISTER_U8, EC_COMMON_DATA_TYPE_U8, (void *)&err_register_val);
			if (ret_code != EC_COMMON_OK)
			{
				LOG("ec_common_get_pdo_data error, %s:%d\n", __FILE__, __LINE__);
			}
			ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_STATUS_WORD1_U16, EC_COMMON_DATA_TYPE_U16, (void *)&status_word);
			if (ret_code != EC_COMMON_OK)
			{
				LOG("ec_common_get_pdo_data error, %s:%d\n", __FILE__, __LINE__);
			}
			ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_STATUS_WORD1_U16, EC_COMMON_DATA_TYPE_U16, (void *)&status_word1);
			if (ret_code != EC_COMMON_OK)
			{
				LOG("ec_common_get_pdo_data error, %s:%d\n", __FILE__, __LINE__);
			}

			if (count % 100 == 0)
			{
				LOG("Slave(%d) pos:0x%08x torque:0x%08x vel:0x%08x err_register:0x%02x status_word:0x%04x status_word1:0x%04x\n", slave_0_id_info.position, slave_0_pos, slave_0_torque, slave_0_vel, err_register_val,
					status_word, status_word1);
			}
			*/
			
			SEND_PDO_DATA(master_specified_info.master,  master_specified_info.domain);
			
			
		}    
	}
	else //test cycle time
	{
		//for stastic run time
		struct timespec start;
		struct timespec stop;
		long first_stage_cycle_time_arr[INNER_COUNT_TIMES];
		int first_stage_cycle_count = 0;
		long diff_time_ns, max_time_one_cycle_ns, min_time_one_cycle_ns, ave_time_one_cycle_ns;
		int record_start_time_flag = 1;

	
		while (1)
		{
			usleep(1);
			count++;
			RECEIVE_PDO_DATA(master_specified_info.master,  master_specified_info.domain);

			
			ret_code = ec_common_get_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_POSITION_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&slave_0_pos);
			if (1 == record_start_time_flag)
			{
				clock_gettime(CLOCK_REALTIME, &start);
				record_start_time_flag = 0;
			}
			
			
			if (pre_slave_0_pos != slave_0_pos && count != 1)
			{
				pre_slave_0_pos = slave_0_pos;
				clock_gettime(CLOCK_REALTIME, &stop);
				diff_time_ns = DIFF_NS(start, stop);
				first_stage_cycle_time_arr[first_stage_cycle_count] = diff_time_ns;
				
				if (first_stage_cycle_time_arr[first_stage_cycle_count]/1000 < 100 || first_stage_cycle_time_arr[first_stage_cycle_count]/1000 > 10000) // diff_time_ns<100us or diff_time_ns>10ms
				{
					//LOG("Exception diff_time_val first_stage_cycle_time_arr[%d]=%ldus\n", first_stage_cycle_count, first_stage_cycle_time_arr[first_stage_cycle_count]/1000);
				}
				
				if (INNER_COUNT_TIMES-1 == first_stage_cycle_count)
				{
					first_stage_cycle_count = 0;

					ec_common_get_statistic_results(first_stage_cycle_time_arr, INNER_COUNT_TIMES, &max_time_one_cycle_ns, &min_time_one_cycle_ns, &ave_time_one_cycle_ns);
					LOG("First stage statistic results(uint:ns) CountCycleTimes:%d MaxCycleTime:%ld MinCycleTime:%ld AveCycleTime:%ld\n", \
											INNER_COUNT_TIMES, max_time_one_cycle_ns, min_time_one_cycle_ns, ave_time_one_cycle_ns);
					
					ec_common_get_statistic_results_hist(first_stage_cycle_time_arr, INNER_COUNT_TIMES, result_hist, ARR_SIZE(result_hist));
					ec_common_disp_hist("FirstStageStageHist", result_hist, ARR_SIZE(result_hist));
					ec_common_reset_hist(result_hist, ARR_SIZE(result_hist));

					memset(first_stage_cycle_time_arr, 0, sizeof(long)*INNER_COUNT_TIMES);
				}
				else
				{
					first_stage_cycle_count++;
				}
				
				record_start_time_flag = 1;
				
			}
			
			SEND_PDO_DATA(master_specified_info.master,  master_specified_info.domain);
			
		}
	}
}



static void signal_handler(int signum) 
{
	
	switch (signum) 
	{
		case SIGSEGV:
			LOG("Caught SIGNAL SIGSEGV\n");
			break;
		case SIGABRT:
			LOG("Caught SIGNAL SIGABRT\n");
			break;
		case SIGTERM:
			LOG("Caught SIGNAL SIGTERM\n");
			break;
		case SIGQUIT:
			LOG("Caught SIGNAL SIGQUIT\n");
			/*
			LOG("Caught SIGQUIT");
			if (NULL != slave_domain_regs)
				ec_common_free_pdo_entry_reg_arr(slave_domain_regs);
			if (NULL != main_master)
				ecrt_release_master(main_master);
			*/
			if (1 == master_specified_info.init_flag)
				ec_common_master_free(&master_specified_info);
			break;
		default:
			return;
			
    }
}


int test_io_board2(char *master_dev)
{
	
	struct sigaction sa;
	
	//请求主机
    main_master = ecrt_request_master(0);
    if (NULL != main_master)
    {
		printf("Request master(0) success!\n");
	}
    else
   	{
		printf("Request master(0) failed!\n");
		return -1;
	}
    //请求域, 解释见IgH ethercatmaster手册
    main_domain = ecrt_master_create_domain(main_master);
    if (NULL != main_domain)
    {
		printf("Master create domain success!\n");
	}
	else
	{
		printf("Master create domain failed!\n");
		return -1;
	}

    //获取从机0配置
    if (!(slave_config = ecrt_master_slave_config(main_master, IO_BOARD_0_ALIAS, 
															IO_BOARD_0_POSITION, IO_BOARD_0_VENDORID, IO_BOARD_0_PRODUCTID))) 
    {
        LOG( "Get slave(%d) configuration failed!\n", IO_BOARD_0_POSITION);
        return -1;
    }
	else
	{
		LOG( "Get slave(%d) configuration success!\n", IO_BOARD_0_POSITION);
	}
	
	//从机PDO配置
    if (ecrt_slave_config_pdos(slave_config, EC_END, slave_0_syncs)) 
	{
        LOG("Configure slave(%d) PDOs failed!\n", IO_BOARD_0_POSITION);
        return -1;
    }
	else
	{
		LOG("Configure slave(%d)PDOs success!\n", IO_BOARD_0_POSITION);
	}
    
	//域注册
	slave_domain_regs = ec_common_generate_pdo_entry_reg_arr(IO_BOARD_0_ALIAS, IO_BOARD_0_POSITION, IO_BOARD_0_VENDORID, IO_BOARD_0_PRODUCTID,
															slave_0_pdo_entries, 28);
	
	if (NULL == slave_domain_regs)
	{
		LOG("ec_common_generate_pdo_entry_reg_arr error\n");
		return -1;
	}
	
	if (ecrt_domain_reg_pdo_entry_list(main_domain, slave_domain_regs))
	{
        LOG( "Reg slave(%d) PDO entry registration failed!ErrMsg:%s\n", strerror(errno), IO_BOARD_0_POSITION);
        return -1;
    }
	else
	{
		LOG("Reg slave PDO entry registration success\n");
	}
 
	//注册信号处理函数
	sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
	
	if (sigaction(SIGSEGV, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGABRT, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGTERM, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGQUIT, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	
	//激活主机
    if (ecrt_master_activate(main_master))
    {
		LOG( "Activate master failed!\n");
		return -1;
	}
	else
	{
		LOG( "Activate master success!\n");
	}
	
	
	main_domain_pd_bytes_count = ecrt_domain_size(main_domain);
	if (main_domain_pd_bytes_count < 0)
	{
		LOG("Get main_domain_pd_bytes_count error!\n");
		return -1;
	}

	
    if (!(main_domain_pd = ecrt_domain_data(main_domain))) 
	{
		LOG( "ecrt_domain_data(domain1) failed!\n");
		return -1;
    }
	else
	{
		LOG( "ecrt_domain_data(domain1) success main_domain_pd:%p main_domain_pd_bytes_count:%d\n", main_domain_pd, main_domain_pd_bytes_count);
	}

	ec_common_disp_ec_pdo_entry_reg_array(slave_domain_regs);
	
	//开始周期任务
	cycle_task();
    return 0;
}


int test_io_board3(char *master_dev)
{
	struct sigaction sa;
	int ret;
	int val;
	SlaveIdInfo_T slave_0_id_info = {0, 0, VENDOR_ID, PRODUCT_CODE};
	unsigned char operation_model;
	unsigned int abort_code;
	size_t result_size;
	unsigned short control_word;
	unsigned short sm2_sync_model, sm3_sync_model;

	//step 1: Init struct of master_specified_info
	ec_common_reset_master_specified_info(&master_specified_info);
	ec_common_config_master_specified_info(&master_specified_info, 0, SLAVE_COUNT, slaves_specified_info);
	ret = ec_common_init_master_specified_info(&master_specified_info);
	if (ret != EC_COMMON_OK)
	{
		LOG("ec_common_init_master_specified_info failed!");
		return -1;
	}
	
	//setp2. Before actiave master, we must do  configure slave by sdo
	//注意此处使用 SDO方式进行配置是因为有些对象字典从站未进行pdo映射
	
	//system("ethercat download -a 0 -p 0 -t uint8   0x6040 0x00 0x06");
	//setp 2.1 :进行其他设置前0x6040, 0x00=0x06 让电机处于shutdown状态 山羊电机手册 P229
	control_word= 0x06;
	//ret = ec_common_sdo_download(&master_specified_info,  &slave_0_id_info, 0x6040, 0x00, (uint8_t *)&control_word, sizeof(control_word), &abort_code);
	
	ret = ec_common_sdo_download(&master_specified_info,  &slave_0_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, (uint8_t *)&control_word, sizeof(control_word), &abort_code);
	if (ret)
	{
		LOG("ec_common_sdo_download failed,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	//目前没有启动dc通同步模式,因而要配置sm2,sm3为自由模式
	//Step 2.2 set sm2 to free run model
	ret = ec_common_sdo_upload(&master_specified_info,  &slave_0_id_info, INDEX_SANYO_RS2_SM2_SYNC_TYPE_U16, (uint8_t *)&sm2_sync_model, sizeof(sm2_sync_model), &result_size, &abort_code);
	if (ret)
	{
		LOG("ec_common_sdo_upload failed, %s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	if (sm2_sync_model != 0x00) //If sm2 is free run model, we need't configure it
	{
		sm2_sync_model = 0x00;
		ret = ec_common_sdo_download(&master_specified_info,  &slave_0_id_info, INDEX_SANYO_RS2_SM2_SYNC_TYPE_U16, (uint8_t *)&sm2_sync_model, sizeof(sm2_sync_model), &abort_code);
		if (ret)
		{
			LOG("ec_common_sdo_download failed,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
	}
	//set sm3 to free run model
	ret = ec_common_sdo_upload(&master_specified_info,  &slave_0_id_info, INDEX_SANYO_RS2_SM3_SYNC_TYPE_U16, (uint8_t *)&sm2_sync_model, sizeof(sm2_sync_model), &result_size, &abort_code);
	if (ret)
	{
		LOG("ec_common_sdo_upload failed, %s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	if (sm2_sync_model != 0x00) //If sm3 is free run model, we need't configure it
	{
		sm2_sync_model = 0x00;
		ret = ec_common_sdo_download(&master_specified_info,  &slave_0_id_info, INDEX_SANYO_RS2_SM3_SYNC_TYPE_U16, (uint8_t *)&sm2_sync_model, sizeof(sm2_sync_model), &abort_code);
		if (ret)
		{
			LOG("ec_common_sdo_download failed,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
	}
	
	
	//Step 3:set torque model
	//system("ethercat download -a 0 -p 0 -t uint8   0x6060 0x00 0x04");
	operation_model = 0x04;
	ret = ec_common_sdo_download(&master_specified_info,  &slave_0_id_info, INDEX_SANYO_RS2_OPERATION_MODEL_S8, &operation_model, sizeof(operation_model), &abort_code);
	if (ret)
	{
		LOG("ec_common_sdo_download failed,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	
	ret = ec_common_activate_master(&master_specified_info);
	if (ret != EC_COMMON_OK)
	{
		LOG("ec_common_activate_master failed! %s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	main_master = master_specified_info.master;
	main_domain = master_specified_info.domain;
	main_domain_pd = master_specified_info.domain_pd;
	main_domain_pd_bytes_count = master_specified_info.domain_pd_bytes_count;

	//注册信号处理函数
	sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
	
	if (sigaction(SIGSEGV, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGABRT, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGTERM, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGQUIT, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }

	//Step 4:PDO方式使能电机
	val = 0x0f;
	ret = ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, EC_COMMON_DATA_TYPE_S16, (void *)&val);
	if (ret != EC_COMMON_OK)
	{
		LOG("ec_common_set_pdo_data error, %s:%d\n", __FILE__, __LINE__);
	}
	usleep(1); 

	//设置一个值让它开始就转动此处不是必须设置的
	val = 0x001c;
	ret = ec_common_set_pdo_data(&master_specified_info, &slave_0_id_info, INDEX_SANYO_RS2_TARGET_TORQUE_S16, EC_COMMON_DATA_TYPE_S16, (void *)&val);
	if (ret != EC_COMMON_OK)
	{
		LOG("ec_common_set_pdo_data error, %s:%d\n", __FILE__, __LINE__);
	}
	
	ec_common_traverse_master_slave_info(&master_specified_info);
	
	
	//开始周期任务
	//cycle_task();
	//cycle_task2();
	cycle_task3();

	return 0;
	
}



