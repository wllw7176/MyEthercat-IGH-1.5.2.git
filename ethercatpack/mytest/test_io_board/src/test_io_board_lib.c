#include "test.h"
#include "ecat_common.h"



#define NSEC_PER_SEC (1000000000L)
#define PERIOD_NS (NSEC_PER_SEC / FREQUENCY)

/* Master 0, Slave 0, "TempHumi"
 * Vendor ID:       0x00000017
 * Product code:    0x26483056
 * Revision number: 0x00020111
*/
//保存控制字与状态字在domain中的偏移位置 读写PDO入口要用到
unsigned int off_bytes_board0_led1_val;
unsigned int off_bits_board0_led1_val;
unsigned int off_bytes_board0_led2_val;
unsigned int off_bits_board0_led2_val;
unsigned int off_bytes_board0_led3_val;
unsigned int off_bits_board0_led3_val;
unsigned int off_bytes_board0_led4_val;
unsigned int off_bits_board0_led4_val;
unsigned int off_bytes_board0_led5_val;
unsigned int off_bits_board0_led5_val;
unsigned int off_bytes_board0_led6_val;
unsigned int off_bits_board0_led6_val;
unsigned int off_bytes_board0_led7_val;
unsigned int off_bits_board0_led7_val;
unsigned int off_bytes_board0_led8_val;
unsigned int off_bits_board0_led8_val;
unsigned int off_bytes_board0_ledgap;
unsigned int off_bits_board0_ledgap;
unsigned int off_bytes_board0_switch1_status;
unsigned int off_bits_board0_switch1_status;
unsigned int off_bytes_board0_switch2_status;
unsigned int off_bits_board0_switch2_status;
unsigned int off_bytes_board0_switch3_status;
unsigned int off_bits_board0_switch3_status;
unsigned int off_bytes_board0_switch4_status;
unsigned int off_bits_board0_switch4_status;
unsigned int off_bytes_board0_switch5_status;
unsigned int off_bits_board0_switch5_status;
unsigned int off_bytes_board0_switch6_status;
unsigned int off_bits_board0_switch6_status;
unsigned int off_bytes_board0_switch7_status;
unsigned int off_bits_board0_switch7_status;
unsigned int off_bytes_board0_switch8_status;
unsigned int off_bits_board0_switch8_status;
unsigned int off_bytes_board0_switchgap;
unsigned int off_bits_board0_switchgap;
unsigned int off_bytes_board0_underrange_status;
unsigned int off_bits_board0_underrange_status;
unsigned int off_bytes_board0_overrange_status;
unsigned int off_bits_board0_overrange_status;
unsigned int off_bytes_board0_limit1_status;
unsigned int off_bits_board0_limit1_status;
unsigned int off_bytes_board0_limit2_status;
unsigned int off_bits_board0_limit2_status;
unsigned int off_bytes_board0_limitgap;
unsigned int off_bits_board0_limitgap;
unsigned int off_bytes_board0_TxPDOState_status;
unsigned int off_bits_board0_TxPDOState_status;
unsigned int off_bytes_board0_TxPDO_Toggle_status;
unsigned int off_bits_board0_TxPDO_Toggle_status;
unsigned int off_bytes_board0_Analog_input_status;
unsigned int off_bits_board0_Analog_input_status;
unsigned int off_bytes_board0_Temp_val;
unsigned int off_bits_board0_Temp_val;
unsigned int off_bytes_board0_Hum_val;
unsigned int off_bits_board0_Hum_val;

/*
	这里定义我们要操作PDO对象的信息,我们可以定义多个PDO映射,但是可以只使用其中的几个
*/
static ec_pdo_entry_reg_t slave0_domain_regs[] = {

	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x01, //子索引 
		&off_bytes_board0_led1_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led1_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x02, //子索引 
		&off_bytes_board0_led2_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led2_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x03, //子索引 
		&off_bytes_board0_led3_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led3_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x04, //子索引 
		&off_bytes_board0_led4_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led4_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x05, //子索引 
		&off_bytes_board0_led5_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led5_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x06, //子索引 
		&off_bytes_board0_led6_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led6_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x07, //子索引 
		&off_bytes_board0_led7_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led7_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x08, //子索引 
		&off_bytes_board0_led8_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_led8_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x01, //子索引 
		&off_bytes_board0_switch1_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch1_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x02, //子索引 
		&off_bytes_board0_switch2_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch2_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x03, //子索引 
		&off_bytes_board0_switch3_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch3_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x04, //子索引 
		&off_bytes_board0_switch4_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch4_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x05, //子索引 
		&off_bytes_board0_switch5_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch5_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x06, //子索引 
		&off_bytes_board0_switch6_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch6_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x07, //子索引 
		&off_bytes_board0_switch7_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch7_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x08, //子索引 
		&off_bytes_board0_switch8_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_switch8_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x01, //子索引 
		&off_bytes_board0_underrange_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_underrange_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x02, //子索引 
		&off_bytes_board0_overrange_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_overrange_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x03, //子索引 
		&off_bytes_board0_limit1_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_limit1_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x05, //子索引 
		&off_bytes_board0_limit2_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_limit2_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x1802,                          //索引
		0x07, //子索引 
		&off_bytes_board0_TxPDOState_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_TxPDOState_status
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x1802,                          //索引
		0x09, //子索引 
		&off_bytes_board0_TxPDO_Toggle_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_TxPDO_Toggle_status
	},
	
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x11, //子索引 
		&off_bytes_board0_Analog_input_status,       //PDO入口在process_data字节偏移量
		&off_bits_board0_Analog_input_status
	},
	
	
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x12, //子索引 
		&off_bytes_board0_Temp_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_Temp_val
	},
	{
		IO_BOARD_0_ALIAS,               //别名
		IO_BOARD_0_POSITION,            //位置
		IO_BOARD_0_VENDORID,            //厂商id
		IO_BOARD_0_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x13, //子索引 
		&off_bytes_board0_Hum_val,       //PDO入口在process_data字节偏移量
		&off_bits_board0_Hum_val
	},
	{
	}
}; 

static ec_pdo_entry_info_t board0_pdo_entries[] = {
    {0x7010, 0x01, 1}, /* LED 1 */
    {0x7010, 0x02, 1}, /* LED 2 */
    {0x7010, 0x03, 1}, /* LED 3 */
    {0x7010, 0x04, 1}, /* LED 4 */
    {0x7010, 0x05, 1}, /* LED 5 */
    {0x7010, 0x06, 1}, /* LED 6 */
    {0x7010, 0x07, 1}, /* LED 7 */
    {0x7010, 0x08, 1}, /* LED 8 */
    {0x0000, 0x00, 8}, /* Gap */
    {0x6000, 0x01, 1}, /* Switch 1 */
    {0x6000, 0x02, 1}, /* Switch 2 */
    {0x6000, 0x03, 1}, /* Switch 3 */
    {0x6000, 0x04, 1}, /* Switch 4 */
    {0x6000, 0x05, 1}, /* Switch 5 */
    {0x6000, 0x06, 1}, /* Switch 6 */
    {0x6000, 0x07, 1}, /* Switch 7 */
    {0x6000, 0x08, 1}, /* Switch 8 */
    {0x0000, 0x00, 8}, /* Gap */
    {0x6020, 0x01, 1}, /* Underrange */
    {0x6020, 0x02, 1}, /* Overrange */
    {0x6020, 0x03, 2}, /* Limit 1 */
    {0x6020, 0x05, 2}, /* Limit 2 */
    {0x0000, 0x00, 8}, /* Gap */
    {0x1802, 0x07, 1}, /* TxPDOState */
    {0x1802, 0x09, 1}, /* TxPDOToggle */
    {0x6020, 0x11, 16}, /* AnalogInput */
    {0x6020, 0x12, 16}, /* Temp */
    {0x6020, 0x13, 16}, /* Hum */
};

static ec_pdo_info_t board0_pdos[] = {
    {0x1601, 9, board0_pdo_entries + 0}, /* DO RxPDO-Map */
    {0x1a00, 9, board0_pdo_entries + 9}, /* DI TxPDO-Map */
    {0x1a02, 10, board0_pdo_entries + 18}, /* AI TxPDO-Map */
};

const static ec_sync_info_t board0_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    /*
		{2, EC_DIR_OUTPUT, 1, board0_pdos + 0, EC_WD_ENABLE},
    	第3个参数1代表对象在SM2 SM2(Output) Synchronisation  Parameter 的位置
    	SM2 RxPDO 对象索引为:0x1600～0x1603,0x1700～0x1703 参照山羊电机手册P213
    	board0_pdos + 0, 第一个参数PDO_index:0x1601, 因而第3个参数为1
    	1601 在 0x1600～0x1603,0x1700～0x1703 对象序列中位置为1
	*/
    {2, EC_DIR_OUTPUT, 1, board0_pdos + 0, EC_WD_ENABLE},
    {3, EC_DIR_INPUT, 2, board0_pdos + 1, EC_WD_DISABLE},
    {0xff}
};


//使用ethercat库函数时用到的变量定义 非直接ioctl模式
static ec_master_t *main_master = NULL;
static ec_master_state_t main_master_state = {};
static ec_domain_t *main_domain = NULL;
static unsigned char *main_domain_pd = NULL;
static ec_domain_state_t main_domain_state = {};
static ssize_t main_domain_pd_bytes_count;
static ec_slave_config_t *board0_slave_config = NULL;



//slave1 define
//保存控制字与状态字在domain中的偏移位置 读写PDO入口要用到
unsigned int off_bytes_board1_led1_val;
unsigned int off_bits_board1_led1_val;
unsigned int off_bytes_board1_led2_val;
unsigned int off_bits_board1_led2_val;
unsigned int off_bytes_board1_led3_val;
unsigned int off_bits_board1_led3_val;
unsigned int off_bytes_board1_led4_val;
unsigned int off_bits_board1_led4_val;
unsigned int off_bytes_board1_led5_val;
unsigned int off_bits_board1_led5_val;
unsigned int off_bytes_board1_led6_val;
unsigned int off_bits_board1_led6_val;
unsigned int off_bytes_board1_led7_val;
unsigned int off_bits_board1_led7_val;
unsigned int off_bytes_board1_led8_val;
unsigned int off_bits_board1_led8_val;
unsigned int off_bytes_board1_ledgap;
unsigned int off_bits_board1_ledgap;
unsigned int off_bytes_board1_switch1_status;
unsigned int off_bits_board1_switch1_status;
unsigned int off_bytes_board1_switch2_status;
unsigned int off_bits_board1_switch2_status;
unsigned int off_bytes_board1_switch3_status;
unsigned int off_bits_board1_switch3_status;
unsigned int off_bytes_board1_switch4_status;
unsigned int off_bits_board1_switch4_status;
unsigned int off_bytes_board1_switch5_status;
unsigned int off_bits_board1_switch5_status;
unsigned int off_bytes_board1_switch6_status;
unsigned int off_bits_board1_switch6_status;
unsigned int off_bytes_board1_switch7_status;
unsigned int off_bits_board1_switch7_status;
unsigned int off_bytes_board1_switch8_status;
unsigned int off_bits_board1_switch8_status;
unsigned int off_bytes_board1_switchgap;
unsigned int off_bits_board1_switchgap;
unsigned int off_bytes_board1_underrange_status;
unsigned int off_bits_board1_underrange_status;
unsigned int off_bytes_board1_overrange_status;
unsigned int off_bits_board1_overrange_status;
unsigned int off_bytes_board1_limit1_status;
unsigned int off_bits_board1_limit1_status;
unsigned int off_bytes_board1_limit2_status;
unsigned int off_bits_board1_limit2_status;
unsigned int off_bytes_board1_limitgap;
unsigned int off_bits_board1_limitgap;
unsigned int off_bytes_board1_TxPDOState_status;
unsigned int off_bits_board1_TxPDOState_status;
unsigned int off_bytes_board1_TxPDO_Toggle_status;
unsigned int off_bits_board1_TxPDO_Toggle_status;
unsigned int off_bytes_board1_Analog_input_status;
unsigned int off_bits_board1_Analog_input_status;
unsigned int off_bytes_board1_Temp_val;
unsigned int off_bits_board1_Temp_val;
unsigned int off_bytes_board1_Hum_val;
unsigned int off_bits_board1_Hum_val;


const static ec_pdo_entry_reg_t board1_domain_regs[] = {
	/*Leds*/
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x01, //子索引 
		&off_bytes_board1_led1_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_led1_val
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x02, //子索引 
		&off_bytes_board1_led2_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_led2_val
	},
	
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x03, //子索引 
		&off_bytes_board1_led3_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_led3_val
	},
	
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x04, //子索引 
		&off_bytes_board1_led4_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_led4_val
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x05, //子索引 
		&off_bytes_board1_led5_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_led5_val
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x06, //子索引 
		&off_bytes_board1_led6_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_led6_val
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x07, //子索引 
		&off_bytes_board1_led7_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_led7_val
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x7010,                          //索引
		0x08, //子索引 
		&off_bytes_board1_led8_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_led8_val
	},
	/*
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x0000,                          //索引
		0x00, //子索引 
		&off_bytes_board1_ledgap,       //PDO入口在process_data字节偏移量
		&off_bits_board1_ledgap
	},
	*/
	/*switchs*/
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x01, //子索引 
		&off_bytes_board1_switch1_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switch1_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x02, //子索引 
		&off_bytes_board1_switch2_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switch2_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x03, //子索引 
		&off_bytes_board1_switch3_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switch3_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x04, //子索引 
		&off_bytes_board1_switch4_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switch4_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x05, //子索引 
		&off_bytes_board1_switch5_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switch5_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x06, //子索引 
		&off_bytes_board1_switch6_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switch6_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x07, //子索引 
		&off_bytes_board1_switch7_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switch7_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6000,                          //索引
		0x08, //子索引 
		&off_bytes_board1_switch8_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switch8_status
	},

	/*
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x0000,                          //索引
		0x00, //子索引 
		&off_bytes_board1_switchgap,       //PDO入口在process_data字节偏移量
		&off_bits_board1_switchgap
	},
	*/
	
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x01, //子索引 
		&off_bytes_board1_underrange_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_underrange_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x02, //子索引 
		&off_bytes_board1_overrange_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_overrange_status
	},
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x03, //子索引 
		&off_bytes_board1_limit1_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_limit1_status
	},

	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x05, //子索引 
		&off_bytes_board1_limit2_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_limit2_status
	},

	/*
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x0000,                          //索引
		0x00, //子索引 
		&off_bytes_board1_limitgap,//PDO入口在process_data字节偏移量
		&off_bits_board1_limitgap
	},
	*/

	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x1802,                          //索引
		0x07, //子索引 
		&off_bytes_board1_TxPDOState_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_TxPDOState_status
	},
	
	
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x1802,                          //索引
		0x09, //子索引 
		&off_bytes_board1_TxPDO_Toggle_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_TxPDO_Toggle_status
	},
	
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x11, //子索引 
		&off_bytes_board1_Analog_input_status,       //PDO入口在process_data字节偏移量
		&off_bits_board1_Analog_input_status
	},
	
	
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x12, //子索引 
		&off_bytes_board1_Temp_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_Temp_val
	},
	
	{
		IO_BOARD_1_ALIAS,               //别名
		IO_BOARD_1_POSITION,            //位置
		IO_BOARD_1_VENDORID,            //厂商id
		IO_BOARD_1_PRODUCTID,           //产品id
		0x6020,                          //索引
		0x13, //子索引 
		&off_bytes_board1_Hum_val,       //PDO入口在process_data字节偏移量
		&off_bits_board1_Hum_val
	},
	{
	}
}; 
static ec_slave_config_t *board0_slave1_config = NULL;

/*PDO 实时性测试 */
int test_cycle_time =  0;
int dc_model;
long int global_loop_count = 0;
long *global_loop_cycle_time_one_cycle_ns = NULL;
long global_loop_max_time_one_cycle_ns, global_loop_min_time_one_cycle_ns, global_loop_ave_time_one_cycle_ns;

//0~100us, 101~200us, 201~300us, 301~400us, 401~500us, 501~1000us, 1001~10000us, >10000us
HistNode_T result_hist[] = { 
	{{1,   100}, 0, 0.0}, /*0~100us*/
	{{101, 200}, 0, 0.0},
	{{201, 300}, 0, 0.0},
	{{301, 400}, 0, 0.0},
	{{401, 500}, 0, 0.0},
	{{501, 1000}, 0, 0.0},
	{{1001, 10000}, 0, 0.0},
	{{10001, 100000}, 0, 0.0},
	{{100001, 1000000}, 0, 0.0},
	{{0, 0}, 0, 0.0},
	
};

/*
	@description
		Main loop, we obtain real torque info or other info

	@param
		None
		
	@return
		None
*/
static void cycle_task(void)
{
	int first_stage_cycle_count = 0;
	int i = 0, j = 0;
	int leds_count = 0; /*流水灯计数*/
	unsigned char leds_val = 1; /*控制led点点*/
	unsigned char slave0_switchs_val = 0, slave0_switchs_val_previous = 0;  
	unsigned char slave1_switchs_val = 0, slave1_switchs_val_previous = 0;

	LOG("LONG_MAX:%ld\n", LONG_MAX);
	if (test_cycle_time == 1)
	{
			struct timespec start;
			struct timespec stop;
			struct timespec t1, t2, t3, t4, t5, t6, t7, t8, t9, t10;
			long diff_time_ns, max_time_one_cycle_ns, min_time_one_cycle_ns, ave_time_one_cycle_ns;
			long first_stage_cycle_time_arr[INNER_COUNT_TIMES];
			unsigned char read_leds_val;
			char write_flag = 1; /*标示是否要写入数据*/
			int second_stage_cycle_count = 0;
			unsigned char read_switches_val = 0, pre_read_switches_val = 0;
			long secnd_stage_max_time_one_cycle_ns[OUTTER_COUNT_TIMES];
			long secnd_stage_min_time_one_cycle_ns[OUTTER_COUNT_TIMES];
			long secnd_stage_ave_time_one_cycle_ns[OUTTER_COUNT_TIMES];
			memset(first_stage_cycle_time_arr, 0, sizeof(first_stage_cycle_time_arr));

			#if GLOBAl_STATISTIC_ON
			global_loop_cycle_time_one_cycle_ns = (long *) malloc(INNER_COUNT_TIMES * OUTTER_COUNT_TIMES *sizeof(long));
			if (!global_loop_cycle_time_one_cycle_ns)
			{
				LOG("No memory!!!");
				return;
			}
			#endif //GLOBAl_STATISTIC_ON
				
			while (1)
			{
				clock_gettime(CLOCK_REALTIME, &t1);
				usleep(1);
				clock_gettime(CLOCK_REALTIME, &t2);
				
				//receive process data
				clock_gettime(CLOCK_REALTIME, &t3);
				RECEIVE_PDO_DATA(main_master, main_domain);
				clock_gettime(CLOCK_REALTIME, &t4);

				clock_gettime(CLOCK_REALTIME, &t7);
				if (1 == write_flag)
				{
					clock_gettime(CLOCK_REALTIME, &start);
					EC_WRITE_U8(main_domain_pd + off_bytes_board0_led1_val, leds_val);
					write_flag = 0;
				}
				slave0_switchs_val = EC_READ_U8(main_domain_pd + off_bytes_board0_switch1_status);
				clock_gettime(CLOCK_REALTIME, &t8);
				
				if ((leds_val == 1 && BIT_VAL(slave0_switchs_val, 0) == 0) || (leds_val == 0 && BIT_VAL(slave0_switchs_val, 0) == 1))
				{ 
					//slave0_switchs_val_previous = slave0_switchs_val;
					if (0 == slave0_switchs_val)
						slave0_switchs_val = 1;
					else
						slave0_switchs_val = 0;
					clock_gettime(CLOCK_REALTIME, &stop);
					diff_time_ns = DIFF_NS(start, stop);
					first_stage_cycle_time_arr[first_stage_cycle_count] = diff_time_ns;

					if (first_stage_cycle_time_arr[first_stage_cycle_count]/1000 >= 100001) //>100ms
					{
						/*
						LOG("first_stage_cycle_time_arr[%d]=%ldus usleep(1)_actual:%ldus receive_pdo_data_time:%ldus send_pdo_data_time:%ldus r_w_pdo_data_time:%ldus\n"
							"stop.tv_sec:%ld stop.tv_nsec:%ld start.tv_sec:%ld start.tv_nsec:%ld\n",
							first_stage_cycle_count, first_stage_cycle_time_arr[first_stage_cycle_count]/1000, DIFF_NS(t1, t2)/1000, DIFF_NS(t3, t4)/1000, DIFF_NS(t5, t6)/1000, DIFF_NS(t7, t8)/1000,
							stop.tv_sec, stop.tv_nsec, start.tv_sec, start.tv_nsec
							);
						*/
						LOG("first_stage_cycle_time_arr[%d]=%ldus usleep(1)_actual:%ldus receive_pdo_data_time:%ldus send_pdo_data_time:%ldus r_w_pdo_data_time:%ldus\n",
							first_stage_cycle_count, first_stage_cycle_time_arr[first_stage_cycle_count]/1000, DIFF_NS(t1, t2)/1000, DIFF_NS(t3, t4)/1000, DIFF_NS(t5, t6)/1000, DIFF_NS(t7, t8)/1000);
					}

					if (INNER_COUNT_TIMES == first_stage_cycle_count-1)
					{
						first_stage_cycle_count = 0;

						ec_common_get_statistic_results(first_stage_cycle_time_arr, INNER_COUNT_TIMES, &max_time_one_cycle_ns, &min_time_one_cycle_ns, &ave_time_one_cycle_ns);
						LOG("First stage statistic results(uint:ns) CountCycleTimes:%d MaxCycleTime:%ld MinCycleTime:%ld AveCycleTime:%ld\n", \
												INNER_COUNT_TIMES, max_time_one_cycle_ns, min_time_one_cycle_ns, ave_time_one_cycle_ns);
						
						ec_common_get_statistic_results_hist(first_stage_cycle_time_arr, INNER_COUNT_TIMES, result_hist, ARR_SIZE(result_hist));
						ec_common_disp_hist("FirstStageStaHist", result_hist, ARR_SIZE(result_hist));
						ec_common_reset_hist(result_hist, ARR_SIZE(result_hist));
						//memset(first_stage_cycle_time_arr, 0, sizeof(first_stage_cycle_time_arr));
#if 0	

						for (i = 0; i<INNER_COUNT_TIMES; i++)
						{
							first_stage_cycle_time_arr[i] = 0L;
						}
					
						#if SECND_STAGE_STATISTIC_ON
						//二次统计
						secnd_stage_max_time_one_cycle_ns[second_stage_cycle_count] = max_time_one_cycle_ns;
						secnd_stage_min_time_one_cycle_ns[second_stage_cycle_count] = min_time_one_cycle_ns;
						secnd_stage_ave_time_one_cycle_ns[second_stage_cycle_count] = ave_time_one_cycle_ns;
						
						if (OUTTER_COUNT_TIMES == second_stage_cycle_count - 1)
						{
							ec_common_get_statistic_results(secnd_stage_max_time_one_cycle_ns, OUTTER_COUNT_TIMES, &max_time_one_cycle_ns, NULL, NULL);
							ec_common_get_statistic_results(secnd_stage_max_time_one_cycle_ns, OUTTER_COUNT_TIMES, NULL, &min_time_one_cycle_ns, NULL);
							ec_common_get_statistic_results(secnd_stage_max_time_one_cycle_ns, OUTTER_COUNT_TIMES, NULL, NULL, &ave_time_one_cycle_ns);
							LOG("Secnd stage statistic results(uint:ns) OuterCountCycleTimes:%d InnerCountCycleTimes:%d Max_MaxCycleTime:%ld Min_MaxCycleTime:%ld Ave_MaxCycleTime:%ld\n", \
												OUTTER_COUNT_TIMES, INNER_COUNT_TIMES, max_time_one_cycle_ns, min_time_one_cycle_ns, ave_time_one_cycle_ns);
							ec_common_get_statistic_results(secnd_stage_min_time_one_cycle_ns, OUTTER_COUNT_TIMES, &max_time_one_cycle_ns, NULL, NULL);
							ec_common_get_statistic_results(secnd_stage_min_time_one_cycle_ns, OUTTER_COUNT_TIMES, NULL, &min_time_one_cycle_ns, NULL);
							ec_common_get_statistic_results(secnd_stage_min_time_one_cycle_ns, OUTTER_COUNT_TIMES, NULL, NULL, &ave_time_one_cycle_ns);
							LOG("Secnd stage statistic results(uint:ns) OuterCountCycleTimes:%d InnerCountCycleTimes:%d Max_MinCycleTime:%ld Min_MinCycleTime:%ld Ave_MinCycleTime:%ld\n", \
												OUTTER_COUNT_TIMES,INNER_COUNT_TIMES, max_time_one_cycle_ns, min_time_one_cycle_ns, ave_time_one_cycle_ns);
							ec_common_get_statistic_results(secnd_stage_ave_time_one_cycle_ns, OUTTER_COUNT_TIMES, &max_time_one_cycle_ns, NULL, NULL);
							ec_common_get_statistic_results(secnd_stage_ave_time_one_cycle_ns, OUTTER_COUNT_TIMES, NULL, &min_time_one_cycle_ns, NULL);
							ec_common_get_statistic_results(secnd_stage_ave_time_one_cycle_ns, OUTTER_COUNT_TIMES, NULL, NULL, &ave_time_one_cycle_ns);
							LOG("Secnd stage statistic results(uint:ns) OuterCountCycleTimes:%d InnerCountCycleTimes:%d Max_AveCycleTime:%ld Min_AveCycleTime:%ld AveAveCycleTime:%ld\n", \
												OUTTER_COUNT_TIMES,INNER_COUNT_TIMES, max_time_one_cycle_ns, min_time_one_cycle_ns, ave_time_one_cycle_ns);
							second_stage_cycle_count = 0;
							memset(secnd_stage_max_time_one_cycle_ns, 0, sizeof(secnd_stage_ave_time_one_cycle_ns));
							memset(secnd_stage_min_time_one_cycle_ns, 0, sizeof(secnd_stage_ave_time_one_cycle_ns));
							memset(secnd_stage_ave_time_one_cycle_ns, 0, sizeof(secnd_stage_ave_time_one_cycle_ns));
						}
						second_stage_cycle_count++;
						#endif 
						
					}

					#if GLOBAl_STATISTIC_ON
					global_loop_cycle_time_one_cycle_ns[global_loop_count++] = diff_time_ns;
					if (global_loop_count >= MAX_GLOBAL_LOOP_COUNT)
					{
						LOG("Reach max loop count\n");
						ec_common_get_statistic_results(global_loop_cycle_time_one_cycle_ns, global_loop_count, &global_loop_max_time_one_cycle_ns, &global_loop_min_time_one_cycle_ns, &global_loop_ave_time_one_cycle_ns);
						
						LOG("Global statistic results global_loop_count:%ld, global_loop_max_time_one_cycle_ns:%ld global_loop_min_time_one_cycle_ns:%ld global_loop_ave_time_one_cycle_ns:%ld\n", 
							global_loop_count, global_loop_max_time_one_cycle_ns, global_loop_min_time_one_cycle_ns, global_loop_ave_time_one_cycle_ns);
						global_loop_count = 0;
					}
					#endif //GLOBAl_STATISTIC_ON
#endif
					}

					first_stage_cycle_count++;
					write_flag = 1;
					if (1 == leds_val)
					{
						leds_val = 0;
					}
					else
					{
						leds_val = 1;
					}
				}

			   clock_gettime(CLOCK_REALTIME, &t5);
			   // send process data (send process data 与 receive process data 要成对出现)
			   SEND_PDO_DATA(main_master, main_domain);
			   clock_gettime(CLOCK_REALTIME, &t6);
		}
	}
	else //PDO 周期读写
	{
			while (1)
			{
				first_stage_cycle_count++;	
				usleep(10);
				//在一定周期内一定要进行过程数据处理让状态机维持在OP状态
				//receive process data
				ecrt_master_receive(main_master);
		    	ecrt_domain_process(main_domain);
				if (first_stage_cycle_count % 5000 == 0)
				{
					
					LOG("main_domain process data size:%d\n", main_domain_pd_bytes_count);
					for (i = 0; i<main_domain_pd_bytes_count; i++)
					{
						j++;
						LOG("%02X ", EC_READ_U8(main_domain_pd + i));
						if (j % 12 == 0)
						{
							LOG("\n");
						}
						
					}
					LOG("\n");
				}
				if (first_stage_cycle_count % 100 == 0)
				{	
					leds_val = 1 << leds_count;
					EC_WRITE_U8(main_domain_pd + off_bytes_board0_led1_val, leds_val);
					EC_WRITE_U8(main_domain_pd + off_bytes_board1_led1_val, leds_val);
					leds_count++;
					if (leds_count == 8)
					{
						leds_count = 0;
					}
					
				}

				/*
				if (first_stage_cycle_count % 5 == 0)
				{
					slave0_switchs_val = EC_READ_U8(main_domain_pd + off_bytes_board0_switch1_status);
					slave1_switchs_val = EC_READ_U8(main_domain_pd + off_bytes_board1_switch1_status);
					
					if (slave0_switchs_val != slave0_switchs_val_previous)
					{
						LOG("Slave(0) switch status:%d %d %d %d %d %d %d %d\n", BIT_VAL(slave0_switchs_val, 0), BIT_VAL(slave0_switchs_val, 1), BIT_VAL(slave0_switchs_val, 2), \
							BIT_VAL(slave0_switchs_val, 3), BIT_VAL(slave0_switchs_val, 4), BIT_VAL(slave0_switchs_val, 5), BIT_VAL(slave0_switchs_val, 6), 
							BIT_VAL(slave0_switchs_val, 7), BIT_VAL(slave0_switchs_val, 8));
						slave0_switchs_val_previous = slave0_switchs_val;
					}
					if (slave1_switchs_val != slave1_switchs_val_previous)
					{
						LOG("Slave(1) switch status:%d %d %d %d %d %d %d %d\n", BIT_VAL(slave1_switchs_val, 0), BIT_VAL(slave1_switchs_val, 1), BIT_VAL(slave1_switchs_val, 2), \
							BIT_VAL(slave1_switchs_val, 3), BIT_VAL(slave1_switchs_val, 4), BIT_VAL(slave1_switchs_val, 5), BIT_VAL(slave1_switchs_val, 6), 
							BIT_VAL(slave1_switchs_val, 7), BIT_VAL(slave1_switchs_val, 8));
						slave1_switchs_val_previous = slave1_switchs_val;
					}
					
				}
				*/
				// send process data (send process data 与 receive process data 要成对出现)
			    ecrt_domain_queue(main_domain);
			    ecrt_master_send(main_master);
			}
	}
	
}

/*
	@description
		Signal hander

	@param
		Signal num
		
	@return
		None
*/
static void signal_handler(int signum) 
{
	switch (signum) 
	{
		case SIGSEGV:
			break;
		case SIGABRT:
			break;
		case SIGTERM:
			break;
		case SIGQUIT:
			if (main_master)
			{
				ecrt_release_master(main_master);
			}
			if (1 == test_cycle_time)
			{
				if (global_loop_cycle_time_one_cycle_ns)
				{
					ec_common_get_statistic_results(global_loop_cycle_time_one_cycle_ns, global_loop_count, &global_loop_max_time_one_cycle_ns, &global_loop_min_time_one_cycle_ns, &global_loop_ave_time_one_cycle_ns);
					LOG("Global statistic results global_loop_count:%ld, global_loop_max_time_one_cycle_ns:%ld global_loop_min_time_one_cycle_ns:%ld global_loop_ave_time_one_cycle_ns:%ld\n", 
						global_loop_count, global_loop_max_time_one_cycle_ns, global_loop_min_time_one_cycle_ns, global_loop_ave_time_one_cycle_ns);
					free(global_loop_cycle_time_one_cycle_ns); /*一定要释放申请内存*/
				}	
			}
			
			break;
		default:
			return;
			
    }
}

/*
	@description
		Test torque model

	@param
		Ethercat Master device /dev/EtherCATx
		
	@return
		0 :
			Test successfuly
		-1:
			Test failed
*/

int test_io_board(char *master_dev)
{

	struct sigaction sa;
	struct itimerval tv;     
	
	//请求主机
    main_master = ecrt_request_master(0);
    if (NULL != main_master)
    {
		printf("Request master(0) success!\n");
	}
    else
   	{
		printf("Request master(0) failed!\n");
		return -1;
	}
    //请求域, 解释见IgH ethercatmaster手册
    main_domain = ecrt_master_create_domain(main_master);
    if (NULL != main_domain)
    {
		printf("Master create domain success!\n");
	}
	else
	{
		printf("Master create domain failed!\n");
		return -1;
	}

    //获取从机0配置
    if (!(board0_slave_config = ecrt_master_slave_config(main_master, IO_BOARD_0_ALIAS, 
															IO_BOARD_0_POSITION, IO_BOARD_0_VENDORID, IO_BOARD_0_PRODUCTID))) 
    {
        LOG( "Get slave(%d) configuration failed!\n", IO_BOARD_0_POSITION);
        return -1;
    }
	else
	{
		LOG( "Get slave(%d) configuration success!\n", IO_BOARD_0_POSITION);
	}
	
	//从机PDO配置
    if (ecrt_slave_config_pdos(board0_slave_config, EC_END, board0_syncs)) 
	{
        LOG("Configure slave(%d) PDOs failed!\n", IO_BOARD_0_POSITION);
        return -1;
    }
	else
	{
		LOG("Configure slave(%d)PDOs success!\n", IO_BOARD_0_POSITION);
	}
    
	//域注册
	ec_common_disp_ec_pdo_entry_reg_array(slave0_domain_regs);
	if (ecrt_domain_reg_pdo_entry_list(main_domain, slave0_domain_regs))
	{
        LOG( "Reg slave(%d) PDO entry registration failed!ErrMsg:%s\n", strerror(errno), IO_BOARD_0_POSITION);
        return -1;
    }
	else
	{
		LOG("Reg slave(%d) PDO entry registration success\n"
			"off_bytes_board0_switch1_status:%d off_bits_board0_switch1_status:%d\n" 
			"off_bytes_board0_led1_val:%d, off_bits_board0_led1_val:%d\n"
			"off_bytes_board0_led2_val:%d, off_bits_board0_led2_val:%d\n",
			IO_BOARD_0_POSITION, off_bytes_board0_switch1_status, off_bits_board0_switch1_status,
			off_bytes_board0_led1_val, off_bits_board0_led1_val,
			off_bytes_board0_led2_val, off_bits_board0_led2_val);
	}

	//获取从机1配置
    if (!(board0_slave1_config = ecrt_master_slave_config(main_master, IO_BOARD_1_ALIAS, 
															IO_BOARD_1_POSITION, IO_BOARD_VENDORID, IO_BOARD_PRODUCTID))) 
    {
        LOG( "Get slave(%d) configuration failed!\n", IO_BOARD_1_POSITION);
        return -1;
    }
	else
	{
		LOG( "Get slave(%d) configuration success!\n", IO_BOARD_1_POSITION);
	}
	
	//从机PDO配置
    if (ecrt_slave_config_pdos(board0_slave1_config, EC_END, board0_syncs)) 
	{
        LOG("Configure slave(%d) PDOs failed!\n", IO_BOARD_1_POSITION);
        return -1;
    }
	else
	{
		LOG("Configure slave(%d) PDOs success!\n", IO_BOARD_1_POSITION);
	}
    
	//域注册
	if (ecrt_domain_reg_pdo_entry_list(main_domain, board1_domain_regs))
	{
        LOG( "Reg slave(%d) PDO entry registration failed!ErrMsg:%s\n", strerror(errno), IO_BOARD_1_POSITION);
        return -1;
    }
	else
	{
		LOG("Reg slave(%d) PDO entry registration success\n"
			"off_bytes_board1_switch1_status:%d off_bits_board1_switch1_status:%d\n" 
			"off_bytes_board1_led1_val:%d, off_bits_board1_led1_val:%d\n"
			"off_bytes_board1_led2_val:%d, off_bits_board1_led2_val:%d\n",
			IO_BOARD_1_POSITION, off_bytes_board1_switch1_status, off_bits_board1_switch1_status,
			off_bytes_board1_led1_val, off_bits_board1_led1_val,
			off_bytes_board1_led2_val, off_bits_board1_led2_val);
	}
	

	//注册信号处理函数
	sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
	
	if (sigaction(SIGSEGV, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGABRT, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGTERM, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGQUIT, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	#if (PRIORITY == 1)
    pid_t pid = getpid();
    if (setpriority(PRIO_PROCESS, pid, -19))
        LOG("Warning: Failed to set priority: %s\n", strerror(errno));
	#endif
	
	//激活主机
    if (ecrt_master_activate(main_master))
    {
		LOG( "Activate master failed!\n");
		return -1;
	}
	else
	{
		LOG( "Activate master success!\n");
	}
	

	main_domain_pd_bytes_count = ecrt_domain_size(main_domain);
	if (main_domain_pd_bytes_count < 0)
	{
		LOG("Get main_domain_pd_bytes_count error!\n");
		return -1;
	}

	
    if (!(main_domain_pd = ecrt_domain_data(main_domain))) 
	{
		LOG( "ecrt_domain_data(domain1) failed!\n");
		return -1;
    }
	else
	{
		LOG( "ecrt_domain_data(domain1) success main_domain_pd:%p main_domain_pd_bytes_count:%d\n", main_domain_pd, main_domain_pd_bytes_count);
	}
	
	//开始周期任务
	cycle_task();
    return 0;
}


