#ifndef SLAVES_CONFIG_DEFINE_H
#define SLAVES_CONFIG_DEFINE_H

/********************************************
 *    Authour:WuLiang
 *    Email:jxustwl@163.com
 ********************************************/

#include  "ecrt.h"

#define    VENDOR_ID    0x00000017
#define    PRODUCT_CODE 0x26483056
#define    SLAVE_COUNT  2

#define    MASTER_COUNT 1

typedef struct SlaveIdInfo{
	int alias;
	int position;
	int vendorid;
	int productcode;
}SlaveIdInfo_T;

//Select one slave depend on follow data:
//vendorid, productcode, alias, position
//All slave data define in following struct.
typedef struct SlaveSpecifiedInfo {
	SlaveIdInfo_T slave_id_info;
	ec_pdo_entry_info_t *pdo_entries;
	unsigned int pdo_entries_size;
	ec_pdo_info_t *pdos;
	unsigned int pdos_size;
	ec_sync_info_t *syncs;
	unsigned int syns_size;
	//Waring:domain_regs generated by malloc
	//When exit, we should call 'ec_common_free_pdo_entry_reg_arr' to free space
	ec_pdo_entry_reg_t *domain_regs;
}SlaveSpecifiedInfo_T;


//All master data defined in following struct
typedef struct MasterSpecifiedInfo {
	int init_flag;
	int index; //default:0
	ec_master_t *master;
	ec_domain_t *domain;
	unsigned char *domain_pd;
	ec_domain_state_t domain_state;
	ec_master_state_t state;
	unsigned int domain_pd_bytes_count;
	unsigned int slaves_count;
	//Slave info attatched to one master
	struct ec_slave_config *slaves_config;
	//int slaves_configs_count;
	SlaveSpecifiedInfo_T *slaves_specified_infos;
	//unsigned int slaves_specified_info_count;
}MasterSpecifiedInfo_T;

//slave 0 array definition
extern ec_pdo_entry_info_t slave_0_pdo_entries[28];
extern int slave_0_pdo_entries_size;
extern ec_pdo_info_t slave_0_pdos[3];
extern int slave_0_pdos_size;
extern ec_sync_info_t slave_0_syncs[5];
extern int slave_0_syncs_size;

//slave 1 array definition
extern ec_pdo_entry_info_t slave_1_pdo_entries[28];
extern int slave_1_pdo_entries_size;
extern ec_pdo_info_t slave_1_pdos[3];
extern int slave_1_pdos_size;
extern ec_sync_info_t slave_1_syncs[5];
extern int slave_1_syncs_size;

//Last element in slaves_specified_info is '{-1,-1,-1, -1}'to indicate end of array
//slaves_specified_info[i] slave_i info
//Size SLAVE_COUNT+1
extern SlaveSpecifiedInfo_T slaves_specified_info[3];


#endif //SLAVES_CONFIG_DEFINE_H
