#ifndef TEST_TORQUE_TECH_SERVO_H
#define TEST_TORQUE_TECH_SERVO_H


//从设备别名,位置信息，根据手册定义
#define IO_BOARD_VENDORID  		    0x00000017
#define IO_BOARD_PRODUCTID 		    0x26483056
#define IO_BOARD_0_VENDORID  		0x00000017
#define IO_BOARD_0_PRODUCTID 		0x26483056
#define IO_BOARD_0_ALIAS            0x00
#define IO_BOARD_0_POSITION         0x00
#define IO_BOARD_1_VENDORID         IO_BOARD_0_VENDORID
#define IO_BOARD_1_PRODUCTID        IO_BOARD_0_PRODUCTID
#define IO_BOARD_1_ALIAS            0x00
#define IO_BOARD_1_POSITION         0x01



#endif //TEST_TORQUE_TECH_SERVO_H




