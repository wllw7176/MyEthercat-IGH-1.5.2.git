#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "ecrt.h"
#include "../lib/ioctl.h"


#define EC_AL_STATE_BOOT 0x03
#define STATE_STR(x) (x == EC_AL_STATE_INIT ? "STATE_INIT" :\
					  x == EC_AL_STATE_PREOP ? "STATE_PREOP" :\
					  x == EC_AL_STATE_SAFEOP ? "STATE_SAFEOP" :\
					  x == EC_AL_STATE_OP ? "STATE_OP":\
					  x == EC_AL_STATE_BOOT ? "STATE_BOOT":\
					  "UNKNOWN_STATE")

int master_fd;
ec_ioctl_module_t module_data;
ec_ioctl_slave_state_t st_data;
ec_ioctl_slave_t slave_data;
ec_ioctl_config_t config_data;



int main(int argc, char *argv[])
{
	int err;
	

	//data.slave_position = slave_pos;
	if (argc !=2)
	{
		printf("Usage %s devname\n", argv[0]);
		return -1;
	}
	
	master_fd = open(argv[1], O_RDWR);
	if (master_fd < 0)
	{
		printf("Open file %s error \n", argv[1]);
		return -1;
	}	

	//Get info of module and slaves
	err = ioctl(master_fd, EC_IOCTL_MASTER, &module_data);
	if (err)
	{
		printf("Ioctl %s error,errcode:%d\n", "EC_IOCTL_MASTER", err);
		return -1;
	}
	else
	{
		printf("IoctlMagicNum:0X%08X MasterCount:%d\n", module_data.ioctl_version_magic, module_data.master_count);
	}

	err = ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
	if (err)
	{
		printf("ioctl %s error,errcode:%d\n", "EC_IOCTL_SLAVE", err);
		return -1;
	}
	else
	{
		printf("SlaveInfo:VendorId:0x%08X ProudctId:0x%08X Alias:%d Posotion:%d\n", slave_data.vendor_id, slave_data.product_code, slave_data.alias, slave_data.position);
	}


	/*configure DC_SYNC0*/
	config_data.config_index = 0;
	config_data.dc_assign_activate = 0x300;
	//sync0
	config_data.dc_sync[0].cycle_time = 0;
	config_data.dc_sync[0].shift_time = 0;
	//sync1
	config_data.dc_sync[1].cycle_time = 0;
	config_data.dc_sync[1].shift_time = 0;
	
	err = ioctl(master_fd, EC_IOCTL_SC_DC, &config_data);
	if (err)
	{
	    printf("Configure DC SYNC0 error,errcode:%d\n", err);
	   return -1;
	}

	/*configure DC_SYNC1*/
	config_data.config_index = 1;
	config_data.dc_assign_activate = 0x700;
	//sync0
	config_data.dc_sync[0].cycle_time = 0;
	config_data.dc_sync[0].shift_time = 0;
	//sync1
	config_data.dc_sync[1].cycle_time = 0;
	config_data.dc_sync[1].shift_time = 0;
	err = ioctl(master_fd, EC_IOCTL_SC_DC, &config_data);
	if (err)
	{
	   printf("Configure DC SYNC1 error,errcode:%d\n", err);
	   return -1;
	}
	
	
	/*Configurue states*/
	
	st_data.slave_position = slave_data.position;
	
	st_data.al_state = EC_AL_STATE_INIT;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   printf("Set state %s error,errcode:%d\n", "EC_AL_STATE_INIT", err);
	   return -1;
	}
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}

	st_data.al_state = EC_AL_STATE_PREOP;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   printf("Set state %s error,errcode:%d\n", "EC_AL_STATE_PREOP", err);
	   return -1;
	}
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}
	
	st_data.al_state = EC_AL_STATE_BOOT;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   printf("Set state %s error,errcode:%d\n", "EC_AL_STATE_PREOP", err);
	   return -1;
	}
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}

	st_data.al_state = EC_AL_STATE_SAFEOP;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   printf("Set state %s error, errcode:%d\n", "EC_AL_STATE_SAFEOP", err);
	   return -1;
	}
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}

	st_data.al_state = EC_AL_STATE_OP;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   
	   printf("Set state %s error, errcode:%d\n", "EC_AL_STATE_OP", err);
	   return -1;
    }
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}
	
	return 0;
	

}


