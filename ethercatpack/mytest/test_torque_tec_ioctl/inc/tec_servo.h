#ifndef TEST_TORQUE_TECH_SERVO_H
#define TEST_TORQUE_TECH_SERVO_H

/*以下几个宏定义参照山羊电机手册P212*/
//从设备别名,位置信息，根据手册定义
#define TEC_SERVO_AXIS0_ALIAS     0xff
#define TEC_SERVO_AXIS0_POSITION  0x00
#define TEC_SERVO_AXIS0_VENDORID  0x000001B9
#define TEC_SERVO_AXIS0_PRODUCTID 0x00000002

//待访问和设置PDO对象宏定义
#define TEC_SERVO_AXIS0_VENDORID_SDO_INDEX 0x1018
#define TEC_SERVO_AXIS0_VENDORID_SDO_SUBINDEX 0x01
#define TEC_SERVO_AXIS0_PRODUCTID_SDO_INDEX 0x1018
#define TEC_SERVO_AXIS0_PRODUCTID_SDO_SUBINDEX 0x02

//AL 模式定义
#define EC_AL_STATE_BOOT 0x03

//控制字宏定义
#define  TEC_SERVO_AXIS0_CONTROL_WORD_SDO_INDEX                          0x6040 
#define  TEC_SERVO_AXIS0_CONTROL_WORD_SDO_SUBINDEX                       0x00

//控制模式宏定义
#define TEC_SERVO_AXIS0_CONTROL_MODEL_TORQUE                             0x04

//反馈：Position:6064H,  Velocity:6066H, Torque:6077H  这些反馈信息应该都能读上来吧。

//力矩相关对象字典宏定义
#define  MAX_OUTPUT_TORQUE                                               100
#define  TEC_SERVO_AXIS0_MODEL_CONFIGURE_SDO_INDEX                       0x6060
#define  TEC_SERVO_AXIS0_MODEL_CONFIGURE_SDO_SUBINDEX                    0x00
#define  TEC_SERVO_AXIS0_TORQUE_PLAN_TYPE_SDO_INDEX                      0x6088
#define  TEC_SERVO_AXIS0_TORQUE_PLAN_TYPE_SDO_SUBINDEX                   0x00
#define  TEC_SERVO_AXIS0_TORQUE_GRADIENT_SDO_INDEX                       0x6087 
#define  TEC_SERVO_AXIS0_TORQUE_GRADIENT_SDO_SUBINDEX                    0x00
#define  TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_INDEX                           0x6071 
#define  TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_SUBINDEX                        0x00
#define  TEC_SERVO_AXIS0_MAX_TORQUE_SDO_INDEX                            0x6072 
#define  TEC_SERVO_AXIS0_MAX_TORQUE_SDO_SUBINDEX                         0x00
#define  TEC_SERVO_AXIS0_REAL_TORQUE_SDO_INDEX                           0x6077 
#define  TEC_SERVO_AXIS0_REAL_TORQUE_SDO_SUBINDEX                        0x00

//编码器相关对象字典定义 P232
#define  TEC_SERVO_AXIS0_ENCODER_INNER_REAL_POS_SDO_INDEX                0x6063
#define  TEC_SERVO_AXIS0_ENCODER_INNER_REAL_POS_SDO_SUBINDEX             0x00
#define  TEC_SERVO_AXIS0_ENCODER_REAL_POS_SDO_INDEX                      0x6064
#define  TEC_SERVO_AXIS0_ENCODER_REAL_POS_SDO_SUBINDEX                   0x00
#define  TEC_SERVO_AXIS0_ENCODER_REAL_VELOCITY_SDO_INDEX                 0x606C
#define  TEC_SERVO_AXIS0_ENCODER_REAL_VELOCITY_SDO_SUBINDEX              0x00

//控制模式宏定义
#define TEC_SERVO_CONROL_MODE_PROFILE_POSITION                     1
#define TEC_SERVO_CONROL_MODE_PROFILE_VELOCITY                     3
#define TEC_SERVO_CONROL_MODE_PROFILE_TORQUE                       4
#define TEC_SERVO_CONROL_MODE_HOMING                               6
#define TEC_SERVO_CONROL_MODE_INTERPOLATED_POSITION_               7
#define TEC_SERVO_CONROL_MODE_CYCLIC_SYNCHRONOUS_POSITION          8
#define TEC_SERVO_CONROL_MODE_CYCLIC_SYNCHRONOUS_VELOCITY          9
#define TEC_SERVO_CONROL_MODE_CYCLIC_SYNCHRONOUS_TORQUE            10


//状态字符串，调试用
#define AL_STATE_STR(x) (x == EC_AL_STATE_INIT   ? "AL_STATE_INIT"     :\
					     x == EC_AL_STATE_PREOP  ? "AL_STATE_PREOP"    :\
					     x == EC_AL_STATE_SAFEOP ? "AL_STATE_SAFEOP"   :\
					     x == EC_AL_STATE_OP     ? "AL_STATE_OP"       :\
					     x == EC_AL_STATE_BOOT   ? "AL_STATE_BOOT"     : "AL_STATE_UNKNOWN")
					  
#define DOMAIN_STATE_STR(x) (x == EC_WC_ZERO       ? "DOMAIN_STATE_WC_ZERO"       : \
	                         x == EC_WC_INCOMPLETE ? "DOMAIN_STATE_WC_INCOMPLETE" : \
	                         x == EC_WC_COMPLETE   ? "DOMAIN_STATE_WC_COMPLETE"   : "DOMAIN_STATE_UNKNOWN")

//电机运行模式
#define TEC_SERVO_RUN_MODEL_FREE 0x00
#define TEC_SERVO_RUN_MODEL_DC 0x01


#endif //TEST_TORQUE_TECH_SERVO_H




