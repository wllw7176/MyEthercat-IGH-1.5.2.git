#ifndef MY_TEST_TORQUE_H
#define MY_TEST_TORQUE_H

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdint.h>
#include "ecrt.h"
#include "ioctl.h"
#include "tec_servo.h"


//是否启动调试模式
#define DEBUG_MODEL 0


int test_torque(char *master_dev);


/******************************For Debug**************************************/
#define CMDID_INVALID                              -1
#define CMDID_SET_AXIS_ON_OFF                       1
#define CMDID_SET_AXIS_MAX_OUTPUT_TORQUE            2
#define CMDID_SET_AXIS_GOAL_OUTPUT_TORQUE           3
#define CMDID_GET_AXIS_REAL_OUTPUT_TORQUE           4
#define CMDID_GET_AXIS_ENCODER_INNER_REAL_POS       5
#define CMDID_GET_AXIS_ENCODER_REAL_POS             6
#define CMDID_GET_AXIS_ENCODER_VELOCITY             7
#define CMDID_QUIT_DEBUG                            0

typedef int (*cmd_callback)(int master_fd, unsigned short slave_position, int *arg);
typedef struct CmdNode{
	int cmdid;
	cmd_callback f;
}CmdNode;

int cmd_process(int cmdid, int master_fd, unsigned short slave_position, int *arg);
void debug_menu(void);
void debug_torque(char *master_dev);
/******************************For Debug**************************************/


#endif //MY_TEST_TORQUE_H


