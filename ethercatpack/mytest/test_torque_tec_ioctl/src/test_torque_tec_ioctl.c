#include "test.h"

// process data
ec_ioctl_slave_sdo_download_t sdo_download_data;
ec_ioctl_slave_sdo_download_t sdo_upload_data;
int master_fd = -1;
int arg_val = 0;


/*
	@description
		Download data into slaves
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want download 
		
	@return
		0:
			download success 
		-1:
			dowolad failed
*/
static int sdo_download(int fd, ec_ioctl_slave_sdo_download_t *data)
{
	int i, err;
	err = ioctl(fd, EC_IOCTL_SLAVE_SDO_DOWNLOAD, data);
	if (err) 
	{
#if (DEBUG_MODEL == 1)
		LOG("sdo_download index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" failed,ErrMsg:%s\n", strerror(errno));
#endif //#if DEBUG_MODEL
		return -1;
    }
	else
	{
#if (DEBUG_MODEL == 1)
		struct timespec start;
		struct timespec stop;
		long diff_time_ns;
		
		clock_gettime(CLOCK_REALTIME, &start);
		ioctl(fd, EC_IOCTL_SLAVE_SDO_DOWNLOAD, data);
		clock_gettime(CLOCK_REALTIME, &stop);
		diff_time_ns = (stop.tv_sec - start.tv_sec) * 1000000000 + (stop.tv_nsec - start.tv_nsec);
		
		LOG("sdo_download index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" success\n");
		
		LOG("One time sdo download spend %ld ns\n", diff_time_ns);
#endif //#if DEBUG_MODEL
		return 0;
	}
}


/*
	@description
		Upload data into slaves
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want to store data of upload 
		
	@return
		0:
			upload success 
		-1:
			upload failed
*/
static int sdo_upload(int fd, ec_ioctl_slave_sdo_download_t *data)
{
	int i, err;
	err = ioctl(fd, EC_IOCTL_SLAVE_SDO_UPLOAD, data);
	if (err) 
	{
#if (DEBUG_MODEL == 1)
		LOG("SdoUpload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" failed,ErrMsg:%s\n", strerror(errno));
#endif //#if DEBUG_MODEL
		return -1;
    }
	else
	{
#if (DEBUG_MODEL == 1)
		struct timespec start;
		struct timespec stop;
		long diff_time_ns;
		
		clock_gettime(CLOCK_REALTIME, &start);
		ioctl(fd, EC_IOCTL_SLAVE_SDO_UPLOAD, data);
		clock_gettime(CLOCK_REALTIME, &stop);
		diff_time_ns = (stop.tv_sec - start.tv_sec) * 1000000000 + (stop.tv_nsec - start.tv_nsec);
		
		LOG("SdoUpload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" success\n");
		LOG("One time sdo upload spend %ld ns\n", diff_time_ns);
#endif //#if DEBUG_MODEL
		return 0;
	}
}

/*
	@description
		Set slave states
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want to store data of set 
		
	@return
		0:
			Set state success 
		-1:
			Set state failed
*/
static int set_slave_state(int master_fd, unsigned short slave_position, unsigned char al_state)
{
	
	ec_ioctl_slave_state_t data;

	data.al_state = slave_position;
	data.slave_position = slave_position;
	
	return ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &data);

}


static int set_axis_on_off(int master_fd, unsigned short slave_position, int *arg_val)
{

	unsigned char download_data;
	int err;
	
	sdo_download_data.slave_position = slave_position;
	sdo_download_data.sdo_index = TEC_SERVO_AXIS0_CONTROL_WORD_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_CONTROL_WORD_SDO_SUBINDEX;
	sdo_download_data.data = &download_data;
	sdo_download_data.data_size = sizeof(download_data);
	
	if (*arg_val == 1)
	{
		//设置 驱动器伺服ON
		download_data = 0x06;
		err = sdo_download(master_fd, &sdo_download_data);
		if (err)
		{
			return err;
		}
		download_data = 0x07;
		err = sdo_download(master_fd, &sdo_download_data);
		if (err)
		{
			return err;
		}
		download_data = 0x0f;
		err = sdo_download(master_fd, &sdo_download_data);
		return err;
	}
	else
	{
		//设置 驱动器伺服OFF
		download_data = 0x0f;
		err = sdo_download(master_fd, &sdo_download_data);
		if (err)
		{
			return err;
		}
		download_data = 0x06;
		err = sdo_download(master_fd, &sdo_download_data);
		return err;
	}
	
}

static int set_axis_max_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	short torque_data;
	torque_data= *arg_val;
	int err;
	sdo_download_data.slave_position = slave_position;
	sdo_download_data.sdo_index = TEC_SERVO_AXIS0_MAX_TORQUE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_MAX_TORQUE_SDO_SUBINDEX;
	sdo_download_data.data = (unsigned char *)&torque_data;
	sdo_download_data.data_size = sizeof(torque_data);
	err = sdo_download(master_fd, &sdo_download_data);
	return err;
}

static int set_axis_goal_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	short torque_data = *arg_val;
	int err;
	sdo_download_data.slave_position = slave_position;
	sdo_download_data.sdo_index = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_SUBINDEX;
	sdo_download_data.data = (unsigned char *)&torque_data;
	sdo_download_data.data_size = sizeof(torque_data);
	err = sdo_download(master_fd, &sdo_download_data);
	return err;
}

static int get_axis_real_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	int err;
	sdo_upload_data.slave_position = slave_position;
	sdo_upload_data.sdo_index = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_INDEX;
	sdo_upload_data.sdo_entry_subindex = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_SUBINDEX;
	sdo_upload_data.data = (unsigned char*)arg_val;
	sdo_upload_data.data_size = sizeof(int);
	err = sdo_upload(master_fd, &sdo_upload_data);
	*arg_val = (*(int *)(sdo_upload_data.data));
	return err;
		
}

static int get_axis_encoder_inner_real_pos(int master_fd, unsigned short slave_position, int *arg_val)
{

	int err;
	sdo_upload_data.slave_position = slave_position;
	sdo_upload_data.sdo_index = TEC_SERVO_AXIS0_ENCODER_INNER_REAL_POS_SDO_INDEX;
	sdo_upload_data.sdo_entry_subindex = TEC_SERVO_AXIS0_ENCODER_INNER_REAL_POS_SDO_SUBINDEX;
	sdo_upload_data.data = (unsigned char*)arg_val;
	sdo_upload_data.data_size = sizeof(int);
	err = sdo_upload(master_fd, &sdo_upload_data);
	*arg_val = (*(int *)(sdo_upload_data.data));
	return err;
		
}

static int get_axis_encoder_real_pos(int master_fd, unsigned short slave_position, int *arg_val)
{

	int err;
	sdo_upload_data.slave_position = slave_position;
	sdo_upload_data.sdo_index = TEC_SERVO_AXIS0_ENCODER_REAL_POS_SDO_INDEX;
	sdo_upload_data.sdo_entry_subindex = TEC_SERVO_AXIS0_ENCODER_REAL_POS_SDO_SUBINDEX;
	sdo_upload_data.data = (unsigned char*)arg_val;
	sdo_upload_data.data_size = sizeof(int);
	err = sdo_upload(master_fd, &sdo_upload_data);
	*arg_val = (*(int *)(sdo_upload_data.data));
	return err;
		
}

static int get_axis_encoder_velocity(int master_fd, unsigned short slave_position, int *arg_val)
{

	int err;
	sdo_upload_data.slave_position = slave_position;
	sdo_upload_data.sdo_index = TEC_SERVO_AXIS0_ENCODER_REAL_VELOCITY_SDO_INDEX;
	sdo_upload_data.sdo_entry_subindex = TEC_SERVO_AXIS0_ENCODER_REAL_VELOCITY_SDO_SUBINDEX;
	sdo_upload_data.data = (unsigned char*)arg_val;
	sdo_upload_data.data_size = sizeof(int);
	err = sdo_upload(master_fd, &sdo_upload_data);
	*arg_val = (*(int *)(sdo_upload_data.data));
	return err;
		
}


/*
	@description
		Main loop, we obtain real torque info or other info

	@param
		None
		
	@return
		None
*/
static void cycle_task(void)
{
	static int inner_pos;
	static int pos;
	static int vel;
	static char dir_flag = 1;
	static int err = 0;
	static short torque_data;
	static long int count = 0;

#if (DEBUG_MODEL == 1)
	struct timespec start;
	struct timespec stop;
	long diff_time_ms;
#endif
	
	while (1)
	{

#if (DEBUG_MODEL == 1)
		clock_gettime(CLOCK_REALTIME, &start);
		diff_time_ms = (stop.tv_sec - start.tv_sec) * 1000 + (stop.tv_nsec - start.tv_nsec) / 1000000;
		LOG("One cycle spend %ldms\n", diff_time_ms);	
#endif
		usleep(1000);
		// receive process data
		ioctl(master_fd, EC_IOCTL_RECEIVE, NULL);
		ioctl(master_fd, EC_IOCTL_DOMAIN_PROCESS, TEC_SERVO_AXIS0_POSITION);

		sdo_upload_data.slave_position = TEC_SERVO_AXIS0_POSITION;
		sdo_upload_data.sdo_index = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_INDEX;
		sdo_upload_data.sdo_entry_subindex = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_SUBINDEX;
		sdo_upload_data.data = (unsigned char*)&torque_data;
		sdo_upload_data.data_size = sizeof(torque_data);
		err = sdo_upload(master_fd, &sdo_upload_data);
		if (err) 
		{
			LOG("Get axis:%d real_torque failed!\n", sdo_upload_data.slave_position);
			continue;
		}
		torque_data = (*(short *)(sdo_upload_data.data));
		
		sdo_upload_data.sdo_index = TEC_SERVO_AXIS0_ENCODER_INNER_REAL_POS_SDO_INDEX;
		sdo_upload_data.sdo_entry_subindex = TEC_SERVO_AXIS0_ENCODER_INNER_REAL_POS_SDO_SUBINDEX;
		sdo_upload_data.data = (unsigned char*)&inner_pos;
		sdo_upload_data.data_size = sizeof(inner_pos);
		if (err) 
		{
			LOG("Get axis:%d inner_pos failed!\n", sdo_upload_data.slave_position);
			continue;
		}
		inner_pos= (*(int *)(sdo_upload_data.data));

		sdo_upload_data.sdo_index = TEC_SERVO_AXIS0_ENCODER_REAL_POS_SDO_INDEX;
		sdo_upload_data.sdo_entry_subindex = TEC_SERVO_AXIS0_ENCODER_REAL_POS_SDO_SUBINDEX;
		sdo_upload_data.data = (unsigned char*)&pos;
		sdo_upload_data.data_size = sizeof(pos);
		err = sdo_upload(master_fd, &sdo_upload_data);
		if (err) 
		{
			LOG("Get axis:%d pos failed!\n", sdo_upload_data.slave_position);
			continue;
		}
		pos= (*(int *)(sdo_upload_data.data));

		sdo_upload_data.sdo_index = TEC_SERVO_AXIS0_ENCODER_REAL_VELOCITY_SDO_INDEX;
		sdo_upload_data.sdo_entry_subindex = TEC_SERVO_AXIS0_ENCODER_REAL_VELOCITY_SDO_SUBINDEX;
		sdo_upload_data.data = (unsigned char*)&vel;
		sdo_upload_data.data_size = sizeof(vel);
		err = sdo_upload(master_fd, &sdo_upload_data);
		if (err) 
		{
			LOG("Get axis:%d velocity failed!\n", sdo_upload_data.slave_position);
			continue;
		}
		vel= (*(int *)(sdo_upload_data.data));
		
		if (!err)
		{
			LOG("Get axis:%d torque:%d inner_pos:%d pos:%d velocity:%d\n", sdo_upload_data.slave_position, torque_data, inner_pos, pos, vel);
		}

		if (count++ % 200 == 0) //换方向
		{	

			LOG("Warning inverse dir...\n");
			if (1 == dir_flag) 
			{
				dir_flag = 0;
				torque_data = 100;
				sdo_download_data.slave_position = TEC_SERVO_AXIS0_POSITION;
				sdo_download_data.sdo_index = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_INDEX;
				sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_SUBINDEX;
				sdo_download_data.data = (unsigned char *)&torque_data;
				sdo_download_data.data_size = sizeof(torque_data);
				err = sdo_download(master_fd, &sdo_download_data);
				if (err) 
				{
					LOG("Set goal_torque:%d failed!\n", torque_data);
				}
				else 
				{
					LOG("Set goal_torque:%d success!\n", torque_data);
				}
			}
			else
			{
				dir_flag = 1;
				torque_data = -100;
				sdo_download_data.slave_position = TEC_SERVO_AXIS0_POSITION;
				sdo_download_data.sdo_index = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_INDEX;
				sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_SUBINDEX;
				sdo_download_data.data = (unsigned char *)&torque_data;&torque_data;
				sdo_download_data.data_size = sizeof(torque_data);
				err = sdo_download(master_fd, &sdo_download_data);
				if (err) 
				{
					LOG("Set goal_torque:%d failed!\n", torque_data);
				}
				else 
				{
					LOG("Set goal_torque:%d success!\n", torque_data);
				}
			}
			
			
		}
		
		// send process data
		ioctl(master_fd, EC_IOCTL_SEND, NULL);
		ioctl(master_fd, EC_IOCTL_DOMAIN_QUEUE, TEC_SERVO_AXIS0_POSITION);
#if (DEBUG_MODEL == 1)
	clock_gettime(CLOCK_REALTIME, &stop);
#endif
	}
	
}

/*
	@description
		Signal hander

	@param
		Signal num
		
	@return
		None
*/
static void signal_handler(int signum) 
{
	
	switch (signum) 
	{
		case SIGALRM:
			break;
		case SIGSEGV:
		case SIGABRT:
			set_axis_on_off(master_fd, TEC_SERVO_AXIS0_POSITION, &arg_val);
			if ( -1 != master_fd)
			{
				close(master_fd);
			}
			
			break;
		default:
			return;
			
    }
}

/*
	@description
		Test torque model

	@param
		Ethercat Master device /dev/EtherCATx
		
	@return
		0 :
			Test successfuly
		-1:
			Test failed
*/
int test_torque(char *master_dev)
{
	ec_slave_config_t *sc;
    struct sigaction sa;
    struct itimerval tv;
	short run_mode;
	char contorl_mode;
	unsigned char slave_state;
	unsigned char download_data;
	short torque_data; 
	unsigned char recv_buff[4];
	int err;
	uint8_t state = 0x00;
	
	/********************************************************************/
	if (NULL == master_dev)
	{
		fprintf(stderr, "Null mater_dev pointer!\n");
		return -1;
	}
	
	master_fd = open(master_dev, O_RDWR);
	if (master_fd < 0)
	{
		LOG("Open file %s error! \n", master_dev);
		return -1;
	}
	
	/********************************************************************/
	//sdo_download_data.complete_access = 0;
	/*配置电机运行自由运行模式*/
	
	run_mode = TEC_SERVO_RUN_MODEL_FREE;
	sdo_download_data.slave_position = TEC_SERVO_AXIS0_POSITION;
	sdo_download_data.sdo_index = 0x1c32;
	sdo_download_data.sdo_entry_subindex = 0x01;
	sdo_download_data.data = (uint8_t *)&run_mode;
	sdo_download_data.data_size = sizeof(run_mode);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		close(master_fd);
		return err;
	}
	run_mode = TEC_SERVO_RUN_MODEL_FREE;
	sdo_download_data.slave_position = TEC_SERVO_AXIS0_POSITION;
	sdo_download_data.sdo_index = 0x1c33;
	sdo_download_data.sdo_entry_subindex = 0x01;
	sdo_download_data.data = (uint8_t *)&run_mode;
	sdo_download_data.data_size = sizeof(run_mode);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		
		LOG("Set free model failed!\n");
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set free model success!\n");
	}
	
	
	/*配置驱动器进入力矩控制模式*/
	//设置驱动器成转矩模式
	contorl_mode = TEC_SERVO_AXIS0_CONTROL_MODEL_TORQUE;
	sdo_download_data.slave_position = TEC_SERVO_AXIS0_POSITION;
	sdo_download_data.sdo_index = TEC_SERVO_AXIS0_MODEL_CONFIGURE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_MODEL_CONFIGURE_SDO_SUBINDEX;
	sdo_download_data.data = (uint8_t *)&contorl_mode;
	sdo_download_data.data_size = sizeof(contorl_mode);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set torque control model failed!\n");
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set torque control model success!\n");
	}
	
	//设置转矩(推力)规划类型 先用默认值  不设置
	//设置 转矩(推力)斜率 先用默认值 不设置

	//设置最大目标转矩
	//实际输出转矩=set_value * 0.1% * 额定输出转矩
	//此处为了测试安全 将输出设置最大转矩10%
	torque_data= 100;
	sdo_download_data.slave_position = TEC_SERVO_AXIS0_POSITION;
	sdo_download_data.sdo_index = TEC_SERVO_AXIS0_MAX_TORQUE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_MAX_TORQUE_SDO_SUBINDEX;
	sdo_download_data.data = (unsigned char *)&torque_data;
	sdo_download_data.data_size = sizeof(torque_data);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set max_torque:%d failed!\n", torque_data);
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set max_torque:%d success!\n", torque_data);
	}
	
	//设置目标转矩(推力)
	torque_data = 100;
	sdo_download_data.slave_position = TEC_SERVO_AXIS0_POSITION;
	sdo_download_data.sdo_index = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_SUBINDEX;
	sdo_download_data.data = (unsigned char *)&torque_data;
	sdo_download_data.data_size = sizeof(torque_data);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set goal_torque:%d failed!\n", torque_data);
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set goal_torque:%d success!\n", torque_data);
	}
	
	//设置 驱动器伺服ON
	short tmp_data = 0x06;
	sdo_download_data.slave_position = TEC_SERVO_AXIS0_POSITION;
	sdo_download_data.sdo_index = TEC_SERVO_AXIS0_CONTROL_WORD_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SERVO_AXIS0_CONTROL_WORD_SDO_SUBINDEX;
	sdo_download_data.data = (uint8_t *)&tmp_data;
	sdo_download_data.data_size = sizeof(tmp_data);
	sdo_download(master_fd, &sdo_download_data);
	tmp_data = 0x07;
	sdo_download(master_fd, &sdo_download_data);
	tmp_data = 0x0f;
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set server on faild!\n");
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set server on success!\n");
	}

	//注意注意注意:
	//直接用ioctl方式进行操作,一定要在此处调用系统调用system设置从机为OP
	//直接用ioctl控制方式设置OP模式会出错
	system("ethercat -a 255 -p 0 states OP");

	/********************************************************************/
    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
	if (sigaction(SIGTERM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGSEGV, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
   
	/********************************************************************/
	// 开始主循环
	cycle_task();
	
    /********************************************************************/
	close(master_fd);
	
    return 0;
}


/******************************For Debug**************************************/
static int quit_debug(int master_fd, unsigned short slave_position, int *arg_val)
{
	exit(0);
	return 0;
}

CmdNode cmd_list[] = {
		{CMDID_SET_AXIS_ON_OFF, set_axis_on_off},
		{CMDID_SET_AXIS_MAX_OUTPUT_TORQUE, set_axis_max_output_torque},
		{CMDID_SET_AXIS_GOAL_OUTPUT_TORQUE, set_axis_goal_output_torque},
		{CMDID_GET_AXIS_REAL_OUTPUT_TORQUE, get_axis_real_output_torque},
		{CMDID_GET_AXIS_ENCODER_INNER_REAL_POS, get_axis_encoder_inner_real_pos},
		{CMDID_GET_AXIS_ENCODER_REAL_POS, get_axis_encoder_real_pos},
		{CMDID_GET_AXIS_ENCODER_VELOCITY, get_axis_encoder_velocity},
		{CMDID_QUIT_DEBUG, quit_debug},
		{CMDID_INVALID, NULL}
};
#define CMD_LIST_SIZE (sizeof(cmd_list) / sizeof(cmd_list[0]))

int cmd_process(int cmdid, int master_fd, unsigned short slave_position, int *arg)
{
	int i;
	for (i = 0; i<CMD_LIST_SIZE; i++) 
	{
		if (cmdid == cmd_list[i].cmdid)
		{
			return cmd_list[i].f(master_fd, slave_position, arg);
		}
	}
	if (i == CMD_LIST_SIZE)
	{
		return -1;
	}
}

void debug_menu(void)
{

	printf("****************Ethercat Servo Torque Debug****************\n");
	printf("1 Set axis servo on/off                     (on:cmdid axis_num 1 off:cmdid axi_num 0)\n");
	printf("2 Set axis max output_torque                (cmdid axis_num max_output_torque)\n");
	printf("3 Set axis goal torque                      (cmdid axis_num goal_torque)\n");
	printf("4 Get axis real outpue torque               (cmdid axis_num 0)\n");
	printf("5 Get axis encoder inner real pos           (cmdid 0 0)\n");
	printf("6 Get axis encoder real pos                 (cmdid 0 0)\n");
	printf("7 Get axis encoder velocity                 (cmdid 0 0)\n");
	printf("0 Quit debug system                         (cmdid 0 0)\n");
	printf("Input cmd:");
	
}

void debug_torque(char *master_dev)
{

	unsigned short slave_positon = TEC_SERVO_AXIS0_POSITION;
	int arg_val;
	int cmdid;
	
	if (NULL == master_dev)
	{
		LOG("Null mater_dev pointer!\n");
		return;
	}
	master_fd = open(master_dev, O_RDWR);
	if (master_fd < 0)
	{
		LOG("Open file %s error! \n", master_dev);
		return;
	}

	//为了测试安全 先设置一个小点的最大输出扭矩
	cmdid = CMDID_SET_AXIS_MAX_OUTPUT_TORQUE;
	arg_val = 100;
	cmd_process(cmdid, master_fd, slave_positon, &arg_val);
	
	while (1)
	{
		debug_menu();
		if (scanf("%d %d %d", &cmdid, &slave_positon, &arg_val) != 3)
		{
			LOG("Error input cmd!!!\n");			
			getchar();
			continue;
		}
		if (TEC_SERVO_AXIS0_POSITION != slave_positon)
		{
			LOG("Waring now only supported slave_positon=%d\n", TEC_SERVO_AXIS0_POSITION);
			continue;
		}
		if (
			cmdid == CMDID_GET_AXIS_REAL_OUTPUT_TORQUE     ||
			cmdid == CMDID_GET_AXIS_ENCODER_INNER_REAL_POS || 
			cmdid == CMDID_GET_AXIS_ENCODER_REAL_POS       || 
			cmdid == CMDID_GET_AXIS_ENCODER_VELOCITY
		   )
		{
			if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
			{
				LOG("Execute cmd success!\n");
				LOG("Get Axis:%d Value:%d\n", slave_positon, arg_val);
				continue;
			}
			
		}
		if (cmdid == CMDID_SET_AXIS_MAX_OUTPUT_TORQUE )
		{
			if (arg_val >= MAX_OUTPUT_TORQUE)
			{
				arg_val = MAX_OUTPUT_TORQUE;
				LOG("Warning max output torque limit(0~%d)!\n", MAX_OUTPUT_TORQUE);
				
			}
			if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
			{
				LOG("Execute cmd success!\n");
				continue;
			}
		}
		if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
		{
			LOG("Execute cmd success!\n");
			continue;
		}
		
	}
	
	
}
/******************************For Debug**************************************/




