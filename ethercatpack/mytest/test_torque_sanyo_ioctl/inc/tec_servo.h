#ifndef TEST_TORQUE_TECH_SERVO_H
#define TEST_TORQUE_TECH_SERVO_H

/*********************泰科伺服相关宏定义*************************/
#define TEC_SERVO_AXIS0_ALIAS                 255
#define TEC_SERVO_AXIS0_POSITION              0

//控制字宏定义
#define  TEC_SRVO_CONTROL_WORD_SDO_INDEX                          0x6040 
#define  TEC_SRVO_CONTROL_WORD_SDO_SUBINDEX                       0x00

//控制模式宏定义
#define TEC_SRVO_CONTROL_MODEL_TORQUE                             0x04

#define  MAX_OUTPUT_TORQUE                                        100
#define  TEC_SRVO_MODEL_CONFIGURE_SDO_INDEX                       0x6060
#define  TEC_SRVO_MODEL_CONFIGURE_SDO_SUBINDEX                    0x00
#define  TEC_SRVO_TORQUE_PLAN_TYPE_SDO_INDEX                      0x6088
#define  TEC_SRVO_TORQUE_PLAN_TYPE_SDO_SUBINDEX                   0x00
#define  TEC_SRVO_TORQUE_GRADIENT_SDO_INDEX                       0x6087 
#define  TEC_SRVO_TORQUE_GRADIENT_SDO_SUBINDEX                    0x00
#define  TEC_SRVO_GOAL_TORQUE_SDO_INDEX                           0x6071 
#define  TEC_SRVO_GOAL_TORQUE_SDO_SUBINDEX                        0x00
#define  TEC_SRVO_MAX_TORQUE_SDO_INDEX                            0x6072 
#define  TEC_SRVO_MAX_TORQUE_SDO_SUBINDEX                         0x00
#define  TEC_SRVO_REAL_TORQUE_SDO_INDEX                           0x6077 
#define  TEC_SRVO_REAL_TORQUE_SDO_SUBINDEX                        0x00


#define  TEC_SRVO_ENCODER_INNER_REAL_POS_SDO_INDEX                0x6063
#define  TEC_SRVO_ENCODER_INNER_REAL_POS_SDO_SUBINDEX             0x00
#define  TEC_SRVO_ENCODER_REAL_POS_SDO_INDEX                      0x6064
#define  TEC_SRVO_ENCODER_REAL_POS_SDO_SUBINDEX                   0x00
#define  TEC_SRVO_ENCODER_REAL_VELOCITY_SDO_INDEX                 0x606C
#define  TEC_SRVO_ENCODER_REAL_VELOCITY_SDO_SUBINDEX              0x00


#endif //TEST_TORQUE_TECH_SERVO_H




