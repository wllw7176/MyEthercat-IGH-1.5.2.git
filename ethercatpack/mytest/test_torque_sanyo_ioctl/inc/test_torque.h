#ifndef MY_TEST_TORQUE_H
#define MY_TEST_TORQUE_H

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdint.h>
#include "ecrt.h"
#include "ioctl.h"


#define PRINTF_LEVEL 1
#if (PRINTF_LEVEL == 0)
#define LOG(...) printf("DEBUG=======>"__VA_ARGS__)
#define LOG_ERROR(err_msg) printf("DEBUG_ERROR=======> %s:%s:%d", __FILE__, __FUNCTION__, __LINE__);printf(" (err_msg:%s)\n", err_msg)
#elif  (PRINTF_LEVEL == 1)
#define LOG(...) printf(__VA_ARGS__)
#define LOG_ERROR(err_msg) printf("DEBUG_ERROR=======> %s:%s:%d", __FILE__, __FUNCTION__, __LINE__);printf(" (err_msg:%s)\n", err_msg)
#else
#define LOG(...) do {} while(0)
#define LOG_ERROR(err_msg) do {} while(0)
#endif

//是否启动调试模式
#define DEBUG_MODEL 1

/*以下几个宏定义参照山羊电机手册P212*/
//从设备别名,位置信息，根据手册定义
#define SANMOTION_RS2_ALIAS 0x0000
#define SANMOTION_RS2_POSITION 0x0000
#define SANMOTION_RS2_VENDORID  0x000001B9
#define SANMOTION_RS2_PRODUCTID 0x00000002

//待访问和设置PDO对象宏定义
#define SANMOTION_RS2_VENDORID_SDO_INDEX 0x1018
#define SANMOTION_RS2_VENDORID_SDO_SUBINDEX 0x01
#define SANMOTION_RS2_PRODUCTID_SDO_INDEX 0x1018
#define SANMOTION_RS2_PRODUCTID_SDO_SUBINDEX 0x02

//AL 模式定义
#define EC_AL_STATE_BOOT 0x03

//控制字宏定义
#define  SANMOTION_RS2_CONTROL_WORD_SDO_INDEX                          0x6040 
#define  SANMOTION_RS2_CONTROL_WORD_SDO_SUBINDEX                       0x00

//控制模式宏定义
#define SANMOTION_RS2_CONTROL_MODEL_TORQUE                             0x04

//反馈：Position:6064H,  Velocity:6066H, Torque:6077H  这些反馈信息应该都能读上来吧。

//力矩相关对象字典宏定义
#define  MAX_OUTPUT_TORQUE                                             100
#define  SANMOTION_RS2_MODEL_CONFIGURE_SDO_INDEX                       0x6060
#define  SANMOTION_RS2_MODEL_CONFIGURE_SDO_SUBINDEX                    0x00
#define  SANMOTION_RS2_TORQUE_PLAN_TYPE_SDO_INDEX                      0x6088
#define  SANMOTION_RS2_TORQUE_PLAN_TYPE_SDO_SUBINDEX                   0x00
#define  SANMOTION_RS2_TORQUE_GRADIENT_SDO_INDEX                       0x6087 
#define  SANMOTION_RS2_TORQUE_GRADIENT_SDO_SUBINDEX                    0x00
#define  SANMOTION_RS2_GOAL_TORQUE_SDO_INDEX                           0x6071 
#define  SANMOTION_RS2_GOAL_TORQUE_SDO_SUBINDEX                        0x00
#define  SANMOTION_RS2_MAX_TORQUE_SDO_INDEX                            0x6072 
#define  SANMOTION_RS2_MAX_TORQUE_SDO_SUBINDEX                         0x00
#define  SANMOTION_RS2_REAL_TORQUE_SDO_INDEX                           0x6077 
#define  SANMOTION_RS2_REAL_TORQUE_SDO_SUBINDEX                        0x00

//编码器相关对象字典定义 P232
#define  SANMOTION_RS2_ENCODER_INNER_REAL_POS_SDO_INDEX                0x6063
#define  SANMOTION_RS2_ENCODER_INNER_REAL_POS_SDO_SUBINDEX             0x00
#define  SANMOTION_RS2_ENCODER_REAL_POS_SDO_INDEX                      0x6064
#define  SANMOTION_RS2_ENCODER_REAL_POS_SDO_SUBINDEX                   0x00
#define  SANMOTION_RS2_ENCODER_REAL_VELOCITY_SDO_INDEX                 0x606C
#define  SANMOTION_RS2_ENCODER_REAL_VELOCITY_SDO_SUBINDEX              0x00


//状态字符串，调试用
#define AL_STATE_STR(x) (x == EC_AL_STATE_INIT   ? "AL_STATE_INIT"     :\
					     x == EC_AL_STATE_PREOP  ? "AL_STATE_PREOP"    :\
					     x == EC_AL_STATE_SAFEOP ? "AL_STATE_SAFEOP"   :\
					     x == EC_AL_STATE_OP     ? "AL_STATE_OP"       :\
					     x == EC_AL_STATE_BOOT   ? "AL_STATE_BOOT"     : "AL_STATE_UNKNOWN")
					  
#define DOMAIN_STATE_STR(x) (x == EC_WC_ZERO       ? "DOMAIN_STATE_WC_ZERO"       : \
	                         x == EC_WC_INCOMPLETE ? "DOMAIN_STATE_WC_INCOMPLETE" : \
	                         x == EC_WC_COMPLETE   ? "DOMAIN_STATE_WC_COMPLETE"   : "DOMAIN_STATE_UNKNOWN")

//电机运行模式
#define SANMOTION_R_AD_RUN_MODEL_FREE 0x00
#define SANMOTION_R_AD_RUN_MODEL_DC 0x01

int test_torque(char *master_dev);


/******************************For Debug**************************************/
#define CMDID_INVALID                              -1
#define CMDID_SET_AXIS_ON_OFF                       1
#define CMDID_SET_AXIS_MAX_OUTPUT_TORQUE            2
#define CMDID_SET_AXIS_GOAL_OUTPUT_TORQUE           3
#define CMDID_GET_AXIS_REAL_OUTPUT_TORQUE           4
#define CMDID_GET_AXIS_ENCODER_INNER_REAL_POS       5
#define CMDID_GET_AXIS_ENCODER_REAL_POS             6
#define CMDID_GET_AXIS_ENCODER_VELOCITY             7
#define CMDID_QUIT_DEBUG                            0

typedef int (*cmd_callback)(int master_fd, unsigned short slave_position, int *arg);
typedef struct CmdNode{
	int cmdid;
	cmd_callback f;
}CmdNode;

int cmd_process(int cmdid, int master_fd, unsigned short slave_position, int *arg);
void debug_menu(void);
void debug_torque(char *master_dev);
/******************************For Debug**************************************/


#endif //MY_TEST_TORQUE_H


