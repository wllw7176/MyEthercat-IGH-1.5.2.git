#!/bin/bash
#dir_list="./test_torque ./test_torque2 ./test_torque_sanyo_ioctl ./test_torque_tec_ioctl ./test_torque_tec_lib ./test_torque_tec_ioctl"
dir_list="`ls -l | grep 'd[rwx-]' | grep test`"
for x in ${dir_list[@]}
do
	echo "cd $x/src/ && make clean && cd $OLDPWD"	
	cd $x/src/ && make clean && cd $OLDPWD	
done 
echo "Clean done"
