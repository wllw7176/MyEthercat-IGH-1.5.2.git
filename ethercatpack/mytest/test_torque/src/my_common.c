#include "my_common.h"

/*
	@description 
		Print domain info according by status domain
	@param 
		domain:Pointer to domain
	@return
		NULL
*/
void check_domain_state(ec_domain_t *domain)
{
	static ec_domain_state_t pre_ds;
	ec_domain_state_t ds;

	if (NULL == domain) {
		return;
	}
	
    ecrt_domain_state(domain, &ds);
	if (pre_ds.wc_state != ds.wc_state || pre_ds.working_counter != ds.working_counter || pre_ds.redundancy_active != ds.redundancy_active)
	{
		pre_ds = ds;
		LOG("---------Domain1 WC:%d State:%s ReundancyActive:%d---------\n", ds.working_counter, DOMAIN_STATE_STR(ds.wc_state), ds.redundancy_active);
	}
}


/*
	@description 
		Print status of master according by status of master
	@param 
		domain:Pointer to domain
	@return
		NULL
*/
void check_master_states(ec_master_t *master)
{
	static ec_al_state_t pre_al_st;
	static unsigned int pre_link_st;
	ec_master_state_t ms;

	if (NULL == master) {
		return;
	}
	
    ecrt_master_state(master, &ms);
	if (ms.al_states != pre_al_st)
	{
		pre_al_st = ms.al_states;
		LOG("MasterAlState:%s\n", AL_STATE_STR(ms.al_states));
	}
	if (ms.link_up != pre_link_st)
	{
		pre_link_st = ms.link_up;
		LOG("LinkStatus:%s\n", ms.link_up ? "UP" : "DOWN");
	}
}

/*
	@description 
		Print slave states according by status of slave configuration
	@param 
		domain:Pointer to domain
	@return
		NULL
*/
void check_slave_config_states(ec_slave_config_t *sc)
{
	static ec_al_state_t pre_al_st;
	ec_slave_config_state_t s;

	if (NULL == sc) {
		return;
	}
	
	ecrt_slave_config_state(sc, &s);
	if (s.al_state != pre_al_st)
	{
		LOG("SlaveAlState:%s\n", AL_STATE_STR(s.al_state));
		pre_al_st = s.al_state;
	}
}





