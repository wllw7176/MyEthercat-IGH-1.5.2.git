#ifndef MY_TEST_COMMON_H
#define MY_TEST_COMMON_H

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "ecrt.h"
#include "ioctl.h"


#define PRINTF_LEVEL 1
#if PRINTF_LEVEL == 0
#define LOG(...) printf("DEBUG=======>"__VA_ARGS__)
#define LOG_ERROR(err_msg) printf("DEBUG_ERROR=======> %s:%s:%d", __FILE__, __FUNCTION__, __LINE__);printf(" (err_msg:%s)\n", err_msg)
#elif  PRINTF_LEVEL == 1
#define LOG(...) printf(__VA_ARGS__)
#define LOG_ERROR(err_msg) printf("DEBUG_ERROR=======> %s:%s:%d", __FILE__, __FUNCTION__, __LINE__);printf(" (err_msg:%s)\n", err_msg)
#else
#define LOG(...) do {} while(0)
#define LOG_ERROR(err_msg) do {} while(0)
#endif


/*以下几个宏定义参照山羊电机手册P212*/
//从设备别名,位置信息，根据手册定义
#define SANMOTION_RS2_ALIAS 0
#define SANMOTION_RS2_POSITION 0
#define SANMOTION_RS2_VENDORID  0x000001B9
#define SANMOTION_RS2_PRODUCTID 0x00000002
//待访问和设置PDO对象宏定义
#define SANMOTION_RS2_VENDORID_SDO_INDEX 0x1018
#define SANMOTION_RS2_VENDORID_SDO_SUBINDEX 0x01
#define SANMOTION_RS2_PRODUCTID_SDO_INDEX 0x1018
#define SANMOTION_RS2_PRODUCTID_SDO_SUBINDEX 0x02
//AL 模式定义
#define EC_AL_STATE_BOOT 0x03
//控制字宏定义
#define  SANMOTION_RS2_CONTROL_WORD_SDO_INDEX                          0x6040 
#define  SANMOTION_RS2_CONTROL_WORD_SDO_SUBINDEX                       0x00
//配置力矩模式相关宏定义
#define  SANMOTION_RS2_MODEL_CONFIGURE_SDO_INDEX                       0x6060
#define  SANMOTION_RS2_MODEL_CONFIGURE_SDO_SUBINDEX                    0x00
#define  SANMOTION_RS2_TORQUE_PLAN_TYPE_SDO_INDEX                      0x6088
#define  SANMOTION_RS2_TORQUE_PLAN_TYPE_SDO_SUBINDEX                   0x00
#define  SANMOTION_RS2_TORQUE_GRADIENT_SDO_INDEX                       0x6087 
#define  SANMOTION_RS2_TORQUE_GRADIENT_SDO_SUBINDEX                    0x00
#define  SANMOTION_RS2_GOAL_TORQUE_SDO_INDEX                           0x6071 
#define  SANMOTION_RS2_GOAL_TORQUE_SDO_SUBINDEX                        0x00

#define  SANMOTION_RS2_CONTROL_MODEL_TORQUE                            0x04


//状态字符串，调试用
#define AL_STATE_STR(x) (x == EC_AL_STATE_INIT   ? "AL_STATE_INIT"     :\
					     x == EC_AL_STATE_PREOP  ? "AL_STATE_PREOP"    :\
					     x == EC_AL_STATE_SAFEOP ? "AL_STATE_SAFEOP"   :\
					     x == EC_AL_STATE_OP     ? "AL_STATE_OP"       :\
					     x == EC_AL_STATE_BOOT   ? "AL_STATE_BOOT"     : "AL_STATE_UNKNOWN")
					  
/*****************************************************
typedef enum {
    EC_WC_ZERO = 0,   //**< No registered process data were exchanged. 
    EC_WC_INCOMPLETE, //**< Some of the registered process data were
                      //  exchanged. 
    EC_WC_COMPLETE    //**< All registered process data were exchanged. 
} ec_wc_state_t;
*****************************************************/
#define DOMAIN_STATE_STR(x) (x == EC_WC_ZERO       ? "DOMAIN_STATE_WC_ZERO"       : \
	                         x == EC_WC_INCOMPLETE ? "DOMAIN_STATE_WC_INCOMPLETE" : \
	                         x == EC_WC_COMPLETE   ? "DOMAIN_STATE_WC_COMPLETE"   : "DOMAIN_STATE_UNKNOWN")



//电机运行模式
#define SANMOTION_R_AD_RUN_MODEL_FREE 0x00
#define SANMOTION_R_AD_RUN_MODEL_DC 0x01

/*通用函数*/

//状态检测函数,测试用
void check_domain_state(ec_domain_t *domain);
void check_master_states(ec_master_t *master);
void check_slave_config_states(ec_slave_config_t *sc);

#endif //MY_TEST_COMMON_H

