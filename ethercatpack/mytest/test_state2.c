#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "ecrt.h"
#include "../lib/ioctl.h"


/****************************************************************************/
#define EC_AL_STATE_BOOT 0x03
// Application parameters
#define FREQUENCY 1000
#define CLOCK_TO_USE CLOCK_REALTIME
#define MEASURE_TIMING

#define NSEC_PER_SEC (1000000000L)
#define PERIOD_NS (NSEC_PER_SEC / FREQUENCY)

//从设备别名,位置信息，根据手册定义
#define SANMOTION_R_AD_M0011098G_ALIAS 0
#define SANMOTION_R_AD_M0011098G_POSITION 0
#define SANMOTION_R_AD_M0011098G_VENDOR_ID  0x000001B9
#define SANMOTION_R_AD_M0011098G_PRODUCT_ID 0x00000002
#define STATE_STR(x) (x == EC_AL_STATE_INIT ? "STATE_INIT" :\
					  x == EC_AL_STATE_PREOP ? "STATE_PREOP" :\
					  x == EC_AL_STATE_SAFEOP ? "STATE_SAFEOP" :\
					  x == EC_AL_STATE_OP ? "STATE_OP":\
					  x == EC_AL_STATE_BOOT ? "STATE_BOOT":\
					  "UNKNOWN_STATE")
/****************************************************************************/

ec_master_t *master = NULL;
ec_slave_config_t *sc = NULL;
int master_fd;
ec_ioctl_module_t module_data;
ec_ioctl_slave_state_t st_data;
ec_ioctl_slave_t slave_data;
ec_ioctl_config_t config_data;

int main(int argc, char *argv[])
{
	int err;

	if (argc !=2)
	{
		printf("Usage %s devname\n", argv[0]);
		return -1;
	}
	
	master_fd = open(argv[1], O_RDWR);
	if (master_fd < 0)
	{
		printf("Open file %s error \n", argv[1]);
		return -1;
	}	

	
	/*
	master = ecrt_request_master(0);
	if (!master)
	{
		printf("Request master 0 failed!\n");
		return -1;
	}
	else
	{
		printf("Request master 0 Ok!\n");
	}

	sc = ecrt_master_slave_config(master, SANMOTION_R_AD_M0011098G_ALIAS, SANMOTION_R_AD_M0011098G_POSITION, SANMOTION_R_AD_M0011098G_VENDOR_ID, SANMOTION_R_AD_M0011098G_PRODUCT_ID);
	if (!sc)
	{
		printf("Request slave configure failed!\n");
		return -1;
	}
	else
	{
		printf("Request slave configure Ok!\n");
	}
	
	ecrt_slave_config_dc(sc, 0x0700, PERIOD_NS, 4400000, 0, 0);
	
	printf("Activating master...\n");
	if (ecrt_master_activate(master))
	{
		printf("Activating master failed!\n");
		return -1;
	}
	else
	{
		printf("Activating master Ok!\n");
	}
	*/
	
	/****************************StateTest****************************************/
	//Get info of module and slave

	err = ioctl(master_fd, EC_IOCTL_MASTER, &module_data);
	if (err)
	{
		printf("Ioctl %s error,errcode:%d\n", "EC_IOCTL_MASTER", err);
		return -1;
	}
	else
	{
		printf("IoctlMagicNum:0X%08X MasterCount:%d\n", module_data.ioctl_version_magic, module_data.master_count);
	}

	err = ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
	if (err)
	{
		printf("ioctl %s error,errcode:%d\n", "EC_IOCTL_SLAVE", err);
		return -1;
	}
	else
	{
		printf("SlaveInfo:\n\tVendorId:0x%08X ProudctId:0x%08X \n\tAlias:%d Posotion:%d\n", slave_data.vendor_id, slave_data.product_code, slave_data.alias, slave_data.position);
	}


	/*configure DC_SYNC0*/
	//config_data.config_index = 0;
	//config_data.dc_assign_activate = 0x300;
	//sync0
	//config_data.dc_sync[0].cycle_time = 0;
	//config_data.dc_sync[0].shift_time = 0;
	//sync1
	//config_data.dc_sync[1].cycle_time = 0;
	//config_data.dc_sync[1].shift_time = 0;
	
	//err = ioctl(master_fd, EC_IOCTL_SC_DC, &config_data);
	//if (err)
	//{
	//    printf("Configure DC SYNC0 error,errcode:%d\n", err);
	//   return -1;
	//}

	/*configure DC_SYNC1*/
	//config_data.config_index = 1;
	//config_data.dc_assign_activate = 0x700;
	//sync0
	//config_data.dc_sync[0].cycle_time = 300 * 1000 *1000; 
	//config_data.dc_sync[0].shift_time = 0;
	//sync1
	//config_data.dc_sync[1].cycle_time = 0;
	//config_data.dc_sync[1].shift_time = 0;
	//err = ioctl(master_fd, EC_IOCTL_SC_DC, &config_data);
	//if (err)
	//{
	//   printf("Configure DC SYNC1 error,errcode:%d\n", err);
	//   return -1;
	//}
	
	
	st_data.slave_position = slave_data.position;
	
	st_data.al_state = EC_AL_STATE_INIT;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   printf("Set state %s error,errcode:%d\n", "EC_AL_STATE_INIT", err);
	   return -1;
	}
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}

	st_data.al_state = EC_AL_STATE_PREOP;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   printf("Set state %s error,errcode:%d\n", "EC_AL_STATE_PREOP", err);
	   return -1;
	}
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}
	
	st_data.al_state = EC_AL_STATE_BOOT;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   printf("Set state %s error,errcode:%d\n", "EC_AL_STATE_PREOP", err);
	   return -1;
	}
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}

	st_data.al_state = EC_AL_STATE_SAFEOP;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   printf("Set state %s error, errcode:%d\n", "EC_AL_STATE_SAFEOP", err);
	   return -1;
	}
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}

	st_data.al_state = EC_AL_STATE_OP;
	err = ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &st_data);
	if (err)
	{
	   
	   printf("Set state %s error, errcode:%d\n", "EC_AL_STATE_OP", err);
	   return -1;
    }
	else
	{
		usleep(300 * 1000);
		ioctl(master_fd, EC_IOCTL_SLAVE, &slave_data);
		printf("Slave State:%s\n", STATE_STR(slave_data.al_state));
	}
	
	
	return 0;
	

}


