#include "test_torque.h"
#include "tec_servo.h"

/*********************************** 
 * PDO 映射信息,通过ethercat cstruct
 * Master 0, Slave 0, "AEM-090-30"
 * Vendor ID:       0x000000ab
 * Product code:    0x00001030
 * Revision number: 0x00010003
 **********************************/

ec_pdo_entry_info_t slave_0_pdo_entries[] = {
    {0x6040, 0x00, 16},
    {0x607a, 0x00, 32},
    {0x60b1, 0x00, 32},
    {0x60b2, 0x00, 16},
    {0x6041, 0x00, 16},
    {0x6064, 0x00, 32},
    {0x60f4, 0x00, 32},
    {0x606c, 0x00, 32},
    {0x6077, 0x00, 16},
};

ec_pdo_info_t slave_0_pdos[] = {
    {0x1700, 4, slave_0_pdo_entries + 0},
    {0x1b00, 5, slave_0_pdo_entries + 4},
};

ec_sync_info_t slave_0_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, slave_0_pdos + 0, EC_WD_ENABLE},
    {3, EC_DIR_INPUT, 1, slave_0_pdos + 1, EC_WD_DISABLE},
    {0xff}
};

//使用ethercat库函数时用到的变量定义 非直接ioctl模式
static ec_master_t *master = NULL;
static ec_master_state_t master_state = {};
static ec_domain_t *domain1 = NULL;
static ec_domain_state_t domain1_state = {};
static ec_slave_config_t *sc_ana_in = NULL;
static ec_slave_config_state_t sc_ana_in_state = {};


// process data
ec_ioctl_slave_sdo_download_t sdo_download_data;
ec_ioctl_slave_sdo_download_t sdo_upload_data;
int master_fd = -1;
int slave_alias = TEC_SERVO_AXIS0_ALIAS;
int slave_pos = TEC_SERVO_AXIS0_POSITION;

/*
	@description
		Download data into slaves
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want download 
		
	@return
		0:
			download success 
		-1:
			dowolad failed
*/
static int sdo_download(int fd, ec_ioctl_slave_sdo_download_t *data)
{
	int i, err;
	err = ioctl(fd, EC_IOCTL_SLAVE_SDO_DOWNLOAD, data);
	
	if (err) 
	{
#if (DEBUG_MODEL == 1)
		LOG("sdo_download index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" failed,ErrMsg:%s\n", strerror(errno));
#endif //#if DEBUG_MODEL
		return -1;
    }
	else
	{
#if (DEBUG_MODEL == 1)
		struct timespec start;
		struct timespec stop;
		long diff_time_ns;
		
		clock_gettime(CLOCK_REALTIME, &start);
		ioctl(fd, EC_IOCTL_SLAVE_SDO_DOWNLOAD, data);
		clock_gettime(CLOCK_REALTIME, &stop);
		diff_time_ns = (stop.tv_sec - start.tv_sec) * 1000000000 + (stop.tv_nsec - start.tv_nsec);
		
		LOG("sdo_download index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" success\n");
		
		LOG("One time sdo download spend %ld ns\n", diff_time_ns);
#endif //#if DEBUG_MODEL
		return 0;
	}
}


/*
	@description
		Upload data into slaves
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want to store data of upload 
		
	@return
		0:
			upload success 
		-1:
			upload failed
*/
static int sdo_upload(int fd, ec_ioctl_slave_sdo_download_t *data)
{
	int i, err;
	err = ioctl(fd, EC_IOCTL_SLAVE_SDO_UPLOAD, data);
	if (err) 
	{
#if (DEBUG_MODEL == 1)
		LOG("SdoUpload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" failed,ErrMsg:%s\n", strerror(errno));
#endif //#if DEBUG_MODEL
		return -1;
    }
	else
	{
#if (DEBUG_MODEL == 1)
		struct timespec start;
		struct timespec stop;
		long diff_time_ns;
		
		clock_gettime(CLOCK_REALTIME, &start);
		ioctl(fd, EC_IOCTL_SLAVE_SDO_UPLOAD, data);
		clock_gettime(CLOCK_REALTIME, &stop);
		diff_time_ns = (stop.tv_sec - start.tv_sec) * 1000000000 + (stop.tv_nsec - start.tv_nsec);
		
		LOG("SdoUpload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" success\n");
		LOG("One time sdo upload spend %ld ns\n", diff_time_ns);
#endif //#if DEBUG_MODEL
		return 0;
	}
}

/*
	@description
		Set slave states
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want to store data of set 
		
	@return
		0:
			Set state success 
		-1:
			Set state failed
*/
static int set_slave_state(int master_fd, unsigned short slave_position, unsigned char al_state)
{
	
	ec_ioctl_slave_state_t data;

	data.al_state = slave_position;
	data.slave_position = slave_position;
	
	return ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &data);

}

/*
	@description
		Main loop, we obtain real torque info or other info

	@param
		None
		
	@return
		None
*/
static void cycle_task(void)
{
	static short torque_data;
	static int inner_pos;
	static int pos;
	static int vel;
	static int err;

#if (DEBUG_MODEL == 1)
	struct timespec start;
	struct timespec stop;
	long diff_time_ms;
#endif
	
	while (1)
	{
#if (DEBUG_MODEL == 1)
	clock_gettime(CLOCK_REALTIME, &start);
	diff_time_ms = (stop.tv_sec - start.tv_sec) * 1000 + (stop.tv_nsec - start.tv_nsec) / 1000000;
	LOG("One cycle spend %ldms\n", diff_time_ms);
	usleep(1000 * 50);
#else
	usleep(1000 * 50);
#endif
		// receive process data
		ioctl(master_fd, EC_IOCTL_RECEIVE, NULL);
		ioctl(master_fd, EC_IOCTL_DOMAIN_PROCESS, slave_pos);

		sdo_upload_data.slave_position = slave_pos;slave_pos;
		sdo_upload_data.sdo_index = TEC_SRVO_GOAL_TORQUE_SDO_INDEX;
		sdo_upload_data.sdo_entry_subindex = TEC_SRVO_GOAL_TORQUE_SDO_SUBINDEX;
		sdo_upload_data.data = (unsigned char*)&torque_data;
		sdo_upload_data.data_size = sizeof(torque_data);
		err = sdo_upload(master_fd, &sdo_upload_data);
		if (err) 
		{
			LOG("Get axis:%d real_torque failed!\n", sdo_upload_data.slave_position);
			continue;
		}
		torque_data = (*(short *)(sdo_upload_data.data));
		
		sdo_upload_data.sdo_index = TEC_SRVO_ENCODER_INNER_REAL_POS_SDO_INDEX;
		sdo_upload_data.sdo_entry_subindex = TEC_SRVO_ENCODER_INNER_REAL_POS_SDO_SUBINDEX;
		sdo_upload_data.data = (unsigned char*)&inner_pos;
		sdo_upload_data.data_size = sizeof(inner_pos);
		if (err) 
		{
			LOG("Get axis:%d inner_pos failed!\n", sdo_upload_data.slave_position);
			continue;
		}
		inner_pos= (*(int *)(sdo_upload_data.data));

		sdo_upload_data.sdo_index = TEC_SRVO_ENCODER_REAL_POS_SDO_INDEX;
		sdo_upload_data.sdo_entry_subindex = TEC_SRVO_ENCODER_REAL_POS_SDO_SUBINDEX;
		sdo_upload_data.data = (unsigned char*)&pos;
		sdo_upload_data.data_size = sizeof(pos);
		err = sdo_upload(master_fd, &sdo_upload_data);
		if (err) 
		{
			LOG("Get axis:%d pos failed!\n", sdo_upload_data.slave_position);
			continue;
		}
		pos= (*(int *)(sdo_upload_data.data));

		sdo_upload_data.sdo_index = TEC_SRVO_ENCODER_REAL_VELOCITY_SDO_INDEX;
		sdo_upload_data.sdo_entry_subindex = TEC_SRVO_ENCODER_REAL_VELOCITY_SDO_SUBINDEX;
		sdo_upload_data.data = (unsigned char*)&vel;
		sdo_upload_data.data_size = sizeof(vel);
		err = sdo_upload(master_fd, &sdo_upload_data);
		if (err) 
		{
			LOG("Get axis:%d velocity failed!\n", sdo_upload_data.slave_position);
			continue;
		}
		vel= (*(int *)(sdo_upload_data.data));
		
		if (!err)
		{
			LOG("Get axis:%d torque:%d inner_pos:%d pos:%d velocity:%d\n", sdo_upload_data.slave_position, torque_data, inner_pos, pos, vel);
			continue;
		}

		// send process data
		ioctl(master_fd, EC_IOCTL_SEND, NULL);
		ioctl(master_fd, EC_IOCTL_DOMAIN_QUEUE, slave_pos);
#if (DEBUG_MODEL == 1)
	clock_gettime(CLOCK_REALTIME, &stop);
#endif
	}
	
}

/*
	@description
		Signal hander

	@param
		Signal num
		
	@return
		None
*/
static void signal_handler(int signum) 
{
	static long int count = 0;
	static char dir_flag = 1;
	static int err = 0;
	static short torque_data;
	switch (signum) 
	{
		case SIGALRM:
			count++;
			if (count % 20 == 0) //装换方向
			{
				//LOG("signal_handler count=%d\n", count);
				
				if (1 == dir_flag) 
				{
					dir_flag = 0;
					torque_data = 28;
					sdo_download_data.slave_position = slave_pos;
					sdo_download_data.sdo_index = TEC_SRVO_GOAL_TORQUE_SDO_INDEX;
					sdo_download_data.sdo_entry_subindex = TEC_SRVO_GOAL_TORQUE_SDO_SUBINDEX;
					sdo_download_data.data = (unsigned char *)&torque_data;
					sdo_download_data.data_size = sizeof(torque_data);
					err = sdo_download(master_fd, &sdo_download_data);
					if (err) 
					{
						LOG("Set goal_torque:%d failed!\n", torque_data);
					}
					else 
					{
						LOG("Set goal_torque:%d success!\n", torque_data);
					}
				}
				else
				{
					dir_flag = 1;
					torque_data = -28;
					sdo_download_data.slave_position = slave_pos;
					sdo_download_data.sdo_index = TEC_SRVO_GOAL_TORQUE_SDO_INDEX;
					sdo_download_data.sdo_entry_subindex = TEC_SRVO_GOAL_TORQUE_SDO_SUBINDEX;
					sdo_download_data.data = (unsigned char *)&torque_data;&torque_data;
					sdo_download_data.data_size = sizeof(torque_data);
					err = sdo_download(master_fd, &sdo_download_data);
					if (err) 
					{
						LOG("Set goal_torque:%d failed!\n", torque_data);
					}
					else 
					{
						LOG("Set goal_torque:%d success!\n", torque_data);
					}
				}
				
				
			}
			break;
		case SIGSEGV:
		case SIGABRT:
			if ( -1 != master_fd)
			{
				close(master_fd);
			}
			break;
		default:
			return;
			
    }
}

/*
	@description
		Test torque model

	@param
		Ethercat Master device /dev/EtherCATx
		
	@return
		0 :
			Test successfuly
		-1:
			Test failed
*/

int test_torque(char *master_dev)
{
	ec_slave_config_t *sc;
    struct sigaction sa;
    struct itimerval tv;
	unsigned char run_mode;
	char contorl_mode;
	unsigned char slave_state;
	unsigned short download_data;
	short torque_data; 
	int torque_slope;
	unsigned char recv_buff[4];
	int err;
	uint8_t state = 0x00;
	
	/********************************************************************/
	if (NULL == master_dev)
	{
		fprintf(stderr, "Null mater_dev pointer!\n");
		return -1;
	}
	
	master_fd = open(master_dev, O_RDWR);
	if (master_fd < 0)
	{
		LOG("Open file %s error! \n", master_dev);
		return -1;
	}
	
	/********************************************************************/

	//设置'OP'状态
	/*
	state = EC_AL_STATE_OP;
	err = set_slave_state(master_fd, slave_pos, EC_AL_STATE_OP);
	if (err)  
	{
		
		LOG("Set 'OP' state failed!\n");
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set 'OP' state success!\n");
	}
	*/

	
	/*配置驱动器进入力矩控制模式*/
	//设置驱动器成转矩模式
	contorl_mode = TEC_SRVO_CONTROL_MODEL_TORQUE;
	sdo_download_data.slave_position = slave_pos;
	sdo_download_data.sdo_index = TEC_SRVO_MODEL_CONFIGURE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SRVO_MODEL_CONFIGURE_SDO_SUBINDEX;
	sdo_download_data.data = &contorl_mode;
	sdo_download_data.data_size = sizeof(contorl_mode);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set torque control model failed!\n");
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set torque control model success!\n");
	}
	
	//设置转矩(推力)规划类型 先用默认值  不设置
	//设置 转矩(推力)斜率 先用默认值 不设置
	torque_slope = 1000;
	sdo_download_data.slave_position = slave_pos;
	sdo_download_data.sdo_index = TEC_SRVO_TORQUE_SLOPE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SRVO_TORQUE_SLOPE_SDO_SUBINDEX;
	sdo_download_data.data = &torque_slope;
	sdo_download_data.data_size = sizeof(torque_slope);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set torque slope failed!\n");
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set torque slope:%d success!\n", torque_slope);
	}

	//设置最大目标转矩
	//实际输出转矩=set_value * 0.1% * 额定输出转矩
	//此处为了测试安全 将输出设置最大转矩10%
	torque_data= 100;
	sdo_download_data.slave_position = slave_pos;
	sdo_download_data.sdo_index = TEC_SRVO_MAX_TORQUE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SRVO_MAX_TORQUE_SDO_SUBINDEX;
	sdo_download_data.data = (unsigned char *)&torque_data;
	sdo_download_data.data_size = sizeof(torque_data);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set max_torque:%d failed!\n", torque_data);
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set max_torque:%d success!\n", torque_data);
	}
	
	//设置目标转矩(推力)
	torque_data = 100;
	sdo_download_data.slave_position = slave_pos;
	sdo_download_data.sdo_index = TEC_SRVO_GOAL_TORQUE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SRVO_GOAL_TORQUE_SDO_SUBINDEX;
	sdo_download_data.data = (unsigned char *)&torque_data;
	sdo_download_data.data_size = sizeof(torque_data);
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set goal_torque:%d failed!\n", torque_data);
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set goal_torque:%d success!\n", torque_data);
	}
	
	//设置 驱动器伺服ON
	sdo_download_data.slave_position = slave_pos;
	sdo_download_data.sdo_index = TEC_SRVO_CONTROL_WORD_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SRVO_CONTROL_WORD_SDO_SUBINDEX;
	sdo_download_data.data = &download_data;
	sdo_download_data.data_size = sizeof(download_data);
	download_data = 0x06;
	sdo_download(master_fd, &sdo_download_data);
	download_data = 0x0f;
	err = sdo_download(master_fd, &sdo_download_data);
	if (err) 
	{
		LOG("Set server on faild!\n");
		close(master_fd);
		return err;
	}
	else 
	{
		LOG("Set server on success!\n");
	}
	
	
	/********************************************************************/
    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGTERM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGSEGV, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
    LOG("Starting timer...\n");
	LOG("**************Start test************\n");
    tv.it_interval.tv_sec = 1;
    tv.it_interval.tv_usec = 0; //100ms
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 1000;
    if (setitimer(ITIMER_REAL, &tv, NULL)) {
        fprintf(stderr, "Failed to start timer: %s\n", strerror(errno));
        return 1;
    }
	
	/********************************************************************/
	// 开始主循环
	cycle_task();
	
    /********************************************************************/
	close(master_fd);
	
    return 0;
}


/******************************For Debug**************************************/
static int set_axis_on_off(int master_fd, unsigned short slave_position, int *arg_val)
{

	unsigned short download_data;
	int err;
	
	sdo_download_data.slave_position = slave_position;
	sdo_download_data.sdo_index = TEC_SRVO_CONTROL_WORD_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SRVO_CONTROL_WORD_SDO_SUBINDEX;
	sdo_download_data.data = &download_data;
	sdo_download_data.data_size = sizeof(download_data);
	
	if (*arg_val == 1)
	{
		//设置 驱动器伺服ON
		download_data = 0x06;
		err = sdo_download(master_fd, &sdo_download_data);
		if (err)
		{
			return err;
		}
		download_data = 0x07;
		err = sdo_download(master_fd, &sdo_download_data);
		if (err)
		{
			return err;
		}
		download_data = 0x0f;
		err = sdo_download(master_fd, &sdo_download_data);
		return err;
	}
	else
	{
		//设置 驱动器伺服OFF
		download_data = 0x0f;
		err = sdo_download(master_fd, &sdo_download_data);
		if (err)
		{
			return err;
		}
		download_data = 0x06;
		err = sdo_download(master_fd, &sdo_download_data);
		return err;
	}
	
}

static int set_axix_max_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	short torque_data;
	torque_data= *arg_val;
	int err;
	sdo_download_data.slave_position = slave_position;
	sdo_download_data.sdo_index = TEC_SRVO_MAX_TORQUE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SRVO_MAX_TORQUE_SDO_SUBINDEX;
	sdo_download_data.data = (unsigned char *)&torque_data;
	sdo_download_data.data_size = sizeof(torque_data);
	err = sdo_download(master_fd, &sdo_download_data);
	return err;
}

static int set_axix_goal_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	short torque_data = *arg_val;
	int err;
	sdo_download_data.slave_position = slave_position;
	sdo_download_data.sdo_index = TEC_SRVO_GOAL_TORQUE_SDO_INDEX;
	sdo_download_data.sdo_entry_subindex = TEC_SRVO_GOAL_TORQUE_SDO_SUBINDEX;
	sdo_download_data.data = (unsigned char *)&torque_data;
	sdo_download_data.data_size = sizeof(torque_data);
	err = sdo_download(master_fd, &sdo_download_data);
	return err;
}

static int get_axix_real_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	int err;
	sdo_upload_data.slave_position = slave_position;
	sdo_upload_data.sdo_index = TEC_SRVO_GOAL_TORQUE_SDO_INDEX;
	sdo_upload_data.sdo_entry_subindex = TEC_SRVO_GOAL_TORQUE_SDO_SUBINDEX;
	sdo_upload_data.data = (unsigned char*)arg_val;
	sdo_upload_data.data_size = sizeof(int);
	err = sdo_upload(master_fd, &sdo_upload_data);
	*arg_val = (*(int *)(sdo_upload_data.data));
	return err;
		
}

static int get_axix_encoder_inner_real_pos(int master_fd, unsigned short slave_position, int *arg_val)
{

	int err;
	sdo_upload_data.slave_position = slave_position;
	sdo_upload_data.sdo_index = TEC_SRVO_ENCODER_INNER_REAL_POS_SDO_INDEX;
	sdo_upload_data.sdo_entry_subindex = TEC_SRVO_ENCODER_INNER_REAL_POS_SDO_SUBINDEX;
	sdo_upload_data.data = (unsigned char*)arg_val;
	sdo_upload_data.data_size = sizeof(int);
	err = sdo_upload(master_fd, &sdo_upload_data);
	*arg_val = (*(int *)(sdo_upload_data.data));
	return err;
		
}

static int get_axix_encoder_real_pos(int master_fd, unsigned short slave_position, int *arg_val)
{

	int err;
	sdo_upload_data.slave_position = slave_position;
	sdo_upload_data.sdo_index = TEC_SRVO_ENCODER_REAL_POS_SDO_INDEX;
	sdo_upload_data.sdo_entry_subindex = TEC_SRVO_ENCODER_REAL_POS_SDO_SUBINDEX;
	sdo_upload_data.data = (unsigned char*)arg_val;
	sdo_upload_data.data_size = sizeof(int);
	err = sdo_upload(master_fd, &sdo_upload_data);
	*arg_val = (*(int *)(sdo_upload_data.data));
	return err;
		
}

static int get_axix_encoder_velocity(int master_fd, unsigned short slave_position, int *arg_val)
{

	int err;
	
	sdo_upload_data.slave_position = slave_position;
	sdo_upload_data.sdo_index = TEC_SRVO_ENCODER_REAL_VELOCITY_SDO_INDEX;
	sdo_upload_data.sdo_entry_subindex = TEC_SRVO_ENCODER_REAL_VELOCITY_SDO_SUBINDEX;
	sdo_upload_data.data = (unsigned char*)arg_val;
	sdo_upload_data.data_size = sizeof(int);
	err = sdo_upload(master_fd, &sdo_upload_data);
	*arg_val = (*(int *)(sdo_upload_data.data));
	return err;
		
}



static int quit_debug(int master_fd, unsigned short slave_position, int *arg_val)
{
	exit(0);
	return 0;
}

CmdNode cmd_list[] = {
		{CMDID_SET_AXIS_ON_OFF, set_axis_on_off},
		{CMDID_SET_AXIS_MAX_OUTPUT_TORQUE, set_axix_max_output_torque},
		{CMDID_SET_AXIS_GOAL_OUTPUT_TORQUE, set_axix_goal_output_torque},
		{CMDID_GET_AXIS_REAL_OUTPUT_TORQUE, get_axix_real_output_torque},
		{CMDID_GET_AXIS_ENCODER_INNER_REAL_POS, get_axix_encoder_inner_real_pos},
		{CMDID_GET_AXIS_ENCODER_REAL_POS, get_axix_encoder_real_pos},
		{CMDID_GET_AXIS_ENCODER_VELOCITY, get_axix_encoder_velocity},
		{CMDID_QUIT_DEBUG, quit_debug},
		{CMDID_INVALID, NULL}
};
#define CMD_LIST_SIZE (sizeof(cmd_list) / sizeof(cmd_list[0]))

int cmd_process(int cmdid, int master_fd, unsigned short slave_position, int *arg)
{
	int i;
	for (i = 0; i<CMD_LIST_SIZE; i++) 
	{
		if (cmdid == cmd_list[i].cmdid)
		{
			return cmd_list[i].f(master_fd, slave_position, arg);
		}
	}
	if (i == CMD_LIST_SIZE)
	{
		return -1;
	}
}

void debug_menu(void)
{

	printf("****************Ethercat Servo Torque Debug****************\n");
	printf("1 Set axis servo on/off                     (on:cmdid axis_num 1 off:cmdid axi_num 0)\n");
	printf("2 Set axis max output_torque                (cmdid axis_num max_output_torque)\n");
	printf("3 Set axis goal torque                      (cmdid axis_num goal_torque)\n");
	printf("4 Get axis real outpue torque               (cmdid axis_num 0)\n");
	printf("5 Get axis encoder inner real pos           (cmdid 0 0)\n");
	printf("6 Get axis encoder real pos                 (cmdid 0 0)\n");
	printf("7 Get axis encoder velocity                 (cmdid 0 0)\n");
	printf("0 Quit debug system                         (cmdid 0 0)\n");
	printf("Input cmd:");
	
}

void debug_torque(char *master_dev)
{

	unsigned short slave_positon = slave_pos;
	int arg_val;
	int cmdid;
	
	if (NULL == master_dev)
	{
		LOG("Null mater_dev pointer!\n");
		return;
	}
	master_fd = open(master_dev, O_RDWR);
	if (master_fd < 0)
	{
		LOG("Open file %s error! \n", master_dev);
		return;
	}

	//为了测试安全 先设置一个小点的最大输出扭矩
	cmdid = CMDID_SET_AXIS_MAX_OUTPUT_TORQUE;
	arg_val = 100;
	cmd_process(cmdid, master_fd, slave_positon, &arg_val);
	close(master_fd);

	/*使用Ethercat库接口测试*/
    master = ecrt_request_master(0);
    if (NULL != master)
    {
		printf("Request master(0) success!\n");
	}
    else
   	{
		printf("Request master(0) failed!\n");
		return -1;
	}
    //请求域, 解释见IgH ethercatmaster手册
    domain1 = ecrt_master_create_domain(master);
    if (NULL != domain1)
    {
		printf("Master create domain success!\n");
	}
	else
	{
		printf("Master create domain failed!\n");
		return -1;
	}
    //获取从机配置
    if (!(sc_in = ecrt_master_slave_config(
                    master, TEC_SERVO_AXIS0_ALIAS, TEC_SERVO_AXIS0_POSITION, SANMOTION_RS2_VENDORID, SANMOTION_RS2_PRODUCTID))) 
    {
        fprintf(stderr, "Get slave configuration failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Get slave configuration success!\n");
	}
	//从机PDO配置
	//ecrt_slave_config_pdos与ecrt_master_slave_config成对出现校验从机是否配置成功
    printf("Configuring PDOs...\n");
    if (ecrt_slave_config_pdos(sc_in, EC_END, sanmotion_r_ad_m0011098g_syncs)) 
	{
        fprintf(stderr, "Configure PDOs failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Configure PDOs success!\n");
	}
	
    if (!(sc = ecrt_master_slave_config(master, SANMOTION_RS2_ALIAS, SANMOTION_RS2_POSITION, SANMOTION_RS2_VENDORID, SANMOTION_RS2_PRODUCTID))) 
    {
        fprintf(stderr, "Get slave configuration failed after configure pdos !\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Get slave configuration success after configure pdos !\n");
	}
	//从机pdos注册
    if (ecrt_slave_config_pdos(sc, EC_END, sanmotion_r_ad_m0011098g_syncs)) 
	{
        fprintf(stderr, "Slave configure PDOs failed!ErrMsg:%s\n", strerror(errno));
        return -1;
    }
	else
	{
		fprintf(stderr, "Slave configure PDOs configure success!\n");
	}
	//域注册
	if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs))
	{
        fprintf(stderr, "Reg PDO entry registration failed!ErrMsg:%s\n", strerror(errno));
        return -1;
    }
	else
	{
		fprintf(stderr, "Reg PDO entry registration success!\n");
	}
	//激活主机
    printf("Activating master...\n");
    if (ecrt_master_activate(master))
    {
		fprintf(stderr, "Activate master failed!\n");
		return -1;
	}
	else
	{
		fprintf(stderr, "Activate master success!\n");
	}
	//process data in domain1
    if (!(domain1_pd = ecrt_domain_data(domain1))) 
	{
		fprintf(stderr, "ecrt_domain_data(domain1) failed!\n");
		return -1;
    }
	else
	{
		fprintf(stderr, "ecrt_domain_data(domain1) success!\n");
	}
	
	while (1)
	{
		debug_menu();
		if (scanf("%d %d %d", &cmdid, &slave_positon, &arg_val) != 3)
		{
			LOG("Error input cmd!!!\n");			
			getchar();
			continue;
		}
		if (slave_pos != slave_positon)
		{
			LOG("Waring now only supported slave_positon=%d\n", slave_pos);
			continue;
		}
		if (
			cmdid == CMDID_GET_AXIS_REAL_OUTPUT_TORQUE     ||
			cmdid == CMDID_GET_AXIS_ENCODER_INNER_REAL_POS || 
			cmdid == CMDID_GET_AXIS_ENCODER_REAL_POS       || 
			cmdid == CMDID_GET_AXIS_ENCODER_VELOCITY
		   )
		{
			if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
			{
				LOG("Execute cmd success!\n");
				LOG("Get Axis:%d Value:%d\n", slave_positon, arg_val);
				continue;
			}
			
		}
		if (cmdid == CMDID_SET_AXIS_MAX_OUTPUT_TORQUE )
		{
			if (arg_val >= MAX_OUTPUT_TORQUE)
			{
				arg_val = MAX_OUTPUT_TORQUE;
				LOG("Warning max output torque limit(0~%d)!\n", MAX_OUTPUT_TORQUE);
				
			}
			if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
			{
				LOG("Execute cmd success!\n");
				continue;
			}
		}
		if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
		{
			LOG("Execute cmd success!\n");
			continue;
		}
		
	}
	
	
}
/******************************For Debug**************************************/




