#include "test_torque.h"
#include "tec_servo_tec.h"

/*********************************** 
 * PDO 映射信息,通过ethercat cstruct
 * Master 0, Slave 0, "AEM-090-30"
 * Vendor ID:       0x000000ab
 * Product code:    0x00001030
 * Revision number: 0x00010003
 **********************************/

//保存控制字与状态字在domain中的偏移位置 读写PDO入口要用到
unsigned int off_tec_servo_contorl_word_bit_position;
unsigned int off_tec_servo_status_word_bit_position;

/*
	这里定义我们要操作PDO对象的信息,我们可以定义多个PDO映射,但是可以只使用其中的几个
*/
const static ec_pdo_entry_reg_t slave_axis0_domain_regs[] = {
    {
		TEC_SERVO_AXIS0_ALIAS,               //别名
		TEC_SERVO_AXIS0_POSITION,            //位置
		TEC_SERVO_VENDOR_ID,                 //厂商id
		TEC_SERVO_PRODUCT_ID,                //产品id
		TEC_SERVO_CONTROL_WORD_SDO_INDEX,    //索引
		TEC_SERVO_CONTROL_WORD_SDO_SUBINDEX, //子索引 
		&off_tec_servo_contorl_word_bit_position       //PDO入口在process_data字节偏移量
	},
    {
    	TEC_SERVO_AXIS0_ALIAS,               //别名
		TEC_SERVO_AXIS0_POSITION,            //位置
		TEC_SERVO_VENDOR_ID,                 //厂商id
		TEC_SERVO_PRODUCT_ID,                //产品id
		TEC_SERVO_STATUS_WORD_SDO_INDEX,    //索引
		TEC_SERVO_STATUS_WORD_SDO_SUBINDEX, //子索引 
		&off_tec_servo_status_word_bit_position       //PDO入口在process_data字节偏移量
	},
    {
    }
}; 

/*
  要进行 PDO 映射对象字典索引及大小
 */
ec_pdo_entry_info_t slave_axis0_pdo_entries[] = {
    {0x6040, 0x00, 16},
    {0x607a, 0x00, 32},
    {0x60b1, 0x00, 32},
    {0x60b2, 0x00, 16},
    {0x6041, 0x00, 16},
    {0x6064, 0x00, 32},
    {0x60f4, 0x00, 32},
    {0x606c, 0x00, 32},
    {0x6077, 0x00, 16},
};

/*
	RECEIVE PDO MAPPING PARAMETERS  INDEX 0X1700
	TRANSMIT PDO MAPPING PARAMETERS  INDEX 0X1B00
	说明:
		PDO映射发送与接收是相对slave来说
		进行RECEIVE PDO映射到的对象词典  主站可以写数据，        从站接收主站写入的数据
		进行TRANSMIT PDO映射到的对象词典 主站可以读从站写入数据，从站写数据
*/
ec_pdo_info_t slave_axis0_pdos[] = {
    {0x1700, 4, slave_axis0_pdo_entries + 0},
    {0x1b00, 5, slave_axis0_pdo_entries + 4},
};

/*
	同步管理信息配置,上面已经有了PDO映射的信息, 要使PDO映射可用,要进行同步管理的配置
	说明:
	    ec_direction_t字段与上面pdo信息有关
	    0x1700 receive pdo map:从站接收，因而主站为输出 EC_DIR_OUTPUT
	    0x1b00 transmit pdo map:从站发送，因而主站方向为输入 EC_DIR_INPUT
	    
*/
ec_sync_info_t slave_axis0_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, slave_axis0_pdos + 0, EC_WD_ENABLE},
    {3, EC_DIR_INPUT, 1, slave_axis0_pdos + 1, EC_WD_DISABLE},
    {0xff}
};

//以下变量仅用于于调试模式
int master_fd = -1;                           
int slave_pos = TEC_SERVO_AXIS0_POSITION;     

//使用ethercat库函数时用到的变量定义 非直接ioctl模式
ec_master_t *slave_axis0_master = NULL;
static ec_master_state_t slave_axis0_master_state = {};
static ec_domain_t *slave_axis0_domain = NULL;
static unsigned char *slave_axis0_domain_pd = NULL;
static ec_domain_state_t slave_axis0_domain_state = {};
static ec_slave_config_t *slave_axis0_slave_config = NULL;
static ec_slave_config_state_t slave_axis0_slave_config_state = {};


/*
	@description
		Download data into slaves
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want download 
		
	@return
		0:
			download success 
		-1:
			dowolad failed
*/
static int sdo_download(int fd, ec_ioctl_slave_sdo_download_t *data)
{
	int i, err;
	err = ioctl(fd, EC_IOCTL_SLAVE_SDO_DOWNLOAD, data);
	if (err) 
	{
#if (DEBUG_MODEL == 1)
		LOG("sdo_download index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" failed,ErrMsg:%s\n", strerror(errno));
#endif //#if DEBUG_MODEL
		return -1;
    }
	else
	{
#if (DEBUG_MODEL == 1)
		struct timespec start;
		struct timespec stop;
		long diff_time_ns;
		
		clock_gettime(CLOCK_REALTIME, &start);
		ioctl(fd, EC_IOCTL_SLAVE_SDO_DOWNLOAD, data);
		clock_gettime(CLOCK_REALTIME, &stop);
		diff_time_ns = (stop.tv_sec - start.tv_sec) * 1000000000 + (stop.tv_nsec - start.tv_nsec);
		
		LOG("sdo_download index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" success\n");
		
		LOG("One time sdo download spend %ld ns\n", diff_time_ns);
#endif //#if DEBUG_MODEL
		return 0;
	}
}


/*
	@description
		Upload data into slaves
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want to store data of upload 
		
	@return
		0:
			upload success 
		-1:
			upload failed
*/
static int sdo_upload(int fd, ec_ioctl_slave_sdo_download_t *data)
{
	int i, err;
	err = ioctl(fd, EC_IOCTL_SLAVE_SDO_UPLOAD, data);
	if (err) 
	{
#if (DEBUG_MODEL == 1)
		LOG("SdoUpload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" failed,ErrMsg:%s\n", strerror(errno));
#endif //#if DEBUG_MODEL
		return -1;
    }
	else
	{
#if (DEBUG_MODEL == 1)
		struct timespec start;
		struct timespec stop;
		long diff_time_ns;
		
		clock_gettime(CLOCK_REALTIME, &start);
		ioctl(fd, EC_IOCTL_SLAVE_SDO_UPLOAD, data);
		clock_gettime(CLOCK_REALTIME, &stop);
		diff_time_ns = (stop.tv_sec - start.tv_sec) * 1000000000 + (stop.tv_nsec - start.tv_nsec);
		
		LOG("SdoUpload index:0x%4x,sub_index:0x%02x data:0x", data->sdo_index, data->sdo_entry_subindex);
		for (i=0; i<data->data_size; i++)
		{
			LOG("%02x", data->data[data->data_size-i-1]);
		}
		LOG(" success\n");
		LOG("One time sdo upload spend %ld ns\n", diff_time_ns);
#endif //#if DEBUG_MODEL
		return 0;
	}
}

/*
	@description
		Set slave states
	@param
		fd:
			File fd related to master which we use
		data:
			The pointer to data we want to store data of set 
		
	@return
		0:
			Set state success 
		-1:
			Set state failed
*/
static int set_slave_state(int master_fd, unsigned short slave_position, unsigned char al_state)
{
	
	ec_ioctl_slave_state_t data;

	data.al_state = slave_position;
	data.slave_position = slave_position;
	
	return ioctl(master_fd, EC_IOCTL_SLAVE_STATE, &data);

}


/*
	@description
		Main loop, we obtain real torque info or other info

	@param
		None
		
	@return
		None
*/
static void cycle_task(void)
{
	while (1)
	{
		usleep(1000);
		
		//receive process data
		ecrt_master_receive(slave_axis0_master);
    	ecrt_domain_process(slave_axis0_domain);
		
		// send process data
	    ecrt_domain_queue(slave_axis0_domain);
	    ecrt_master_send(slave_axis0_master);
		
	}
	
}

/*
	@description
		Signal hander

	@param
		Signal num
		
	@return
		None
*/
static void signal_handler(int signum) 
{
	switch (signum) 
	{
		case SIGALRM:
			break;
		case SIGSEGV:
		case SIGABRT:
		case SIGTERM:
		case SIGQUIT:
			if (slave_axis0_master)
			{
				ecrt_release_master(slave_axis0_master);
			}
			break;
		default:
			return;
			
    }
}

/*
	@description
		Test torque model

	@param
		Ethercat Master device /dev/EtherCATx
		
	@return
		0 :
			Test successfuly
		-1:
			Test failed
*/

int test_torque(char *master_dev)
{
	struct sigaction sa;
	uint16_t run_model;         /**< Free Run or DC */
	uint8_t control_model;      /**< Contorl model */
	int32_t torque_gradient;    /**< Torque Gradient*/
	int16_t common_data;        /**/
	int16_t set_torque_val;     /**< Set Torque value */
	uint16_t slave_position;    /**< Slave position. */
    uint16_t index;             /**< Index of the SDO. */
	uint8_t subindex;           /**< SubIndex of the SDO. */
    uint8_t *data;              /**< Data buffer to download. */
    size_t data_size;           /**< Size of the data buffer. */
    uint32_t abort_code;        /**< Abort code of the SDO download. */
	int err;
    
	//请求主机
    slave_axis0_master = ecrt_request_master(0);
    if (NULL != slave_axis0_master)
    {
		printf("Request master(0) success!\n\n");
	}
    else
   	{
		printf("Request master(0) failed!\n");
		return -1;
	}
    //请求域, 解释见IgH ethercatmaster手册
    slave_axis0_domain = ecrt_master_create_domain(slave_axis0_master);
    if (NULL != slave_axis0_domain)
    {
		printf("Master create domain success!\n\n");
	}
	else
	{
		printf("Master create domain failed!\n");
		return -1;
	}
    //获取从机配置
    if (!(slave_axis0_slave_config= ecrt_master_slave_config(slave_axis0_master, TEC_SERVO_AXIS0_ALIAS, TEC_SERVO_AXIS0_POSITION, TEC_SERVO_VENDOR_ID, TEC_SERVO_PRODUCT_ID))) 
    {
        fprintf(stderr, "Get slave configuration failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Get slave configuration success!\n\n");
	}
	//从机PDO配置
	//ecrt_slave_config_pdos与ecrt_master_slave_config成对出现校验从机是否配置成功
    printf("Configuring PDOs...\n");
    if (ecrt_slave_config_pdos(slave_axis0_slave_config, EC_END, slave_axis0_syncs)) 
	{
        fprintf(stderr, "Configure PDOs failed!\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Configure PDOs success!\n\n");
	}
	
    if (!(slave_axis0_slave_config = ecrt_master_slave_config(slave_axis0_master, TEC_SERVO_AXIS0_ALIAS, TEC_SERVO_AXIS0_POSITION, TEC_SERVO_VENDOR_ID, TEC_SERVO_PRODUCT_ID))) 
    {
        fprintf(stderr, "Get slave configuration failed after configure pdos !\n");
        return -1;
    }
	else
	{
		fprintf(stderr, "Get slave configuration success after configure pdos !\n");
	}
	//从机pdos注册
    if (ecrt_slave_config_pdos(slave_axis0_slave_config, EC_END, slave_axis0_syncs)) 
	{
        fprintf(stderr, "Slave configure PDOs failed!ErrMsg:%s\n", strerror(errno));
        return -1;
    }
	else
	{
		fprintf(stderr, "Slave configure PDOs configure success!\n\n");
	}
	//域注册
	if (ecrt_domain_reg_pdo_entry_list(slave_axis0_domain, slave_axis0_domain_regs))
	{
        fprintf(stderr, "Reg PDO entry registration failed!ErrMsg:%s\n", strerror(errno));
        return -1;
    }
	else
	{
		fprintf(stderr, "Reg PDO entry registration success!\n\n");
	}
	

	//configure slave axi0
	slave_position = TEC_SERVO_AXIS0_POSITION;

	//设置自由运行模式
	/*
	run_model = TEC_SERVO_RUN_MODEL_FREE;
	index = 0x1c32;
	subindex = 0x00;
	data = (uint8_t *)&run_model;
	data_size = sizeof(run_model);
	err = ecrt_master_sdo_download(slave_axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set free run model  step1 success!\n");
	}
	else
	{
		return err;
	}
	run_model = TEC_SERVO_RUN_MODEL_FREE;
	index = 0x1c33;
	subindex = 0x00;
	data = (uint8_t *)&run_model;
	data_size = sizeof(run_model);
	err = ecrt_master_sdo_download(slave_axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set free run model success!\n");
	}
	else
	{
		return err;
	}
	*/
	
	//设置力矩控制模式
	control_model = TEC_SERVO_CONROL_MODE_PROFILE_TORQUE;
	index = TEC_SERVO_CONROL_MODE_CONFIGURE_SDO_INDEX;
	subindex = TEC_SERVO_CONROL_MODE_CONFIGURE_SDO_SUBINDEX;
	data = (uint8_t *)&control_model;
	data_size = sizeof(control_model);
	err = ecrt_master_sdo_download(slave_axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set torque control model success!\n");
	}
	else
	{
		return err;
	}

	//设置力矩上升斜率
	torque_gradient = 1000;
	index = TEC_SERVO_TORQUE_GRADIENT_SDO_INDEX;
	subindex = TEC_SERVO_TORQUE_GRADIENT_SDO_SUBINDEX;
	data = (uint8_t *)&torque_gradient;
	data_size = sizeof(torque_gradient);
	err = ecrt_master_sdo_download(slave_axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set  torque gradient success!\n");
	}
	else
	{
		return err;
	}

	//设置输出力矩
	set_torque_val = 100;
	index = TEC_SERVO_GOAL_TORQUE_SDO_INDEX;
	subindex = TEC_SERVO_GOAL_TORQUE_SDO_SUBINDEX;
	data = (uint8_t *)&set_torque_val;
	data_size = sizeof(set_torque_val);
	err = ecrt_master_sdo_download(slave_axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set goal torque success!\n");
	}
	else
	{
		return err;
	}

	//设置伺服ON
	common_data = 0x06;
	index = TEC_SERVO_CONTROL_WORD_SDO_INDEX;
	subindex = TEC_SERVO_CONTROL_WORD_SDO_SUBINDEX;
	data = (uint8_t *)&common_data;
	data_size = sizeof(common_data);
	err = ecrt_master_sdo_download(slave_axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set sevrvo step 1 success!\n");
	}
	else
	{
		return err;
	}
	common_data = 0x07;
	err = ecrt_master_sdo_download(slave_axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set sevrvo step 2 success!\n");
	}
	else
	{
		return err;
	}
	common_data = 0x0f;
	err = ecrt_master_sdo_download(slave_axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set sevrvo step 1 success!\n");
	}
	else
	{
		return err;
	}
	
	//注册信号处理函数
	sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGSEGV, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGABRT, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGTERM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGQUIT, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }


	//激活主机
    printf("Activating master...\n");
    if (ecrt_master_activate(slave_axis0_master))
    {
		fprintf(stderr, "Activate master failed!\n");
		return -1;
	}
	else
	{
		fprintf(stderr, "Activate master success!\n\n");
	}
	//process data in domain1
    if (!(slave_axis0_domain_pd= ecrt_domain_data(slave_axis0_domain))) 
	{
		fprintf(stderr, "ecrt_domain_data(domain1) failed!\n");
		return -1;
    }
	else
	{
		fprintf(stderr, "ecrt_domain_data(domain1) success!\n\n");
	}
	//开始周期任务
	cycle_task();
    return 0;
}


/******************************For Debug**************************************/
static int set_axis_on_off(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
	
}

static int set_axix_max_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
}

static int set_axix_goal_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
}

static int get_axix_real_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
		
}

static int get_axix_encoder_inner_real_pos(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
		
}

static int get_axix_encoder_real_pos(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
		
}

static int get_axix_encoder_velocity(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
		
}



static int quit_debug(int master_fd, unsigned short slave_position, int *arg_val)
{
	exit(0);
	return 0;
}

CmdNode cmd_list[] = {
		{CMDID_SET_AXIS_ON_OFF, set_axis_on_off},
		{CMDID_SET_AXIS_MAX_OUTPUT_TORQUE, set_axix_max_output_torque},
		{CMDID_SET_AXIS_GOAL_OUTPUT_TORQUE, set_axix_goal_output_torque},
		{CMDID_GET_AXIS_REAL_OUTPUT_TORQUE, get_axix_real_output_torque},
		{CMDID_GET_AXIS_ENCODER_INNER_REAL_POS, get_axix_encoder_inner_real_pos},
		{CMDID_GET_AXIS_ENCODER_REAL_POS, get_axix_encoder_real_pos},
		{CMDID_GET_AXIS_ENCODER_VELOCITY, get_axix_encoder_velocity},
		{CMDID_QUIT_DEBUG, quit_debug},
		{CMDID_INVALID, NULL}
};
#define CMD_LIST_SIZE (sizeof(cmd_list) / sizeof(cmd_list[0]))

int cmd_process(int cmdid, int master_fd, unsigned short slave_position, int *arg)
{
	int i;
	for (i = 0; i<CMD_LIST_SIZE; i++) 
	{
		if (cmdid == cmd_list[i].cmdid)
		{
			return cmd_list[i].f(master_fd, slave_position, arg);
		}
	}
	if (i == CMD_LIST_SIZE)
	{
		return -1;
	}
}

void debug_menu(void)
{

	printf("****************Ethercat Servo Torque Debug****************\n");
	printf("1 Set axis servo on/off                     (on:cmdid axis_num 1 off:cmdid axi_num 0)\n");
	printf("2 Set axis max output_torque                (cmdid axis_num max_output_torque)\n");
	printf("3 Set axis goal torque                      (cmdid axis_num goal_torque)\n");
	printf("4 Get axis real outpue torque               (cmdid axis_num 0)\n");
	printf("5 Get axis encoder inner real pos           (cmdid 0 0)\n");
	printf("6 Get axis encoder real pos                 (cmdid 0 0)\n");
	printf("7 Get axis encoder velocity                 (cmdid 0 0)\n");
	printf("0 Quit debug system                         (cmdid 0 0)\n");
	printf("Input cmd:");
	
}

int debug_torque(char *master_dev)
{
	LOG("Debug torque have some unsolved problems,return....\n");
	return 0;
	
	unsigned short slave_positon = slave_pos;
	int arg_val;
	int cmdid;
	
	if (NULL == master_dev)
	{
		LOG("Null mater_dev pointer!\n");
		return;
	}
	master_fd = open(master_dev, O_RDWR);
	if (master_fd < 0)
	{
		LOG("Open file %s error! \n", master_dev);
		return;
	}
	while (1)
	{
		debug_menu();
		if (scanf("%d %d %d", &cmdid, &slave_positon, &arg_val) != 3)
		{
			LOG("Error input cmd!!!\n");			
			getchar();
			continue;
		}
		if (slave_pos != slave_positon)
		{
			LOG("Waring now only supported slave_positon=%d\n", slave_pos);
			continue;
		}
		if (
			cmdid == CMDID_GET_AXIS_REAL_OUTPUT_TORQUE     ||
			cmdid == CMDID_GET_AXIS_ENCODER_INNER_REAL_POS || 
			cmdid == CMDID_GET_AXIS_ENCODER_REAL_POS       || 
			cmdid == CMDID_GET_AXIS_ENCODER_VELOCITY
		   )
		{
			if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
			{
				LOG("Execute cmd success!\n\n");
				LOG("Get Axis:%d Value:%d\n", slave_positon, arg_val);
				continue;
			}
			
		}
		if (cmdid == CMDID_SET_AXIS_MAX_OUTPUT_TORQUE )
		{
			if (arg_val >= MAX_OUTPUT_TORQUE)
			{
				arg_val = MAX_OUTPUT_TORQUE;
				LOG("Warning max output torque limit(0~%d)!\n", MAX_OUTPUT_TORQUE);
				
			}
			if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
			{
				LOG("Execute cmd success!\n\n");
				continue;
			}
		}
		if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
		{
			LOG("Execute cmd success!\n\n");
			continue;
		}
		
	}
	
	
}
/******************************For Debug**************************************/




