#ifndef MY_TEST_TORQUE_H
#define MY_TEST_TORQUE_H

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdint.h>
#include "ecrt.h"
#include "ioctl.h"
#include "tec_servo.h"


#define PRINTF_LEVEL 1
#if (PRINTF_LEVEL == 0)
#define LOG(...) printf("DEBUG=======>"__VA_ARGS__)
#define LOG_ERROR(err_msg) printf("DEBUG_ERROR=======> %s:%s:%d", __FILE__, __FUNCTION__, __LINE__);printf(" (err_msg:%s)\n", err_msg)
#elif  (PRINTF_LEVEL == 1)
#define LOG(...) printf(__VA_ARGS__)
#define LOG_ERROR(err_msg) printf("DEBUG_ERROR=======> %s:%s:%d", __FILE__, __FUNCTION__, __LINE__);printf(" (err_msg:%s)\n", err_msg)
#else
#define LOG(...) do {} while(0)
#define LOG_ERROR(err_msg) do {} while(0)
#endif

//是否启动调试模式
#define DEBUG_MODEL 1


int test_torque(char *master_dev);


/******************************For Debug**************************************/
#define CMDID_INVALID                              -1
#define CMDID_SET_AXIS_ON_OFF                       1
#define CMDID_SET_AXIS_MAX_OUTPUT_TORQUE            2
#define CMDID_SET_AXIS_GOAL_OUTPUT_TORQUE           3
#define CMDID_GET_AXIS_REAL_OUTPUT_TORQUE           4
#define CMDID_GET_AXIS_ENCODER_INNER_REAL_POS       5
#define CMDID_GET_AXIS_ENCODER_REAL_POS             6
#define CMDID_GET_AXIS_ENCODER_VELOCITY             7
#define CMDID_QUIT_DEBUG                            0

typedef int (*cmd_callback)(int master_fd, unsigned short slave_position, int *arg);
typedef struct CmdNode{
	int cmdid;
	cmd_callback f;
}CmdNode;

int cmd_process(int cmdid, int master_fd, unsigned short slave_position, int *arg);
void debug_menu(void);
int debug_torque(char *master_dev);
/******************************For Debug**************************************/


#endif //MY_TEST_TORQUE_H


