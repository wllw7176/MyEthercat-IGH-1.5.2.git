#include "test.h"

int main(int argc, char *argv[])
{
	int model;
	if (argc != 3)
	{
		LOG("Usage %s devname model(1:debug model, 0:normal model)\n", argv[0]);
		return -1;
	}
	
	sscanf(argv[2], "%d", &model);
	if (model) 
	{
		debug_torque(argv[1]);
	}
	else
	{
		test_torque(argv[1]);
	}

	return 0;

}



