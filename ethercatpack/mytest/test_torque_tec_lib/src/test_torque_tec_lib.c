#include "test.h"

/****************************************************
 * PDO 映射信息,通过ethercat cstruct
 * Master 0, Slave 0, "AEM-090-30"
 * Vendor ID:       0x000000ab
 * Product code:    0x00001030
 * Revision number: 0x00010003

 ec_pdo_entry_info_t slave_0_pdo_entries[] = {
	 {0x6040, 0x00, 16},
	 {0x607a, 0x00, 32},
	 {0x60b1, 0x00, 32},
	 {0x60b2, 0x00, 16},
	 {0x6041, 0x00, 16},
	 {0x6064, 0x00, 32},
	 {0x60f4, 0x00, 32},
	 {0x606c, 0x00, 32},
	 {0x6077, 0x00, 16},
 };
 
 ec_pdo_info_t slave_0_pdos[] = {
	 {0x1700, 4, slave_0_pdo_entries + 0},
	 {0x1b00, 5, slave_0_pdo_entries + 4},
 };
 
 ec_sync_info_t slave_0_syncs[] = {
	 {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
	 {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
	 {2, EC_DIR_OUTPUT, 1, slave_0_pdos + 0, EC_WD_ENABLE},
	 {3, EC_DIR_INPUT, 1, slave_0_pdos + 1, EC_WD_DISABLE},
	 {0xff}
 };
****************************************************/

//保存控制字与状态字在domain中的偏移位置 读写PDO入口要用到
unsigned int off_bytes_axis0_contorl_word;
unsigned int off_bytes_axis0_status_word;
unsigned int off_bytes_axis0_axtual_torque;

/*
	这里定义我们要操作PDO对象的信息,我们可以定义多个PDO映射,但是可以只使用其中的几个
*/
const static ec_pdo_entry_reg_t axis0_domain_regs[] = {
    {
		TEC_SERVO_AXIS0_ALIAS,               //别名
		TEC_SERVO_AXIS0_POSITION,            //位置
		TEC_SERVO_AXIS0_VENDORID,            //厂商id
		TEC_SERVO_AXIS0_PRODUCTID,           //产品id
		0x6040,                              //索引
		0x00, //子索引 
		&off_bytes_axis0_contorl_word       //PDO入口在process_data字节偏移量
	},
	{
		TEC_SERVO_AXIS0_ALIAS,               //别名
		TEC_SERVO_AXIS0_POSITION,            //位置
		TEC_SERVO_AXIS0_VENDORID,                 //厂商id
		TEC_SERVO_AXIS0_PRODUCTID,                //产品id
		0x6041,    //索引
		0x00, //子索引 
		&off_bytes_axis0_status_word       //PDO入口在process_data字节偏移量
	},
	{
		TEC_SERVO_AXIS0_ALIAS,               //别名
		TEC_SERVO_AXIS0_POSITION,            //位置
		TEC_SERVO_AXIS0_VENDORID,                 //厂商id
		TEC_SERVO_AXIS0_PRODUCTID,                //产品id
		0x6077,    //索引
		0x00, //子索引 
		&off_bytes_axis0_axtual_torque       //PDO入口在process_data字节偏移量
	},
    {
    }
}; 

/*
  要进行 PDO 映射对象字典索引及大小
 */
 
ec_pdo_entry_info_t axis0_pdo_entries[] = {
    {0x6040, 0x00, 16},
    {0x607a, 0x00, 32},
    {0x60b1, 0x00, 32},
    {0x60b2, 0x00, 16},
    {0x6041, 0x00, 16},
    {0x6064, 0x00, 32},
    {0x60f4, 0x00, 32},
    {0x606c, 0x00, 32},
    {0x6077, 0x00, 16},
};

/*使用默认的PDO映射
ec_pdo_entry_info_t axis0_pdo_entries[] = {
    {0x6040},
    {0x607a},
    {0x60b1},
    {0x60b2},
    {0x6041},
    {0x6064},
    {0x60f4},
    {0x606c},
    {0x6077},
};
*/

/*
	RECEIVE PDO MAPPING PARAMETERS  (RxPDO) INDEX 0X1700
	TRANSMIT PDO MAPPING PARAMETERS (TxPDO) INDEX 0X1B00
	说明:
		PDO映射发送与接收是相对slave来说
		进行RxPDO映射到的对象词典,slave接收数据,主站写入数据
		进行TxPDO映射到的对象词典,slave写入数据,主站接收数据
*/
ec_pdo_info_t axis0_pdos[] = {
    {0x1700, 4, axis0_pdo_entries + 0},
    {0x1b00, 5, axis0_pdo_entries + 4},
};

/*
	同步管理信息配置,上面已经有了PDO映射的信息, 要使PDO映射可用,要进行同步管理的配置
	说明:
	    ec_direction_t字段与上面pdo信息有关
	    0x1700 receive pdo  map:从站接收，因而主站为输出 EC_DIR_OUTPUT
	    0x1b00 transmit pdo map:从站发送，因而主站方向为输入 EC_DIR_INPUT
	    
*/
ec_sync_info_t axis0_syncs[] = {
	{0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},                  //Mailbox Receive 具体解释见山羊电机手册P215
	{1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},                   //Mailbox Send
	{2, EC_DIR_OUTPUT, 1, axis0_pdos + 0, EC_WD_ENABLE},  //PDO output
	{3, EC_DIR_INPUT, 1, axis0_pdos + 1, EC_WD_DISABLE},   //PDO input 
	{0xff}
    
};

//以下变量仅用于于调试模式
int master_fd = -1;

//使用ethercat库函数时用到的变量定义 非直接ioctl模式
ec_master_t *axis0_master = NULL;
static ec_master_state_t axis0_master_state = {};
static ec_domain_t *axis0_domain = NULL;
static unsigned char *axis0_domain_pd = NULL;
static ec_domain_state_t axis0_domain_state = {};
ssize_t axis0_domain_pd_bytes_count;
static ec_slave_config_t *axis0_slave_config = NULL;
static ec_slave_config_state_t axis0_slave_config_state = {};
static ec_sdo_request_t *sdo_req_real_torque = NULL;



/*
	@description
		Main loop, we obtain real torque info or other info

	@param
		None
		
	@return
		None
*/
static void cycle_task(void)
{
	long long cycle_count = 0;
	
	while (1)
	{
		cycle_count++;
		usleep(1000);

		//在一定周期内一定要进行过程数据处理让状态机维持在OP状态
		//receive process data
		ecrt_master_receive(axis0_master);
    	ecrt_domain_process(axis0_domain);

		//获取数据
		if (cycle_count % 1000 == 0)
		{
			LOG("Actual Torque Val:0x%04X\n", EC_READ_S16(axis0_domain_pd + off_bytes_axis0_axtual_torque));
		}
#if (1)
		if (cycle_count % 15000 == 0)
		{
			int i = 0, j = 0;
			LOG("axis0_domain process data size:%d\n", axis0_domain_pd_bytes_count);
			for (i = 0; i<axis0_domain_pd_bytes_count; i++)
			{
				j++;
				LOG("%02X ", EC_READ_U8(axis0_domain_pd + i));
				if (j % 20 == 0)
				{
					LOG("\n");
				}
				
			}
			LOG("\n");
		}
#endif
		// send process data (send process data 与 receive process data 要成对出现)
	    ecrt_domain_queue(axis0_domain);
	    ecrt_master_send(axis0_master);
		
	}
	
}

/*
	@description
		Signal hander

	@param
		Signal num
		
	@return
		None
*/
static void signal_handler(int signum) 
{
	switch (signum) 
	{
		case SIGALRM:
			break;
		case SIGSEGV:
		case SIGABRT:
		case SIGTERM:
		case SIGQUIT:
			if (axis0_master)
			{
				ecrt_release_master(axis0_master);
			}
			break;
		default:
			return;
			
    }
}

/*
	@description
		Test torque model

	@param
		Ethercat Master device /dev/EtherCATx
		
	@return
		0 :
			Test successfuly
		-1:
			Test failed
*/

int test_torque(char *master_dev)
{

	struct sigaction sa;
	struct itimerval tv;
	uint16_t run_model;         /**< Free Run or DC */
	uint8_t control_model;      /**< Contorl model */
	int32_t torque_gradient;    /**< Torque Gradient*/
	int16_t common_data;        /**/
	int16_t set_torque_val;     /**< Set Torque value */
	uint16_t slave_position;    /**< Slave position. */
    uint16_t index;             /**< Index of the SDO. */
	uint8_t subindex;           /**< SubIndex of the SDO. */
    uint8_t *data;              /**< Data buffer to download. */
    size_t data_size;           /**< Size of the data buffer. */
    uint32_t abort_code;        /**< Abort code of the SDO download. */
	int err;

	
	//请求主机
    axis0_master = ecrt_request_master(0);
    if (NULL != axis0_master)
    {
		printf("Request master(0) success!\n");
	}
    else
   	{
		printf("Request master(0) failed!\n");
		return -1;
	}
    //请求域, 解释见IgH ethercatmaster手册
    axis0_domain = ecrt_master_create_domain(axis0_master);
    if (NULL != axis0_domain)
    {
		printf("Master create domain success!\n");
	}
	else
	{
		printf("Master create domain failed!\n");
		return -1;
	}
    //获取从机配置
    if (!(axis0_slave_config = ecrt_master_slave_config(axis0_master, TEC_SERVO_AXIS0_ALIAS, TEC_SERVO_AXIS0_POSITION, TEC_SERVO_AXIS0_VENDORID, TEC_SERVO_AXIS0_PRODUCTID))) 
    {
        LOG( "Get slave configuration failed!\n");
        return -1;
    }
	else
	{
		LOG( "Get slave configuration success!\n");
	}
	//从机PDO配置
    if (ecrt_slave_config_pdos(axis0_slave_config, EC_END, axis0_syncs)) 
	{
        LOG("Configure PDOs failed!\n");
        return -1;
    }
	else
	{
		LOG("Configure PDOs success!\n");
	}
    if (!(axis0_slave_config = ecrt_master_slave_config(axis0_master, TEC_SERVO_AXIS0_ALIAS, 
																TEC_SERVO_AXIS0_POSITION, TEC_SERVO_AXIS0_VENDORID, TEC_SERVO_AXIS0_PRODUCTID))) 
    {
        LOG( "Get slave configuration failed after configure pdos !\n");
        return -1;
    }
	else
	{
		LOG( "Get slave configuration success after configure pdos !\n");
	}
	
	//从机pdos注册
    if (ecrt_slave_config_pdos(axis0_slave_config, EC_END, axis0_syncs)) 
	{
        LOG( "Slave configure PDOs failed!ErrMsg:%s\n", strerror(errno));
        return -1;
    }
	else
	{
		LOG( "Slave configure PDOs configure success!\n");
	}
	
	//域注册
	if (ecrt_domain_reg_pdo_entry_list(axis0_domain, axis0_domain_regs))
	{
        LOG( "Reg PDO entry registration failed!ErrMsg:%s\n", strerror(errno));
        return -1;
    }
	else
	{
		LOG( "Reg PDO entry registration success!\n");
		LOG("off_bytes_axis0_contorl_word:%d off_bytes_axis0_status_word:%d off_bytes_axis0_axtual_torque:%d\n", 
			off_bytes_axis0_contorl_word, off_bytes_axis0_status_word, off_bytes_axis0_axtual_torque);
	}

	
	//sdo 配置
	sdo_req_real_torque = ecrt_slave_config_create_sdo_request(axis0_slave_config, TEC_SERVO_AXIS0_REAL_TORQUE_SDO_INDEX, TEC_SERVO_AXIS0_REAL_TORQUE_SDO_SUBINDEX, 2);
	if (!sdo_req_real_torque)
	{
		LOG("Create sdo_req_real_torque failed!, ErrMsg:%s\n", strerror(errno));
		return -1;
	}
	else
	{
		LOG("Create sdo_req_real_torque success\n");
	}
	ecrt_sdo_request_timeout(sdo_req_real_torque, 1000); // ms
	ecrt_sdo_request_read(sdo_req_real_torque);          // retry reading
	
	//configure slave axi0
	slave_position = TEC_SERVO_AXIS0_POSITION;
	
	//设置力矩控制模式
	control_model = TEC_SERVO_CONROL_MODE_PROFILE_TORQUE;
	index = TEC_SERVO_AXIS0_MODEL_CONFIGURE_SDO_INDEX;
	subindex = TEC_SERVO_AXIS0_MODEL_CONFIGURE_SDO_SUBINDEX;
	data = (uint8_t *)&control_model;
	data_size = sizeof(control_model);
	err = ecrt_master_sdo_download(axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set torque control model success!\n");
	}
	else
	{
		return err;
	}

	//设置力矩上升斜率
	torque_gradient = 50;
	index = TEC_SERVO_AXIS0_TORQUE_GRADIENT_SDO_INDEX;
	subindex = TEC_SERVO_AXIS0_TORQUE_GRADIENT_SDO_SUBINDEX;
	data = (uint8_t *)&torque_gradient;
	data_size = sizeof(torque_gradient);
	err = ecrt_master_sdo_download(axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set  torque gradient success!\n");
	}
	else
	{
		return err;
	}

	//设置输出力矩
	set_torque_val = 100;
	index = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_INDEX;
	subindex = TEC_SERVO_AXIS0_GOAL_TORQUE_SDO_SUBINDEX;
	data = (uint8_t *)&set_torque_val;
	data_size = sizeof(set_torque_val);
	err = ecrt_master_sdo_download(axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set goal torque success!\n");
	}
	else
	{
		return err;
	}

	//设置伺服ON
	common_data = 0x06;
	index = TEC_SERVO_AXIS0_CONTROL_WORD_SDO_INDEX;
	subindex = TEC_SERVO_AXIS0_CONTROL_WORD_SDO_SUBINDEX;
	data = (uint8_t *)&common_data;
	data_size = sizeof(common_data);
	err = ecrt_master_sdo_download(axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set sevro on step 1 success!\n");
	}
	else
	{
		return err;
	}
	common_data = 0x07;
	err = ecrt_master_sdo_download(axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set sevro on step 2 success!\n");
	}
	else
	{
		return err;
	}
	common_data = 0x0f;
	err = ecrt_master_sdo_download(axis0_master, slave_position, index, subindex,  data,  data_size, &abort_code);
	if (!err)
	{
		LOG("Set sevro on step 3 success!\n");
	}
	else
	{
		return err;
	}

	//注册信号处理函数
	sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
	
	if (sigaction(SIGSEGV, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGABRT, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGTERM, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGQUIT, &sa, 0)) 
	{
        LOG( "Failed to install signal handler!\n");
        return -1;
    }


	//激活主机
    if (ecrt_master_activate(axis0_master))
    {
		LOG( "Activate master failed!\n");
		return -1;
	}
	else
	{
		LOG( "Activate master success!\n");
	}
	//process data in domain1

	axis0_domain_pd_bytes_count = ecrt_domain_size(axis0_domain);
	if (axis0_domain_pd_bytes_count < 0)
	{
		LOG("Get axis0_domain_pd_bytes_count error!\n");
		return -1;
	}
	
    if (!(axis0_domain_pd = ecrt_domain_data(axis0_domain))) 
	{
		LOG( "ecrt_domain_data(domain1) failed!\n");
		return -1;
    }
	else
	{
		LOG( "ecrt_domain_data(domain1) success, axis0_domain_pd_bytes_count:%d!\n", axis0_domain_pd_bytes_count);
	}
	
	
	//开始周期任务
	cycle_task();
    return 0;
}




/******************************For Debug**************************************/
static int set_axis_on_off(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
	
}

static int set_axix_max_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
}

static int set_axix_goal_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
}

static int get_axix_real_output_torque(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
		
}

static int get_axix_encoder_inner_real_pos(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
		
}

static int get_axix_encoder_real_pos(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
		
}

static int get_axix_encoder_velocity(int master_fd, unsigned short slave_position, int *arg_val)
{

	return 0;
		
}



static int quit_debug(int master_fd, unsigned short slave_position, int *arg_val)
{
	exit(0);
	return 0;
}

CmdNode cmd_list[] = {
		{CMDID_SET_AXIS_ON_OFF, set_axis_on_off},
		{CMDID_SET_AXIS_MAX_OUTPUT_TORQUE, set_axix_max_output_torque},
		{CMDID_SET_AXIS_GOAL_OUTPUT_TORQUE, set_axix_goal_output_torque},
		{CMDID_GET_AXIS_REAL_OUTPUT_TORQUE, get_axix_real_output_torque},
		{CMDID_GET_AXIS_ENCODER_INNER_REAL_POS, get_axix_encoder_inner_real_pos},
		{CMDID_GET_AXIS_ENCODER_REAL_POS, get_axix_encoder_real_pos},
		{CMDID_GET_AXIS_ENCODER_VELOCITY, get_axix_encoder_velocity},
		{CMDID_QUIT_DEBUG, quit_debug},
		{CMDID_INVALID, NULL}
};
#define CMD_LIST_SIZE (sizeof(cmd_list) / sizeof(cmd_list[0]))

int cmd_process(int cmdid, int master_fd, unsigned short slave_position, int *arg)
{
	int i;
	for (i = 0; i<CMD_LIST_SIZE; i++) 
	{
		if (cmdid == cmd_list[i].cmdid)
		{
			return cmd_list[i].f(master_fd, slave_position, arg);
		}
	}
	if (i == CMD_LIST_SIZE)
	{
		return -1;
	}
}

void debug_menu(void)
{

	printf("****************Ethercat Servo Torque Debug****************\n");
	printf("1 Set axis servo on/off                     (on:cmdid axis_num 1 off:cmdid axi_num 0)\n");
	printf("2 Set axis max output_torque                (cmdid axis_num max_output_torque)\n");
	printf("3 Set axis goal torque                      (cmdid axis_num goal_torque)\n");
	printf("4 Get axis real outpue torque               (cmdid axis_num 0)\n");
	printf("5 Get axis encoder inner real pos           (cmdid 0 0)\n");
	printf("6 Get axis encoder real pos                 (cmdid 0 0)\n");
	printf("7 Get axis encoder velocity                 (cmdid 0 0)\n");
	printf("0 Quit debug system                         (cmdid 0 0)\n");
	printf("Input cmd:");
	
}

int debug_torque(char *master_dev)
{
	LOG("Debug torque have some unsolved problems,return....\n");
	return 0;
	
	unsigned short slave_positon = TEC_SERVO_AXIS0_POSITION;
	int arg_val;
	int cmdid;
	
	if (NULL == master_dev)
	{
		LOG("Null mater_dev pointer!\n");
		return;
	}
	master_fd = open(master_dev, O_RDWR);
	if (master_fd < 0)
	{
		LOG("Open file %s error! \n", master_dev);
		return;
	}
	while (1)
	{
		debug_menu();
		if (scanf("%d %d %d", &cmdid, &slave_positon, &arg_val) != 3)
		{
			LOG("Error input cmd!!!\n");			
			getchar();
			continue;
		}
		if (master_fd != TEC_SERVO_AXIS0_POSITION)
		{
			LOG("Waring now only supported slave_positon=%d\n", slave_positon);
			continue;
		}
		if (
			cmdid == CMDID_GET_AXIS_REAL_OUTPUT_TORQUE     ||
			cmdid == CMDID_GET_AXIS_ENCODER_INNER_REAL_POS || 
			cmdid == CMDID_GET_AXIS_ENCODER_REAL_POS       || 
			cmdid == CMDID_GET_AXIS_ENCODER_VELOCITY
		   )
		{
			if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
			{
				LOG("Execute cmd success!\n");
				LOG("Get Axis:%d Value:%d\n", slave_positon, arg_val);
				continue;
			}
			
		}
		if (cmdid == CMDID_SET_AXIS_MAX_OUTPUT_TORQUE )
		{
			if (arg_val >= MAX_OUTPUT_TORQUE)
			{
				arg_val = MAX_OUTPUT_TORQUE;
				LOG("Warning max output torque limit(0~%d)!\n", MAX_OUTPUT_TORQUE);
				
			}
			if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
			{
				LOG("Execute cmd success!\n");
				continue;
			}
		}
		if (!cmd_process(cmdid, master_fd, slave_positon, &arg_val))
		{
			LOG("Execute cmd success!\n");
			continue;
		}
		
	}
	
	
}
/******************************For Debug**************************************/




