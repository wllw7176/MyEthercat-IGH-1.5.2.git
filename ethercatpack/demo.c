#ifdef __cplusplus
extern "C" {
#endif

#include <unistd.h>
#include <signal.h>
#include <sched.h>
#include "platform.h"
#include "api.h"
#include "klog.h"
#include "ethercat.h"


MasterSpecifiedInfo_T master_specified_info;
//pthread_t ecat_cycle_task_pid;

static void signal_handler(int signum) 
{
	
	switch (signum) 
	{
		case SIGSEGV:
			EC_COMMON_LOG("Caught SIGNAL SIGSEGV\n");
			break;
		case SIGABRT:
			EC_COMMON_LOG("Caught SIGNAL SIGABRT\n");
			break;
		case SIGTERM:
			EC_COMMON_LOG("Caught SIGNAL SIGTERM\n");
			break;
		case SIGQUIT:
			EC_COMMON_LOG("Caught SIGNAL SIGQUIT\n");
			if (1 == master_specified_info.init_flag)
				ec_common_master_free(&master_specified_info);
			pthread_cancel(ecat_cycle_task_pid);
			break;
		default:
			return;
			
    }
}


int ethercat_init(void)
{
	struct sigaction sa;
	int ret;
	unsigned short control_word;
	unsigned short sm2_sync_model, sm3_sync_model;
	unsigned char operation_model;
	unsigned int abort_code;
	size_t result_size;
	int i, val;
	
	ec_common_reset_master_specified_info(&master_specified_info);
	ec_common_config_master_specified_info(&master_specified_info, 0, SLAVE_COUNT, slaves_specified_info);
	ret = ec_common_init_master_specified_info(&master_specified_info);
	ret = ec_common_activate_master(&master_specified_info);
	if (ret != EC_COMMON_OK)
	{
		EC_COMMON_LOG("ec_common_init_master_specified_info failed!");
		return -1;
	}

	//对每个从站执行相同的初始化操作,目前先固定设为力矩操作模式,自由运行模式
	//下面for 循环里面代码一般不要去改动,改动可能导致从站不能正常初始化
	for (i=0; i<SLAVE_COUNT; i++)
	{
		//setp 1 :进行其他设置前0x6040, 0x00=0x06 让电机处于shutdown状态 山羊电机手册 P229
		control_word= 0x06;	
		ret = ec_common_sdo_download(&master_specified_info,  &master_specified_info.slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, (uint8_t *)&control_word, sizeof(control_word), &abort_code);
		if (ret)
		{
			EC_COMMON_LOG("ec_common_sdo_download failed,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		else
		{
			EC_COMMON_LOG("Set slave_pos:%d shutdown success\n", i);
		}
			
		//目前没有启动dc同步模式,因而要配置sm2,sm3为自由模式
		//Step 2 set sm2 to free run model
		ret = ec_common_sdo_upload(&master_specified_info,  &master_specified_info.slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_SM2_SYNC_TYPE_U16, (uint8_t *)&sm2_sync_model, sizeof(sm2_sync_model), &result_size, &abort_code);
		if (ret)
		{
			EC_COMMON_LOG("ec_common_sdo_upload failed, %s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		if (sm2_sync_model != RUN_MODEL_FREE) //If sm2 is free run model, we need't configure it
		{
			sm2_sync_model = RUN_MODEL_FREE;
			ret = ec_common_sdo_download(&master_specified_info,  &master_specified_info.slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_SM2_SYNC_TYPE_U16, (uint8_t *)&sm2_sync_model, sizeof(sm2_sync_model), &abort_code);
			if (ret)
			{
				EC_COMMON_LOG("ec_common_sdo_download failed,%s:%d\n", __FILE__, __LINE__);
				return -1;
			}
			else
			{
				EC_COMMON_LOG("Set slave_pos:%d sm2 free run model success\n", i);
			}
		}
		else
		{
			EC_COMMON_LOG("Slave %d already in sm2 free run model\n", i);
		}
		
		//set sm3 to free run model
		ret = ec_common_sdo_upload(&master_specified_info,  &master_specified_info.slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_SM3_SYNC_TYPE_U16, (uint8_t *)&sm2_sync_model, sizeof(sm2_sync_model), &result_size, &abort_code);
		if (ret)
		{
			EC_COMMON_LOG("ec_common_sdo_upload failed, %s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		if (sm2_sync_model != RUN_MODEL_FREE) //If sm3 is free run model, we need't configure it
		{
			sm2_sync_model = RUN_MODEL_FREE;
			ret = ec_common_sdo_download(&master_specified_info,  &master_specified_info.slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_SM3_SYNC_TYPE_U16, (uint8_t *)&sm2_sync_model, sizeof(sm2_sync_model), &abort_code);
			if (ret)
			{
				EC_COMMON_LOG("ec_common_sdo_download failed,%s:%d\n", __FILE__, __LINE__);
				return -1;
			}
			else
			{
				EC_COMMON_LOG("Set slave_pos:%d sm3 free run model success\n", i);
			}
		}
		else
		{
			EC_COMMON_LOG("Slave %d already in sm3 free run model\n", i);
		}
		
		/*
		//由插补命令驱动
		//Step 3:set torque model 转移到插补命令处理函数中
		operation_model = OPERATION_MODE_PROFILE_TORQUE;
		ret = ec_common_sdo_download(&master_specified_info,  &master_specified_info.slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_OPERATION_MODEL_S8, &operation_model, sizeof(operation_model), &abort_code);
		if (ret)
		{
			EC_COMMON_LOG("ec_common_sdo_download failed,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		*/

	}

	/*
	//由插补命令驱动
	//step 4: activate Master
	ret = ec_common_activate_master(&master_specified_info);
	if (EC_COMMON_OK != ret)
	{
		EC_COMMON_LOG("ec_common_activate_master failed,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}

	//Step 5:因为使用PDO方式使能电机,所以一定在主机激活之后执行使能操作
	for (i=0; i<SLAVE_COUNT; i++)
	{
		
		val = 0x0f;
		ret = ec_common_set_pdo_data(&master_specified_info, &master_specified_info.slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, EC_COMMON_DATA_TYPE_S16, (void *)&val);
		if (ret != EC_COMMON_OK)
		{
			EC_COMMON_LOG("ec_common_set_pdo_data error, %s:%d\n", __FILE__, __LINE__);
			return -1;
		} 
	}
	
	//这里为了测试一开始给电机一个力矩让它转动实际使用时可以删除
	for (i=0; i<SLAVE_COUNT; i++)
	{
		//设置一个值让它开始就转动此处不是必须设置的
		val = 0x001c;
		ret = ec_common_set_pdo_data(&master_specified_info, &master_specified_info.slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TARGET_TORQUE_S16, EC_COMMON_DATA_TYPE_S16, (void *)&val);
		if (ret != EC_COMMON_OK)
		{
			LOG("ec_common_set_pdo_data error, %s:%d\n", __FILE__, __LINE__);
			return -1;
		}
	}
	*/
	
	//注册信号处理函数
	sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
	
	if (sigaction(SIGSEGV, &sa, 0)) 
	{
        EC_COMMON_LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGABRT, &sa, 0)) 
	{
        EC_COMMON_LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGTERM, &sa, 0)) 
	{
        EC_COMMON_LOG( "Failed to install signal handler!\n");
        return -1;
    }
	if (sigaction(SIGQUIT, &sa, 0)) 
	{
        EC_COMMON_LOG( "Failed to install signal handler!\n");
        return -1;
    }

	
	//开始周期任务
	/*
	ret = pthread_create(&ecat_cycle_task_pid, NULL, Task_Ethercat, NULL);
	if (ret)
	{
		EC_COMMON_LOG("pthread_create(ecat_cycle_task_pid) failed, %s:%d", __FILE__, __LINE__);
		return -1;
	}
	*/

	
	
}


/*
	目前如果该模块是等待读写，那么一定要使用单独线程区别开
	PDO 数据改由插补命令驱动
*/
void* Task_Ethercat(void* pvData)
{
	struct timespec tpNow, tpLast, tpDiff;
	gettime(&tpLast);

	while(1)
	{
		
		gettime(&tpNow);
		
		timediff(&tpLast, &tpNow, &tpDiff);
		
		if(tpDiff.tv_sec > 0)
		{
			ASSERT(0);
		}
		else if(tpDiff.tv_nsec > 0)
		{
			//周期性获取各关节反馈信息，然后反馈结果消息出去给相关订阅模块进行闭环处理
			// 1 MS timer
			if(tpDiff.tv_nsec >= 100000000)
			{
				//OK
				//iot_msg_push("ECT/data", NULL, 0);
				iot_msg_push("TIMER/1", NULL, 0);
				tpLast = tpNow;
			}
		}
		else
		{
			ASSERT(0);
		}

		sched_yield();
		//schedule(0,1);
	}
	return NULL;	
}


int ecat2ipt_read_entry(const char * topic, const void * payLoad, int len)
{
	/*
	Interpolation2Ecat_T recv_int2ecat_cmd;
	Ecat2Interpolation_T send_ecat2int_data;
	int ret;
	EC_COMMON_LOG("ecat2ipt_read_entry\n");
	if (strcmp(topic, "ITP2ECAT") == 0)
	{
		//此处可以加入校验机制
		memcpy(&recv_int2ecat_cmd, payLoad, len);
		
		ret = exec_interpolation_2_ecat(&master_specified_info, &recv_int2ecat_cmd);
		if (ret)
		{
			EC_COMMON_LOG("exec_interpolation_2_ecat failed,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		
		//目前返回所有数据先将所有数据位置1
		recv_int2ecat_cmd.getDataFlag = 0x07;
		ret = exec_ecat_2_interpolation(&master_specified_info, &recv_int2ecat_cmd, &send_ecat2int_data);
		if (ret)
		{
			EC_COMMON_LOG("exec_ecat_2_interpolation failed,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}

		//将数据返回给插补模块
		//iot_msg_push("ECAT2ITP", &send_ecat2int_data, sizeof(send_ecat2int_data));
	}
	*/
	
	int ret = exec_interpolation_2_ecat2(&master_specified_info, topic, payLoad, len);
	if (ret)
	{
		EC_COMMON_LOG("exec_interpolation_2_ecat2 failed %s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	return 0;
}


int etc_entry(const char* topic, const void* payLoad, int len)
{
	return 0;
}

#ifdef __cplusplus
}
#endif
