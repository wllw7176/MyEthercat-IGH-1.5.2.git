#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>
#include "ecat_common.h"
#include "ec_common_configs_define.h"
#include "manufacturer_specified_define.h"
#include "ecat_common_intermediate_interface.h"


#ifndef ETHERCAT_H
#define ETHERCAT_H

/*
	@description
		To init ethercat system.Include folllwing info:
		Request master, create domain, get domain_pd,
		configure slaves pdos...
	@return
		NULL
*/
int ethercat_init(void);
void* 	Task_Ethercat(void* pvData);
int 	etc_entry(const char* topic, const void* payLoad, int len);

/*
 *"ECAT2ITP" �Ļص�����
 */
int ecat2ipt_read_entry(const char * topic, const void * payLoad, int len);


#endif //ETHERCAT_H

#ifdef __cplusplus
}
#endif
