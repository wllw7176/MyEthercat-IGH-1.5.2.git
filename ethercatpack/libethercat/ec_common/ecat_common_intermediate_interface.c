#ifdef __cplusplus
extern "C" {
#endif

#include "manufacturer_specified_define.h"
#include "ecat_common_intermediate_interface.h"


int axisNum = 0;
int ready = 0;


/*
 *从插补命令中获取操作模式,获取成功返回0,否则返回-1
 */
int get_operation_model_from_interpolation_cmd(const Interpolation2Ecat_T *interpolation_2_ecat_cmd, unsigned char *operation_model)
{
	switch (interpolation_2_ecat_cmd->ctrlMode)
	{
		case INTERPOLATION_2_ECAT_CTRL_MODEL_POSITION:
			*operation_model = OPERATION_MODE_PROFILE_POSITION;
			break;
		case INTERPOLATION_2_ECAT_CTRL_MODEL_TORQUE:
			*operation_model = OPERATION_MODE_PROFILE_TORQUE;
			break;
		case INTERPOLATION_2_ECAT_CTRL_MODEL_VEL:
			*operation_model = OPERATION_MODE_PROFILE_TORQUE;
			break;
		default:
			return -1;
	}
	return 0;
}

int get_pwr_state(MasterSpecifiedInfo_T *master_specified_info, int slave_pos)
{
	unsigned short control_word;
	int ret = 0;
	int pwr_state = 0;

	if (1 == master_specified_info->activate_flag) //主站激活 pdo数据域才可以使用
	{
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, EC_COMMON_DATA_TYPE_U16, &control_word);
		if (EC_COMMON_OK != ret)
		{
			EC_COMMON_LOG("ec_common_get_pdo_data slave_pos:%d failed, %s:%d\n", slave_pos, __FILE__, __LINE__);
			return -1;
		}
		if (0x06 == control_word)
		{
			pwr_state = 0;
		}
		else if (0x0f == control_word)
		{
			pwr_state = 1;
		}
		else
		{
			//暂且用-1表示其它状态
			pwr_state = -1;
		}
		
	}
	else
	{
		return -1;
	}

	return pwr_state;
}


/*
 *设置从站控制模式 只在第一次接收到命令时设置
 *成功返回0, 否则返回-1
 */
int interpolation_2_ecat_set_slave_operation_model(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, unsigned char operation_model)
{
	int ret;
	unsigned int abort_code;
	
	if (0 == master_specified_info->activate_flag) //sdo 读写要在主站激活前
	{
		ret = ec_common_sdo_download(master_specified_info,  &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_OPERATION_MODEL_S8, &operation_model, sizeof(operation_model), &abort_code);
		if (ret)
		{		
			return -1;
		}
	}
	else
	{
		return -1;
	}
	
	return 0;
}

int interpolation_2_ecat_set_slave_pwr_on(MasterSpecifiedInfo_T *master_specified_info, int slave_pos)
{
	unsigned short control_word;
	int ret = 0;

	if (1 == master_specified_info->activate_flag) //主站激活 pdo数据域才可以使用
	{
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, EC_COMMON_DATA_TYPE_U16, &control_word);
		if (EC_COMMON_OK != ret)
		{
			EC_COMMON_LOG("ec_common_get_pdo_data slave_pos:%d failed, %s:%d\n", slave_pos, __FILE__, __LINE__);
			return -1;
		}
		if (control_word != 0x0f) //已经使能了就不要重复进行使能
		{
			control_word = 0x0f;
			ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, EC_COMMON_DATA_TYPE_U16, (void *)&control_word);
			if (EC_COMMON_OK != ret)
			{
				EC_COMMON_LOG("ec_common_set_pdo_data slave_pos:%d failed, %s:%d\n", slave_pos, __FILE__, __LINE__);
				return -1;
			}
		}
	}
	else
	{
		return -1;
	}

	return 0;
	
}

int interpolation_2_ecat_set_slave_pwr_off(MasterSpecifiedInfo_T *master_specified_info, int slave_pos)
{
	unsigned short control_word;
	int ret = 0;

	if (1 == master_specified_info->activate_flag) //主站激活 pdo数据域才可以使用
	{
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, EC_COMMON_DATA_TYPE_U16, &control_word);
		if (EC_COMMON_OK != ret)
		{
			EC_COMMON_LOG("ec_common_get_pdo_data slave_pos:%d failed, %s:%d\n", slave_pos, __FILE__, __LINE__);
			return -1;
		}
		if (control_word != 0x06) //已经关闭不再重复关闭
		{
			control_word = 0x06;
			ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_CONTROL_WORD_U16, EC_COMMON_DATA_TYPE_U16, (void *)&control_word);
			if (EC_COMMON_OK != ret)
			{
				EC_COMMON_LOG("ec_common_set_pdo_data slave_pos:%d failed, %s:%d\n", slave_pos, __FILE__, __LINE__);
				return -1;
			}
		}
	}
	else
	{
		return -1;
	}

	return 0;
}



/*
 *下面几个函数分别根据命令数据设置从站位置，力矩，速度数据
*/
int interpolation_2_ecat_set_slave_target_pos(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Interpolation2Ecat_T *interpolation_2_ecat_cmd)
{
	int val,ret;
	BUFF_DATA_TYPE buf_val;
	
	buf_val =  interpolation_2_ecat_cmd->setData[slave_pos];
	val = (int) REAL_POS_2_REG_POS_VAL(buf_val);
	
	ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_TARGET_POSITION_S32, EC_COMMON_DATA_TYPE_S32, (void *)&val);
	if (ret != EC_COMMON_OK)
	{
		return -1;
	}
	
	return 0;
}

int interpolation_2_ecat_set_slave_target_torque(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Interpolation2Ecat_T *interpolation_2_ecat_cmd)
{
	int val,ret;
	
	BUFF_DATA_TYPE buf_val;
	
	buf_val =  interpolation_2_ecat_cmd->setData[slave_pos];
	val = (int) REAL_POS_2_REG_POS_VAL(buf_val);

	if (val > 0x00ff)
	{
		val = 0xff;
		EC_COMMON_LOG("Target torue limit in:0x%04x", 0xff);
	}
	ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_TARGET_TORQUE_S16, EC_COMMON_DATA_TYPE_S16, (void *)&val);
	if (ret != EC_COMMON_OK)
	{
		return ret;
	}
	
	return 0;
}

int interpolation_2_ecat_set_slave_commpensation_torque(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Interpolation2Ecat_T *interpolation_2_ecat_cmd)
{
	return 0;
}

int interpolation_2_ecat_set_slave_target_vel(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Interpolation2Ecat_T *interpolation_2_ecat_cmd)
{
	int val,ret;
	
	BUFF_DATA_TYPE buf_val;
	
	buf_val =  interpolation_2_ecat_cmd->setData[slave_pos];
	val = (int) REAL_POS_2_REG_POS_VAL(buf_val);
	
	ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_TARGET_VELOCITY_S32, EC_COMMON_DATA_TYPE_S32, (void *)&val);
	if (ret != EC_COMMON_OK)
	{
		return -1;
	}
	
	return 0;
	
}


/*
 *下面几个函数分别根据命令数据设置插补模块的从站位置，力矩，速度数据
 */
int ecat_2_interpolation_set_slave_cur_pos(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Ecat2Interpolation_T *ecat_2_interpolation_data)
{
	int ret;
	int val;
	
	ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_POSITION_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&val);

	if (ret)
	{
		return -1;
	}
	ecat_2_interpolation_data->getData[slave_pos] = (BUFF_DATA_TYPE)(REG_POS_VAL_2_REAL_POS(val));
	
	return 0;
}

int ecat_2_interpolation_set_slave_cur_torque(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Ecat2Interpolation_T *ecat_2_interpolation_data)
{

	int ret;
	int val;
	
	ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_TORQUE_ACTUAL_VALUE_S16, EC_COMMON_DATA_TYPE_S32, (void *)&val);

	if (ret)
	{
		return -1;
	}
	ecat_2_interpolation_data->getData[slave_pos] = (BUFF_DATA_TYPE)(REG_POS_VAL_2_REAL_POS(val));
		
	return 0;
}

int ecat_2_interpolation_set_slave_cur_vel(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Ecat2Interpolation_T *ecat_2_interpolation_data)
{

	int ret;
	int val;
	
	ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[slave_pos].slave_id_info, INDEX_SANYO_RS2_VELOCITY_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&val);

	if (ret)
	{
		return -1;
	}
	ecat_2_interpolation_data->getData[slave_pos] = (BUFF_DATA_TYPE)(REG_POS_VAL_2_REAL_POS(val));
	
	return 0;
}


int exec_interpolation_2_ecat(MasterSpecifiedInfo_T *master_specified_info, Interpolation2Ecat_T *interpolation_2_ecat_cmd)
{
	static unsigned long long recv_cmd_seq = 0;
	unsigned char operation_model;
	unsigned short control_world;
	int ret;
	int i;
	
	recv_cmd_seq++;
	
	if (interpolation_2_ecat_cmd->ready)
	{
		//控制模式只在第一次进行设置
		if (1 == recv_cmd_seq++) 
		{
			ret = get_operation_model_from_interpolation_cmd(interpolation_2_ecat_cmd, &operation_model);
			if (ret)
			{
				EC_COMMON_LOG("get_operation_model_from_interpolation_cmd failed,%s:%d\n", __FILE__, __LINE__);
				return -1;
			}
			for (i = 0; i<interpolation_2_ecat_cmd->axisNum; i++)
			{
				ret = interpolation_2_ecat_set_slave_operation_model(master_specified_info, i, operation_model);
				if (ret)
				{
					EC_COMMON_LOG("interpolation_2_ecat_set_slave_operation_model slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
				else
				{
					EC_COMMON_LOG("interpolation_2_ecat_set_slave_operation_model(%d) slave_pos:%d success\n", operation_model, i);
				}
			}
			//接收第一个命令时将主站激活
			if (0 == master_specified_info->activate_flag && 1 == master_specified_info->init_flag)
			{
				ret = ec_common_activate_master( master_specified_info);
				if (EC_COMMON_OK != ret)
				{
					EC_COMMON_LOG("ec_common_activate_master failed,%s:%d\n", __FILE__, __LINE__);
					return -1;
				}
				else
				{
					EC_COMMON_LOG("ec_common_activate_master success\n");
				}				
				ec_common_traverse_master_slave_info(master_specified_info);
			}	
		}

		RECEIVE_PDO_DATA(master_specified_info->master, master_specified_info->domain);


		
		//使能/关闭电机
		for (i=0; i<interpolation_2_ecat_cmd->axisNum; i++)
		{
			if (interpolation_2_ecat_cmd->powerOn & (0x01 << i))
			{
				ret = interpolation_2_ecat_set_slave_pwr_on(master_specified_info, i);
				if (ret)
				{
					EC_COMMON_LOG("interpolation_2_ecat_set_slave_pwr_on slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
			}
			else
			{
				ret = interpolation_2_ecat_set_slave_pwr_off(master_specified_info, i);
				if (ret)
				{
					EC_COMMON_LOG("interpolation_2_ecat_set_slave_pwr_off slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
			}
		}	

		//写入控制数据 目前是根据控制模式写入数据
		//控制模式与位掩码要匹配 以后根据需求可能会变化
		switch (interpolation_2_ecat_cmd->ctrlMode)
		{
			case INTERPOLATION_2_ECAT_CTRL_MODEL_POSITION:
				if (interpolation_2_ecat_cmd->setDataFlag & BIT_MASK_SET_DATA_FLAG_WRITE_POS)
				{
					for (i=0; i<interpolation_2_ecat_cmd->axisNum; i++)
					{
						ret = interpolation_2_ecat_set_slave_target_pos(master_specified_info, i, interpolation_2_ecat_cmd);
						if (ret)
						{
							EC_COMMON_LOG("interpolation_2_ecat_set_slave_target_pos slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
							return -1;
						}
					}
				}
				else
				{
					EC_COMMON_LOG("ctrlMode not match setDataFlag,%s:%d\n", __FILE__, __LINE__);
					return -1;
				}
				break;
			case INTERPOLATION_2_ECAT_CTRL_MODEL_TORQUE:
				if (interpolation_2_ecat_cmd->setDataFlag & BIT_MASK_SET_DATA_FLAG_WRITE_TORQUE)
				{
					for (i=0; i<interpolation_2_ecat_cmd->axisNum; i++)
					{
						ret = interpolation_2_ecat_set_slave_target_torque(master_specified_info, i, interpolation_2_ecat_cmd);
						if (ret)
						{
							EC_COMMON_LOG("interpolation_2_ecat_set_slave_target_torque slave_pos:%d failed,ret:%d %s:%d\n", i, ret, __FILE__, __LINE__);
							return -1;
						}
					}
				}
				else
				{
					EC_COMMON_LOG("ctrlMode not match setDataFlag,%s:%d\n", __FILE__, __LINE__);
					return -1;
				}

				/*
				//  先注释以后使用补偿力矩时再打开
				if (interpolation_2_ecat_cmd->setDataFlag & BIT_MASK_SET_DATA_FLAG_WRITE_COMPENSATION_TORQUE)
				{
					for (i=0; i<interpolation_2_ecat_cmd->axisNum; i++)
					{
						ret = interpolation_2_ecat_set_slave_compensation_torque(master_specified_info, i, interpolation_2_ecat_cmd);
						if (ret)
						{
							EC_COMMON_LOG("interpolation_2_ecat_set_slave_commpensation_torque slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
							return -1;
						}
					}
				}
				*/
				break;
			case INTERPOLATION_2_ECAT_CTRL_MODEL_VEL:
				if (interpolation_2_ecat_cmd->setDataFlag & BIT_MASK_SET_DATA_FLAG_WRITE_VEL)
				{
					for (i=0; i<interpolation_2_ecat_cmd->axisNum; i++)
					{
						ret = interpolation_2_ecat_set_slave_target_vel(master_specified_info, i, interpolation_2_ecat_cmd);
						if (ret)
						{
							EC_COMMON_LOG("interpolation_2_ecat_set_slave_target_pos slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
							return -1;
						}
					}
				}
				else
				{
					EC_COMMON_LOG("ctrlMode not match setDataFlag,%s:%d\n", __FILE__, __LINE__);
					return -1;
				}
				break;
			default:
				EC_COMMON_LOG("interpolation_2_ecat_cmd->ctrlMode invalild,%s:%d\n", __FILE__, __LINE__);
				return -1;

		}
		
		SEND_PDO_DATA(master_specified_info->master, master_specified_info->domain);
	}
	else
	{
		return -1;
	}
	
	return 0;

}

ExecInt2EcatCmdNode_T ExecInt2EcatCmdNodeArr [] = {
		{"ecat/ready", exec_interpolation_2_int_ready_callback},
		{"ecat/ctrl_model", exec_interpolation_2_set_ctlmode_callback},
		{"ecat/power_on", exec_interpolation_2_set_pwron_callback},
		{"ecat/position", exec_interpolation_2_set_pos_callback},
		{"ecat/vel", exec_interpolation_2_set_vel_callback},
		{"ecat/torque", exec_interpolation_2_set_torque_callback}
};

int exec_interpolation_2_ecat2(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len)
{
	int i = 0;
	for (i=0; i<ARR_SIZE(ExecInt2EcatCmdNodeArr); i++)
	{
		if (strcmp(ExecInt2EcatCmdNodeArr[i].topic, topic) == 0)
		{
			return ExecInt2EcatCmdNodeArr[i].callback(master_specified_info, topic, payLoad, len);
		}
	}
	return -1;
}

int exec_interpolation_2_int_ready_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len)
{
	static int countTopicIntReady = 0;
	int *pInt = NULL;

	if (len != sizeof(int) * 2)
	{
		EC_COMMON_LOG("Topic(ecat/ready), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	countTopicIntReady++;
	pInt= (int *)payLoad;
	axisNum = pInt[0];
	ready = pInt[1];

	//调试用
	if (1 == countTopicIntReady)
	{
		EC_COMMON_LOG("Recv ecat/ready axisNum:%d ready:%d\n", axisNum, ready);
	}
		
	return 0;
}

int exec_interpolation_2_set_ctlmode_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len)
{

	static int countTopicCtrlModel = 0;
	int *pInt = NULL;
	int ret, i;
	int ctrlMode = 0;
	
	if (len != sizeof(int))
	{
		EC_COMMON_LOG("Topic(ecat/ctrl_model),length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	countTopicCtrlModel++;
	//只在第一个消息才进行设置
	if (1 == countTopicCtrlModel)
	{
		pInt = (int *)payLoad;
		ctrlMode = pInt[0];
		
		for (i = 0; i<axisNum; i++)
		{
			ret = interpolation_2_ecat_set_slave_operation_model(master_specified_info, i, ctrlMode);
			if (ret)
			{
				EC_COMMON_LOG("interpolation_2_ecat_set_slave_operation_model slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
				return -1;
			}
			else
			{
				EC_COMMON_LOG("interpolation_2_ecat_set_slave_operation_model(%d) slave_pos:%d success\n", ctrlMode, i);
			}
		}

		//设置完控制模式才会激活主站
		if (0 == master_specified_info->activate_flag && 1 == master_specified_info->init_flag)
		{
			ret = ec_common_activate_master( master_specified_info);
			if (EC_COMMON_OK != ret)
			{
				EC_COMMON_LOG("ec_common_activate_master failed,%s:%d\n", __FILE__, __LINE__);
				return -1;
			}
			else
			{
				EC_COMMON_LOG("ec_common_activate_master success\n");
			}				
			ec_common_traverse_master_slave_info(master_specified_info);
		}
		
		//返回"ipt/ready"
		int ready = 1;
		iot_msg_push("ipt/ready", &ready, sizeof(ready));
		
	}
	
	return 0;
}

int exec_interpolation_2_set_pwron_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len)
{

	static int countTopicPwrOn;
	int *pInt = NULL;
	int servoPwrFlag = 0;
	int ret,i;
	
	if (len != sizeof(int))
	{
		EC_COMMON_LOG("Topic(ecat/power_on), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	countTopicPwrOn++;
	pInt = (int *)payLoad;

	//使能关闭电机
	for (i=0; i<axisNum; i++)
	{
		if (pInt[0] & (1 << i))
		{
			ret = interpolation_2_ecat_set_slave_pwr_on(master_specified_info, i);
			if (ret)
			{
				EC_COMMON_LOG("interpolation_2_ecat_set_slave_pwr_on slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
				return -1;
			}
			
			servoPwrFlag |= (1 << i);
		}
		else
		{
			ret = interpolation_2_ecat_set_slave_pwr_off(master_specified_info, i);
			if (ret)
			{
				EC_COMMON_LOG("interpolation_2_ecat_set_slave_pwr_off slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
				return -1;
			}
			servoPwrFlag  &= ~(1 << i);
		}
	}

	//返回"ipt/servo_poweron_flag"
	iot_msg_push("ipt/servo_poweron_flag", &servoPwrFlag, sizeof(servoPwrFlag));

	return 0;
}

int exec_interpolation_2_set_pos_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len)
{
	static int countTopicSetPos;
	double *pDouble = NULL;
	int ret,i;
	int setVal;

	if (len != sizeof(double)*axisNum)
	{
		EC_COMMON_LOG("Topic(ecat/position), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	countTopicSetPos++;
	pDouble = (double *)payLoad;

	for (i=0; i<axisNum; i++)
	{

		setVal = (int) REAL_POS_2_REG_POS_VAL(pDouble[i]);
		ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TARGET_POSITION_S32, EC_COMMON_DATA_TYPE_S32, (void *)&setVal);
		if (ret != EC_COMMON_OK)
		{
			return -1;
		}
	}

	return 0;
}

int exec_interpolation_2_set_vel_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len)
{
	static int countTopicSetVel;
	double *pDouble = NULL;
	int ret,i;
	int setVal;

	if (len != sizeof(double)*axisNum)
	{
		EC_COMMON_LOG("Topic(ecat/vel), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	countTopicSetVel++;
	pDouble = (double *)payLoad;

	for (i=0; i<axisNum; i++)
	{
		setVal = (int) REAL_POS_2_REG_POS_VAL(pDouble[i]);
		ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TARGET_VELOCITY_S32, EC_COMMON_DATA_TYPE_S32, (void *)&setVal);
		if (ret != EC_COMMON_OK)
		{
			return -1;
		}
	}
	
	return 0;
}

int exec_interpolation_2_set_torque_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len)
{
	static int countTopicSetVel;
	double *pDouble = NULL;
	int ret,i;
	int setVal;
	int getVal;
	int retBufSize; 
	double *pRetDataBuff = NULL;

	if (len != sizeof(double)*axisNum)
	{
		EC_COMMON_LOG("Topic(ecat/position), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	countTopicSetVel++;
	pDouble = (double *)payLoad;

	for (i=0; i<axisNum; i++)
	{
		setVal = (int) REAL_POS_2_REG_POS_VAL(pDouble[i]);
		ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TARGET_TORQUE_S16, EC_COMMON_DATA_TYPE_S16, (void *)&setVal);
		if (ret != EC_COMMON_OK)
		{
			return -1;
		}
	}
	ret = exec_ecat2interpolation_send_all_data();
	if (ret)
	{
		EC_COMMON_LOG("exec_ecat2interpolation_send_all_data failed, %s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	
	return 0;
}

int exec_ecat2interpolation_send_pos_data(MasterSpecifiedInfo_T *master_specified_info)
{
	int retBufSize; 
	double *pRetDataBuff = NULL;
	int ret,i;
	int getVal;

	//发送"ipt/position" 包括位置,力矩,速度
	retBufSize = sizeof(double)*axisNum;
	pRetDataBuff = (double *)malloc(retBufSize);
	if (pRetDataBuff)
	{
		EC_COMMON_LOG("malloc failed %s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	//填充buff数据
	for (i=0; i<axisNum; i++)
	{
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_POSITION_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&getVal);
		if (ret)
		{
			EC_COMMON_LOG("Get slave:%d postion error %s:%d\n", i, __FILE__, __LINE__);
			free(pRetDataBuff);
			return -1;
		}
		pRetDataBuff[i] = (double)(REG_POS_VAL_2_REAL_POS(getVal));		
	}
	iot_msg_push("ipt/position", pRetDataBuff, retBufSize);

	return 0;
}

int exec_ecat2interpolation_send_torque_data(MasterSpecifiedInfo_T *master_specified_info)
{
	
	int retBufSize; 
	double *pRetDataBuff = NULL;
	int ret,i;
	int getVal;

	//发送"ipt/position" 包括位置,力矩,速度
	retBufSize = sizeof(double)*axisNum;
	pRetDataBuff = (double *)malloc(retBufSize);
	if (pRetDataBuff)
	{
		EC_COMMON_LOG("malloc failed %s:%d\n", __FILE__, __LINE__);
		return -1;
		
	}
	//填充buff数据
	for (i=0; i<axisNum; i++)
	{
		
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TORQUE_ACTUAL_VALUE_S16, EC_COMMON_DATA_TYPE_S16, (void *)&getVal);
		if (ret)
		{
			EC_COMMON_LOG("Get slave:%d tor error %s:%d\n", i, __FILE__, __LINE__);
			free(pRetDataBuff);
			return -1;
		}
		pRetDataBuff[ i] = (double)(REG_TORQUE_VAL_2_REAL_TORQUE(getVal));
		
	}
	iot_msg_push("ipt/torque", pRetDataBuff, retBufSize);
	return 0;
}

int exec_ecat2interpolation_send_vel_data(MasterSpecifiedInfo_T *master_specified_info)
{
	int retBufSize; 
	double *pRetDataBuff = NULL;
	int ret,i;
	int getVal;

	//发送"ipt/position" 包括位置,力矩,速度
	retBufSize = sizeof(double)*axisNum;
	pRetDataBuff = (double *)malloc(retBufSize);
	if (pRetDataBuff)
	{
		EC_COMMON_LOG("malloc failed %s:%d\n", __FILE__, __LINE__);
		return -1;
		
	}
	//填充buff数据
	for (i=0; i<axisNum; i++)
	{
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_VELOCITY_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&getVal);
		if (ret)
		{
			EC_COMMON_LOG("Get slave:%d vel error %s:%d\n", i, __FILE__, __LINE__);
			free(pRetDataBuff);
			return -1;
		}
		pRetDataBuff[i] = (double)(REG_POS_VAL_2_REAL_POS(getVal));
		
	}
	iot_msg_push("ipt/vel", pRetDataBuff, retBufSize);
	
	return 0;
}

int exec_ecat2interpolation_send_all_data(MasterSpecifiedInfo_T *master_specified_info)
{
	int retBufSize; 
	double *pRetDataBuff = NULL;
	int ret,i;
	int getVal;

	//发送"ipt/position" 包括位置,力矩,速度
	retBufSize = sizeof(double)*axisNum*NUM_ARGS_PER_AXIS;
	pRetDataBuff = (double *)malloc(retBufSize);
	if (pRetDataBuff)
	{
		EC_COMMON_LOG("malloc failed %s:%d\n", __FILE__, __LINE__);
		return -1;
		
	}
	//填充buff数据
	for (i=0; i<axisNum; i++)
	{
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_POSITION_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&getVal);
		if (ret)
		{
			EC_COMMON_LOG("Get slave:%d postion error %s:%d\n", i, __FILE__, __LINE__);
			free(pRetDataBuff);
			return -1;
		}
		pRetDataBuff[axisNum*POS_BUFF_SECTION_INDEX + i] = (double)(REG_POS_VAL_2_REAL_POS(getVal));
		
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TORQUE_ACTUAL_VALUE_S16, EC_COMMON_DATA_TYPE_S16, (void *)&getVal);
		if (ret)
		{
			EC_COMMON_LOG("Get slave:%d tor error %s:%d\n", i, __FILE__, __LINE__);
			free(pRetDataBuff);
			return -1;
		}
		pRetDataBuff[axisNum*TORQUE_BUFF_SECTION_INDEX+ i] = (double)(REG_POS_VAL_2_REAL_POS(getVal));
		
		ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_VELOCITY_ACTUAL_VALUE_S32, EC_COMMON_DATA_TYPE_S32, (void *)&getVal);
		if (ret)
		{
			EC_COMMON_LOG("Get slave:%d vel error %s:%d\n", i, __FILE__, __LINE__);
			free(pRetDataBuff);
			return -1;
		}
		pRetDataBuff[axisNum*VEL_BUFF_SECTION_INDEX+ i] = (double)(REG_POS_VAL_2_REAL_POS(getVal));
		
	}
	iot_msg_push("ipt/position", pRetDataBuff, retBufSize);
	
	
	return 0;
}




int exec_ecat_2_interpolation_bak(MasterSpecifiedInfo_T *master_specified_info, Interpolation2Ecat_T *interpolation_2_ecat_cmd, Ecat2Interpolation_T *result)
{
	int ret;
	int i;
	unsigned char err_register_val;

	if (1 == master_specified_info->activate_flag)
	{
		RECEIVE_PDO_DATA(master_specified_info->master, master_specified_info->domain);
		
		//主站激活成功说明Ethercat总线ready
		result->ready = 1;

		result->getDataFlag = interpolation_2_ecat_cmd->getDataFlag;
		
		//设置各从站使能位
		result->powerOnFlag = 0;
		for (i=0; i<interpolation_2_ecat_cmd->axisNum; i++)
		{
			ret = get_pwr_state(master_specified_info, i);
			if (1 == ret)
			{
				result->powerOnFlag |= 1<<i;
			}
			if (-1 == ret)
			{
				EC_COMMON_LOG("get_pwr_state slave_pos:%d failed, %s:%d\n", i, __FILE__, __LINE__);
				return -1;
			}
		}

		//判断各从站是否有错误
		for (i=0; i<interpolation_2_ecat_cmd->axisNum; i++)
		{
			ret = ec_common_get_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_ERROR_REGISTER_U8, EC_COMMON_DATA_TYPE_U8, (void *)&err_register_val);
			if (EC_COMMON_OK != ret)
			{
				EC_COMMON_LOG("ec_common_get_pdo_data slave_pos:%d err_reg err, %s:%d\n", __FILE__, __LINE__);
				return -1;
			}
			//有一个从站错误就将错误标志置位
			if (err_register_val != 0)
			{
				result->errFlag = 1;
				break;
			}
		}

		//填充从站的数据返回给插补模块
		for (i=0; i<interpolation_2_ecat_cmd->axisNum; i++)
		{
			if (interpolation_2_ecat_cmd->getDataFlag & BIT_MASK_GET_DATA_FLAG_CUR_POS)
			{
				ret = ecat_2_interpolation_set_slave_cur_pos(master_specified_info, i, result);
				if (ret)
				{
					EC_COMMON_LOG("ecat_2_interpolation_set_slave_cur_pos slave_pos:%d error, %s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
			}
			if (interpolation_2_ecat_cmd->getDataFlag & BIT_MASK_GET_DATA_FLAG_CUR_TORQUE)
			{
				ret = ecat_2_interpolation_set_slave_cur_torque(master_specified_info, i, result);
				if (ret)
				{
					EC_COMMON_LOG("ecat_2_interpolation_set_slave_cur_pos slave_pos:%d error, %s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
			}
			if (interpolation_2_ecat_cmd->getDataFlag & BIT_MASK_GET_DATA_FLAG_CUR_VEL)
			{
				ret = ecat_2_interpolation_set_slave_cur_vel(master_specified_info, i, result);
				if (ret)
				{
					EC_COMMON_LOG("ecat_2_interpolation_set_slave_cur_pos slave_pos:%d error, %s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
			}
		}
		
		SEND_PDO_DATA(master_specified_info->master, master_specified_info->domain);		
	}	
	else
	{
		return -1;
	}

	return 0;
}

int exec_interpolation_2_ecat_bak(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len)
{
	static int axisNum = 0; 
	static int ready = 0;
	static int ctrlMode = 0;
	static int countTopicCtrlModel = 0;
	static int countTopicIntReady = 0;
	static int countTopicPwrOn = 0;
	static int countTopicSetPos = 0, countTopicSetVel =0, countTopicSetTorque = 0;;
	static int setCtrlModelFlag = 0;
	int operation_model = 0;
	int setVal;
	int ret;
	int i;
	int *p = NULL;
	double *pDouble = NULL;

	if (strcmp(topic, "ecat/ready") == 0)
	{
		if (len != sizeof(int) * 2)
		{
			EC_COMMON_LOG("Topic(ecat/ready), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		
		countTopicIntReady++;
		p = (int *)payLoad;
		axisNum = p[0];
		ready = p[1];

		//调试用
		if (1 == countTopicIntReady)
		{
			EC_COMMON_LOG("Recv ecat/ready axisNum:%d ready:%d\n", axisNum, ready);
		}
		
	}
	else if (strcmp(topic, "ecat/ctrl_model") == 0)
	{
		if (len != sizeof(int))
		{
			EC_COMMON_LOG("Topic(ecat/ctrl_model),length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		countTopicCtrlModel++;
		//只在第一个消息才进行设置
		if (1 == countTopicCtrlModel)
		{
			ctrlMode = *(int *)payLoad;
			for (i = 0; i<axisNum; i++)
			{
				ret = interpolation_2_ecat_set_slave_operation_model(master_specified_info, i, ctrlMode);
				if (ret)
				{
					EC_COMMON_LOG("interpolation_2_ecat_set_slave_operation_model slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
				else
				{
					EC_COMMON_LOG("interpolation_2_ecat_set_slave_operation_model(%d) slave_pos:%d success\n", operation_model, i);
				}
			}
			setCtrlModelFlag = 1;

			//设置完控制模式才会激活主站
			if (0 == master_specified_info->activate_flag && 1 == master_specified_info->init_flag)
			{
				ret = ec_common_activate_master( master_specified_info);
				if (EC_COMMON_OK != ret)
				{
					EC_COMMON_LOG("ec_common_activate_master failed,%s:%d\n", __FILE__, __LINE__);
					return -1;
				}
				else
				{
					EC_COMMON_LOG("ec_common_activate_master success\n");
				}				
				ec_common_traverse_master_slave_info(master_specified_info);
			}	
		}
		
		
	}
	else if (strcmp(topic, "ecat/power_on") == 0)
	{

		if (len != sizeof(int))
		{
			EC_COMMON_LOG("Topic(ecat/power_on), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		
		countTopicPwrOn++;
		p = (int *)payLoad;

		//使能关闭电机
		for (i=0; i<axisNum; i++)
		{
			if (p[0] & (0x01 << i))
			{
				ret = interpolation_2_ecat_set_slave_pwr_on(master_specified_info, i);
				if (ret)
				{
					EC_COMMON_LOG("interpolation_2_ecat_set_slave_pwr_on slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
			}
			else
			{
				ret = interpolation_2_ecat_set_slave_pwr_off(master_specified_info, i);
				if (ret)
				{
					EC_COMMON_LOG("interpolation_2_ecat_set_slave_pwr_off slave_pos:%d failed,%s:%d\n", i, __FILE__, __LINE__);
					return -1;
				}
			}
		}
	}
	else if (strcmp(topic, "ecat/position") == 0)
	{
		if (len != sizeof(double)*axisNum)
		{
			EC_COMMON_LOG("Topic(ecat/position), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		countTopicSetPos++;
		pDouble = (double *)payLoad;

		for (i=0; i<axisNum; i++)
		{

			setVal = (int) REAL_POS_2_REG_POS_VAL(pDouble[i]);
			ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TARGET_POSITION_S32, EC_COMMON_DATA_TYPE_S32, (void *)&setVal);
			if (ret != EC_COMMON_OK)
			{
				return -1;
			}
		}
		
	}
	else if (strcmp(topic, "ecat/vel") == 0)
	{
		if (len != sizeof(double)*axisNum)
		{
			EC_COMMON_LOG("Topic(ecat/vel), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		countTopicSetVel++;
		pDouble = (double *)payLoad;

		for (i=0; i<axisNum; i++)
		{
			setVal = (int) REAL_POS_2_REG_POS_VAL(pDouble[i]);
			ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TARGET_VELOCITY_S32, EC_COMMON_DATA_TYPE_S32, (void *)&setVal);
			if (ret != EC_COMMON_OK)
			{
				return -1;
			}
		}
	}
	else if (strcmp(topic, "ecat/torque") == 0)
	{
		if (len != sizeof(double)*axisNum)
		{
			EC_COMMON_LOG("Topic(ecat/position), length of payload mismatch,%s:%d\n", __FILE__, __LINE__);
			return -1;
		}
		countTopicSetVel++;
		pDouble = (double *)payLoad;

		for (i=0; i<axisNum; i++)
		{
			setVal = (int) REAL_POS_2_REG_POS_VAL(pDouble[i]);
			ret = ec_common_set_pdo_data(master_specified_info, &master_specified_info->slaves_specified_infos[i].slave_id_info, INDEX_SANYO_RS2_TARGET_TORQUE_S16, EC_COMMON_DATA_TYPE_S16, (void *)&setVal);
			if (ret != EC_COMMON_OK)
			{
				return -1;
			}
		}
	}
	
	return 0;
}


#ifdef __cplusplus
}
#endif 


