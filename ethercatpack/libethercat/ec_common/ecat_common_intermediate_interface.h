#ifdef __cplusplus
extern "C" {
#endif

#ifndef ROBOT_ECAT_COMMON_INTERMEDIATE_INTERFACE_H
#define ROBOT_ECAT_COMMON_INTERMEDIATE_INTERFACE_H


/******************************************************************
 *其它模块与ecat_common模块交互的中间接口
 *****************************************************************/

#include "ecat_common.h"

#define NUM_AXIS                                 16 //轴个数
#define NUM_ARGS_PER_AXIS                        3 //每个轴参数个数目前为3个 位置，力矩，速度，补偿力矩
#define INTERPOLATION_2_ECAT_CTRL_MODEL_POSITION 1
#define INTERPOLATION_2_ECAT_CTRL_MODEL_TORQUE   2
#define INTERPOLATION_2_ECAT_CTRL_MODEL_VEL      3

typedef double BUFF_DATA_TYPE;

//读写标志位掩码
#define BIT_MASK_GET_DATA_FLAG_CUR_POS          (0x01 << 0)
#define BIT_MASK_GET_DATA_FLAG_CUR_TORQUE       (0x01 << 1)
#define BIT_MASK_GET_DATA_FLAG_CUR_VEL          (0x01 << 2)
#define BIT_MASK_SET_DATA_FLAG_WRITE_POS        (0x01 << 0)
#define BIT_MASK_SET_DATA_FLAG_WRITE_TORQUE     (0x01 << 1)
#define BIT_MASK_SET_DATA_FLAG_WRITE_VEL        (0x01 << 2)
#define BIT_MASK_SET_DATA_FLAG_WRITE_COMPENSATION_TORQUE (0X01 << 3)


#define TPOIC_ITP2ECAT                           "ITP2ECAT"
#define TPOIC_ECAT2ITP                           "ECAT2ITP"


//数据所在buff区间 返回插补 Buff 分布如下
//|        section0          |            section1         |           section2        |...
//|data0....datan-1(pos_data)|datan...data2n-1(torque_data)|data2n...data3n-1(vel_data)|...
#define POS_BUFF_SECTION_INDEX                   0
#define TORQUE_BUFF_SECTION_INDEX                1
#define VEL_BUFF_SECTION_INDEX                   2



typedef struct EC_CMOMON_INTERPOLATION_2_ECAT_
{
	unsigned char  ready;		//运动插补模块初始化完成标志 
	int   axisNum;	            //机器人配置轴个数
	int   powerOn;	            //伺服上电指令，从bit0开始，每位表示一个轴，上升沿电机上电，下降沿电机断电
	int   ctrlMode;		        //控制模式，1：位置模式，2：扭矩模式，3：速度模式
	int   getDataFlag;		    //读数据标志，bit0：读当前位置；bit1：读当前力矩，bit2：读当前速度
	int   setDataFlag;		    //写周期数据标志bit0：写命令位置；bit1：写命令力矩；bit2：写命令速度；bit3：写力矩补偿值。所写数据必须与控制模式对应，要做控制模式检查。
	BUFF_DATA_TYPE	setData[NUM_AXIS*NUM_ARGS_PER_AXIS];	    //写到伺服放大器的命令数据  最多7个轴,没个轴3个数据:当前位置 当前力矩 当前速度
} Interpolation2Ecat_T;

typedef struct EC_CMOMON_ECAT_2_INTERPOLATION_
{
	  unsigned char  ready;			//伺服总线工作就绪，1为就绪
      unsigned char  errFlag;		//标记标志
      int   powerOnFlag;	       //伺服上电指令，从bit0开始，每位表示一个轴，1为轴上电。
      int   getDataFlag;		   //读数据标志，bit0：当前位置；bit1：当前力矩，bit3：当前速度
      BUFF_DATA_TYPE	 getData[NUM_AXIS*NUM_ARGS_PER_AXIS];		   //根据 getDataFlag标志反馈的数据
} Ecat2Interpolation_T;


/*
 *从插补命令中获取操作模式,获取成功返回0,否则返回-1
 */
int get_operation_model_from_interpolation_cmd(const Interpolation2Ecat_T *interpolation_2_ecat_cmd, unsigned char *operation_model);

/*
 *获取从站的使能状态 1:使能开 0:使能关 -1:获取状态失败
 */
int get_pwr_state(MasterSpecifiedInfo_T *master_specified_info, int slave_pos);

/*
 *设置从站控制模式 只在第一次接收到命令时设置
 *成功返回0, 否则返回-1
 */
int interpolation_2_ecat_set_slave_operation_model(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, unsigned char operation_model);

/*
 *电机使能/禁止使能 操作成功返回0 否则返回-1
 */
int interpolation_2_ecat_set_slave_pwr_on(MasterSpecifiedInfo_T *master_specified_info, int slave_pos);
int interpolation_2_ecat_set_slave_pwr_off(MasterSpecifiedInfo_T *master_specified_info, int slave_pos);

//实际数据与寄存器数据转换宏
#define REAL_POS_2_REG_POS_VAL(X)                     (X)
#define REG_POS_VAL_2_REAL_POS(X)                     (X)
#define REAL_TORUE_2_REG_TORQUE_VAL(X)                (X)
#define REG_TORQUE_VAL_2_REAL_TORQUE(X)               (X)
#define REAL_VEL_2_REG_VEL_VAL(X)                     (X)
#define REG_VEL_VAL_2_REAL_VEL(X)                     (X)


/////////////////////////// 下面几个函数要特别注意实际物理单位与电机寄存器表示单位的装换/////////////////////////////////////////////////////
/*
 *下面几个函数分别根据命令数据设置从站位置，目标力矩，速度数据 补偿力矩
*/
int interpolation_2_ecat_set_slave_target_pos(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Interpolation2Ecat_T *interpolation_2_ecat_cmd);
int interpolation_2_ecat_set_slave_target_torque( MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Interpolation2Ecat_T *interpolation_2_ecat_cmd);
int interpolation_2_ecat_set_slave_compensation_torque(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Interpolation2Ecat_T *interpolation_2_ecat_cmd);
int interpolation_2_ecat_set_slave_target_vel(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Interpolation2Ecat_T *interpolation_2_ecat_cmd);

/*
 *下面几个函数分别根据命令数据设置插补模块的从站位置，力矩，速度数据
 */
int ecat_2_interpolation_set_slave_cur_pos(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Ecat2Interpolation_T *ecat_2_interpolation_data);
int ecat_2_interpolation_set_slave_cur_torque(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Ecat2Interpolation_T *ecat_2_interpolation_data);
int ecat_2_interpolation_set_slave_cur_vel(MasterSpecifiedInfo_T *master_specified_info, int slave_pos, Ecat2Interpolation_T *ecat_2_interpolation_data);


/*
  执行插补模块发过来的命令
  执行成功返回0,否则返回-1
*/
int exec_interpolation_2_ecat(MasterSpecifiedInfo_T *master_specified_info, Interpolation2Ecat_T *interpolation_2_ecat_cmd);

/*
 将执行结果返回给插补模块
 执行成功返回0,否则返回-1
*/
int exec_ecat_2_interpolation(MasterSpecifiedInfo_T *master_specified_info, Interpolation2Ecat_T *interpolation_2_ecat_cmd, Ecat2Interpolation_T *result);


//消息回调处理
typedef int (*int2ecat_callback)(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len);

typedef struct ExecInt2EcatCmdNode{
	const char *topic;
	int2ecat_callback callback;
}ExecInt2EcatCmdNode_T;

/*
	定义主题回调函数其中exec_interpolation_2_ecat2为总入口,根据不同主题调用其它的回调函数
*/
int exec_interpolation_2_ecat2(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len);
int exec_interpolation_2_int_ready_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len);
int exec_interpolation_2_set_ctlmode_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len);
int exec_interpolation_2_set_pwron_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len);
int exec_interpolation_2_set_pos_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len);
int exec_interpolation_2_set_vel_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len);
int exec_interpolation_2_set_torque_callback(MasterSpecifiedInfo_T *master_specified_info, const char * topic, const void * payLoad, int len);


int exec_ecat2interpolation_send_pos_data(MasterSpecifiedInfo_T *master_specified_info);
int exec_ecat2interpolation_send_torque_data(MasterSpecifiedInfo_T *master_specified_info);
int exec_ecat2interpolation_send_vel_data(MasterSpecifiedInfo_T *master_specified_info);
int exec_ecat2interpolation_send_all_data(MasterSpecifiedInfo_T *master_specified_info);




//Mqtt 消息处理


#endif //ROBOT_ECAT_COMMON_INTERMEDIATE_INTERFACE_H

#ifdef __cplusplus
}
#endif



