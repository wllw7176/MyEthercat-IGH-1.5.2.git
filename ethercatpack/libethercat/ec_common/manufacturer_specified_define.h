#ifndef MANUFACTURER_SPECIFIED_OBJ_DICT_H
#define MANUFACTURER_SPECIFIED_OBJ_DICT_H

#define VENDOR_ID_SANYO     0x000001b9
#define MANUFACTURER_SPECIFIED_VENDOR_ID VENDOR_ID_SANYO



//电机运行模式
#define RUN_MODEL_FREE 0x00
#define RUN_MODEL_DC 0x01 

//操作模式宏定义
#define OPERATION_MODE_PROFILE_POSITION                          1
#define OPERATION_MODE_PROFILE_VELOCITY                          3
#define OPERATION_MODE_PROFILE_TORQUE                            4
#define OPERATION_MODE_HOMING                                    6
#define OPERATION_MODE_INTERPOLATED_POSITION_                    7
#define OPERATION_MODE_CYCLIC_SYNCHRONOUS_POSITION               8
#define OPERATION_MODE_CYCLIC_SYNCHRONOUS_VELOCITY               9
#define OPERATION_MODE_CYCLIC_SYNCHRONOUS_TORQUE                 10



//根据不同制造商定义不同的宏
#if MANUFACTURER_SPECIFIED_VENDOR_ID == VENDOR_ID_SANYO

//对象字典定义与ec_common_configs_define.c中对应 后缀数字代表字典数据位宽
#define INDEX_SANYO_RS2_CONTROL_WORD_U16                                  0x6040,0x00
#define INDEX_SANYO_RS2_TARGET_POSITION_S32                               0x607a,0x00
#define INDEX_SANYO_RS2_PROFILE_VELOCITY_U32                              0x6081,0x00
#define INDEX_SANYO_RS2_PROFILE_ACCELERATION_U32                          0x6083,0x00
#define INDEX_SANYO_RS2_PROFILE_DECLARATION_U32                           0x6084,0x00
#define INDEX_SANYO_RS2_TARGET_VELOCITY_S32                               0x60ff,0x00
#define INDEX_SANYO_RS2_TARGET_TORQUE_S16                                 0x6071,0x00
#define INDEX_SANYO_RS2_TOUCH_PROBE_FUNCTION_Y16                          0x60b8,0x00
#define INDEX_SANYO_RS2_PHYSICAL_OUTPUTS_U32                              0x60fe,0x01
#define INDEX_SANYO_RS2_STATUS_WORD_ U16                                  0x6041,0x00
#define INDEX_SANYO_RS2_STATUS_WORD1_U16                                  0x2100,0x00
#define INDEX_SANYO_RS2_POSITION_ACTUAL_VALUE_S32                         0x6064,0x00
#define INDEX_SANYO_RS2_VELOCITY_ACTUAL_VALUE_S32                         0x606c,0x00
#define INDEX_SANYO_RS2_TORQUE_ACTUAL_VALUE_S16                           0x6077,0x00
#define INDEX_SANYO_RS2_FOLLOWING_ERROR_ACTUAL_VALUE_S32                  0x60f4,0x00
#define INDEX_SANYO_RS2_TOUCH_PROBE_STATUS_U16                            0x60b9,0x00
#define INDEX_SANYO_RS2_TOUCH_PROBE_POSITION_1_POSITIVE_VALUE_S32         0x60ba,0x00
#define INDEX_SANYO_RS2_TOUCH_PROBE_POSITION_1_NEGATIVE_VALUE_S32         0x60bb,0x00
#define INDEX_SANYO_RS2_DIGITAL_INPUTS_U32                                0x60fd,0x00
#define INDEX_SANYO_RS2_ERROR_REGISTER_U8                                 0x1001,0x00
#define INDEX_SANYO_RS2_MODES_OF_OPERATION_DISPLAY_S8                     0x6061,0x00

//其它索引定义
#define INDEX_SANYO_RS2_OPERATION_MODEL_S8                                0x6060,0x00
/* 
同步管理器宏定义
	0x1C32:SM2同步(Output SyncManager Parameter) 
	0x1C33:SM3同步(Input SyncManager Parameter)
*/
#define INDEX_SANYO_RS2_SM2_SYNC_ARG_NUM_DIC_U8                           0x1c32,0x00
#define INDEX_SANYO_RS2_SM2_SYNC_TYPE_U16                                 0x1c32,0x01
#define INDEX_SANYO_RS2_SM2_CYCLE_TIME_U32                                0x1c32,0x02
#define INDEX_SANYO_RS2_SM2_SWITCH_TIME_U32                               0x1c32,0x03
#define INDEX_SANYO_RS2_SM2_SUPPOETED_TYPE_U16                            0x1c32,0x04
#define INDEX_SANYO_RS2_SM3_SYNC_ARG_NUM_U8                               0x1c33,0x00
#define INDEX_SANYO_RS2_SM3_SYNC_TYPE_U16                                 0x1c33,0x01
#define INDEX_SANYO_RS2_SM3_CYCLE_TIME_U32                                0x1c33,0x02
#define INDEX_SANYO_RS2_SM3_SWITCH_TIME_U32                               0x1c33,0x03
#define INDEX_SANYO_RS2_SM3_SUPPOETED_TYPE_U16                            0x1c32,0x04


#endif //VENDOR_ID

#endif //MANUFACTURER_SPECIFIED_OBJ_DICT_H




